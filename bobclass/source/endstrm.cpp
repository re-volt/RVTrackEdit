#include <endstrm.h>
using namespace std;

EndianInputStream::EndianInputStream(const char* filename)
{
	offset = 0;
	buffer = NULL;
	ifstream in(filename, ios::in | ios::binary | ios::ate);
	if(in.is_open())
	{
		size = (size_t)in.tellg();
		if(size > 0)
		{
			in.seekg(0, ios::beg);
			buffer = new char[size+1];
			in.read(buffer, size);
			buffer[size] = '\0';	// have a zero terminating char (helpful for text-only data)
		}
		in.close();
	}
}

EndianInputStream::~EndianInputStream()	
{
	//if(buffer) free(buffer);
	if(buffer)
	{
		delete[] buffer;
		buffer = NULL;
	}
}

bool EndianInputStream::is_open(void)
{
	return buffer ? true : false;
}

void EndianInputStream::open(const char* filename)
{
	if(buffer)
	{
		size = 0;
		delete[] buffer;
		buffer = NULL;
	}
	offset = 0;
	ifstream in(filename, ios::in | ios::binary | ios::ate);
	if(in.is_open())
	{
		size = (size_t)in.tellg();
		in.seekg(0, ios::beg);
		if(size > 0)
		{
			buffer = new char[size];
			in.read(buffer, size);
		}
		in.close();
	}
}

void EndianInputStream::GetChunk(void* buf, U32 len)
{
	if(offset+len <= size)
	{
		memcpy(buf, &buffer[offset], len);
		offset += len;
	}
	else
		buf = 0;
}

U32 EndianInputStream::FTell(void)
{
	return offset;
}

U8 EndianInputStream::GetText(char* text, U8 maxtextlen)
{
	int k;
	for(k = 0; k < maxtextlen; k++){
		GetChunk(&text[k], 1);
		if(text[k] == 0)
			break;
	}
	text[k] = 0;
	return k;
}

U8 EndianInputStream::GetPaddedText(char* text, U8 maxtextlen)
{
	U8 count = 0;
	U8 textlen = GetU8();

	bool textispadded = false;
	if((textlen % 2) > 0)
		textispadded = true;
	while( (count < maxtextlen) && (textlen > 0))
	{
		text[count] = GetU8();
		count++;
		textlen--;
	};
	while(textlen--)
		GetU8();

	text[count] = GetU8();	//pull the terminating zero
	if(textispadded)
		GetU8();	//pull the pad byte

	return count;
}

U8 EndianInputStream::GetU8(void)
{
	if(offset+sizeof(U8) <= size)
	{
		U8 r = *(U8 *)(buffer+offset);
		offset++;
		return r;
	}
	else
		return 0;
}

U16 EndianInputStream::GetU16(void)
{
	if(offset+sizeof(U16) <= size)
	{
		U16 v;
		U8 r[4];

		r[0] = *(buffer + offset);
		r[1] = *(buffer + offset + 1);
		v = ( ((U32)r[1] ) << 8) | r[0] ;

		offset += 2;
		return v;
	}
	else
		return 0;
}

U32 EndianInputStream::GetU32(void)
{
	if(offset+sizeof(U32) <= size)
	{
		U32 v;
		U8 r[4];

		r[0] = *(buffer + offset);
		r[1] = *(buffer + offset + 1);
		r[2] = *(buffer + offset + 2);
		r[3] = *(buffer + offset + 3);
		v = ( ((U32)r[3]) << 24) | ( ((U32)r[2]) << 16) | ( ((U32)r[1]) << 8) | r[0] ;

		offset+=4;
		return v;
	}
	else
		return 0;
}

S8 EndianInputStream::GetS8(void)
{
	if(offset+sizeof(S8) <= size)
	{
		S8 r = *(S8 *)(buffer + offset);
		offset++;
		return r;
	}
	else
		return 0;
}

S16 EndianInputStream::GetS16(void)
{
	if(offset+sizeof(S16) <= size)
	{
		S16 v;
		U8 r[4];

		r[0] = *(buffer + offset);
		r[1] = *(buffer + offset + 1);
		v = ( ((U32)r[1]) << 8) | r[0] ;

		offset+=2;
		return v;
	}
	else
		return 0;
}

S32 EndianInputStream::GetS32(void)
{
	if(offset+sizeof(S32) <= size)
	{
		S32 v;
		U8 r[4];

		r[0] = *(buffer + offset);
		r[1] = *(buffer + offset + 1);
		r[2] = *(buffer + offset + 2);
		r[3] = *(buffer + offset + 3);
		v = ( ((U32)r[3]) << 24) | ( ((U32)r[2]) << 16) | ( ((U32)r[1]) << 8) | r[0] ;

		offset+=4;
		return v;
	}
	else
		return 0;
}


float EndianInputStream::GetFloat(void)
{
	if(offset+sizeof(float) <= size)
	{
		U32 v;
		U8 r[4];

		r[0] = *(buffer + offset);
		r[1] = *(buffer + offset + 1);
		r[2] = *(buffer + offset + 2);
		r[3] = *(buffer + offset + 3);
		v = (( ((U32)r[3]) << 24) | ( ((U32)r[2]) << 16) | ( ((U32)r[1]) << 8) | r[0]);

		offset+=4;
		return *((float *)&v);
	}
	else
		return 0.0f;
}

U32 EndianInputStream::GetSize(void)
{
	return size;
}

bool EndianInputStream::SkipBytes(U32 count)
{
	if(offset+count < size)
	{
		offset += count;
		return true;
	}
	else
		return false; 
}

char* EndianInputStream::GetBuffer(void)
{
	return buffer;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


EndianOutputStream::EndianOutputStream(const char* filename)
{
	bufferSize = START_FILE_BUFFER_SIZE;//size;
	buf = (char*)calloc(1, START_FILE_BUFFER_SIZE + 16);//size);
	
	index = 0;
	strcpy(fname,filename);
}

EndianOutputStream::~EndianOutputStream()
{
	if(buf)
		free(buf);
}

bool EndianOutputStream::CheckSize(int len)
{
	if((index + len) >= bufferSize){

		char *newBuf;
		newBuf = (char*)calloc(1, bufferSize + START_FILE_BUFFER_SIZE);

		if(!newBuf){
			return false;
		}
		bufferSize += START_FILE_BUFFER_SIZE;

		memcpy(newBuf, buf, index);

		free(buf);
		buf = newBuf;
	}

	return true;
}

void EndianOutputStream::PutU8(U8 data)
{
	if(!CheckSize(1)) return;
	buf[index++] = data;
}

void EndianOutputStream::PutU16(U16 data)
{
	if(!CheckSize(2)) return;
	char *buffer = &buf[index];
	index+=2;
	*buffer++ = (data>>0)&0xff;
	*buffer++ = (data>>8)&0xff;
}

void EndianOutputStream::PutU32(U32 data)
{
	if(!CheckSize(4)) return;
	char *buffer = &buf[index];
	index+=4;
	*buffer++ = (data>>0)&0xff;
	*buffer++ = (data>>8)&0xff;
	*buffer++ = (data>>16)&0xff;
	*buffer++ = (data>>24)&0xff;
}

void EndianOutputStream::PutS8(S8 data)
{
	if(!CheckSize(1)) return;
	buf[index++] = data;
}

void EndianOutputStream::PutS16(S16 data)
{
	if(!CheckSize(2)) return;
	char *buffer = &buf[index];
	index+=2;
	*buffer++ = (data>>0)&0xff;
	*buffer++ = (data>>8)&0xff;
}

void EndianOutputStream::PutS32(S32 data)
{
	if(!CheckSize(4)) return;
	char *buffer = &buf[index];
	index+=4;
	*buffer++ = (data>>0)&0xff;
	*buffer++ = (data>>8)&0xff;
	*buffer++ = (data>>16)&0xff;
	*buffer++ = (data>>24)&0xff;
}

void EndianOutputStream::PutFloat(float data)
{
	if(!CheckSize(4)) return;
	U32 v = *((U32 *)&data);
	char *buffer = &buf[index];
	index+=4;
	*buffer++ = (v>>0)&0xff;
	*buffer++ = (v>>8)&0xff;
	*buffer++ = (v>>16)&0xff;
	*buffer++ = (v>>24)&0xff;
}

void EndianOutputStream::PutPaddedText(const char* str)
{
	int len = strlen(str);	//we want to add the name length in the file
	bool textispadded = false;
	if((len % 2) > 0)
	{
		textispadded = true;
	}
	PutS8(len);		//insert the text length
	fputs(str);		//insert the text
	PutS8('\0');	//append null char

	if(textispadded)
		PutS8('\0');
}

bool EndianOutputStream::WriteFile()
{
	bool success = true;
	if(!buf || index < 1)
		return false;

	FILE* fp = fopen(fname, "wb");
	if(fp)
	{
		if(fwrite(buf, index, 1, fp) != 1)
			success = false;
		fclose(fp);
	}
	else
		return false;
	
	return success;
}

void EndianOutputStream::write(void* buffer, int len)
{
	if(!CheckSize(len)) return;
	memcpy(&buf[index], buffer, len);
	index += len;
}

void EndianOutputStream::fputs(const char* str)
{
	int len = strlen(str);
	if(!CheckSize(len)) return;
	memcpy(&buf[index], str, len);
	index += len;
}

bool EndianOutputStream::is_open()
{
	if(buf)
		return true;
	else
		return false;
}

U32 EndianOutputStream::tellp()
{
	return (U32)index;
}
