# Microsoft Developer Studio Project File - Name="TrackEdit" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=TrackEdit - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "TrackEdit.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "TrackEdit.mak" CFG="TrackEdit - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "TrackEdit - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "TrackEdit - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Tools/Track Editor/TrackEdit", TQBAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "TrackEdit - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /Zi /O2 /I "..\UnitConv\Custom" /I "..\Game\source\inc" /I "..\d3dframe\include" /I "..\..\dx6\include" /I "..\..\bobclass\inc" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_PC" /D "NEW_WALLS" /D "_TRACK_EDITOR" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ddraw.lib d3drm.lib dxguid.lib dinput.lib mss32.lib /nologo /subsystem:windows /debug /machine:I386 /libpath:"..\..\dx6\lib" /libpath:"..\..\mss\lib"

!ELSEIF  "$(CFG)" == "TrackEdit - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MTd /W3 /Gm /Gi /GX /Zi /Od /I "..\UnitConv\Custom" /I "..\Game\source\inc" /I "..\..\Game\Code\source\inc" /I "..\d3dframe\include" /I "..\..\dx6\include" /I "..\..\bobclass\inc" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_PC" /D "NEW_WALLS" /D "_TRACK_EDITOR" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ddraw.lib d3drm.lib dxguid.lib dinput.lib mss32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"..\..\dx6\lib" /libpath:"..\..\mss\lib"
# SUBTRACT LINK32 /incremental:no

!ENDIF 

# Begin Target

# Name "TrackEdit - Win32 Release"
# Name "TrackEdit - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;hpj;bat;for;f90"
# Begin Source File

SOURCE=.\compile.cpp

!IF  "$(CFG)" == "TrackEdit - Win32 Release"

# ADD CPP /D "D3D_OVERLOADS"
# SUBTRACT CPP /D "_PC"

!ELSEIF  "$(CFG)" == "TrackEdit - Win32 Debug"

# ADD CPP /D "D3D_OVERLOADS" /FR
# SUBTRACT CPP /D "_PC"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Editor.cpp
# ADD CPP /D "D3D_OVERLOADS"
# SUBTRACT CPP /D "_PC"
# End Source File
# Begin Source File

SOURCE=.\editorhelp.cpp
# End Source File
# Begin Source File

SOURCE=..\..\bobclass\source\endstrm.cpp
# End Source File
# Begin Source File

SOURCE=.\fileio.cpp
# ADD CPP /D "D3D_OVERLOADS"
# SUBTRACT CPP /D "_PC"
# End Source File
# Begin Source File

SOURCE=.\Inputs.cpp
# ADD CPP /D "D3D_OVERLOADS"
# SUBTRACT CPP /D "_PC"
# End Source File
# Begin Source File

SOURCE=.\modules.cpp
# End Source File
# Begin Source File

SOURCE=.\platform.cpp
# End Source File
# Begin Source File

SOURCE=.\Render.cpp
# End Source File
# Begin Source File

SOURCE=.\states.cpp
# ADD CPP /D "D3D_OVERLOADS"
# SUBTRACT CPP /D "_PC"
# End Source File
# Begin Source File

SOURCE=.\text.cpp
# End Source File
# Begin Source File

SOURCE=.\textstrings.cpp
# End Source File
# Begin Source File

SOURCE=.\TrackEdit.cpp

!IF  "$(CFG)" == "TrackEdit - Win32 Release"

!ELSEIF  "$(CFG)" == "TrackEdit - Win32 Debug"

# ADD CPP /FR

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\winmain.cpp
# End Source File
# Begin Source File

SOURCE=.\winmain.rc
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Group "Game"

# PROP Default_Filter ".h"
# Begin Source File

SOURCE=..\Game\source\inc\ainode.h
# End Source File
# Begin Source File

SOURCE=..\Game\source\inc\draw.h
# End Source File
# Begin Source File

SOURCE=..\Game\source\inc\editobj.h
# End Source File
# Begin Source File

SOURCE=..\Game\source\inc\light.h
# End Source File
# Begin Source File

SOURCE=..\Game\source\inc\NewColl.h
# End Source File
# Begin Source File

SOURCE=..\Game\source\inc\object.h
# End Source File
# Begin Source File

SOURCE=..\Game\source\inc\revolt.h
# End Source File
# Begin Source File

SOURCE=..\Game\source\inc\TypeDefs.h
# End Source File
# Begin Source File

SOURCE=..\Game\source\inc\world.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\compile.h
# End Source File
# Begin Source File

SOURCE=.\Editor.h
# End Source File
# Begin Source File

SOURCE=.\editorconstants.h
# End Source File
# Begin Source File

SOURCE=.\editorhelp.h
# End Source File
# Begin Source File

SOURCE=..\..\bobclass\inc\endstrm.h
# End Source File
# Begin Source File

SOURCE=.\fileio.h
# End Source File
# Begin Source File

SOURCE=.\inputs.h
# End Source File
# Begin Source File

SOURCE=.\modules.h
# End Source File
# Begin Source File

SOURCE=.\pcincl.h
# End Source File
# Begin Source File

SOURCE=.\platform.h
# End Source File
# Begin Source File

SOURCE=.\Render.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\states.h
# End Source File
# Begin Source File

SOURCE=.\text.h
# End Source File
# Begin Source File

SOURCE=.\textstrings.h
# End Source File
# Begin Source File

SOURCE=.\TrackEditTypes.h
# End Source File
# Begin Source File

SOURCE=.\UnitInfo.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\DirectX.ico
# End Source File
# End Group
# Begin Source File

SOURCE=.\trackunits.vsd
# End Source File
# End Target
# End Project
