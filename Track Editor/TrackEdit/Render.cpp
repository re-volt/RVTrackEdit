#include <assert.h>
#include "platform.h"
#include "Render.h"
#include "editorconstants.h"
#include "UnitInfo.h"
#include "states.h"
#include "textstrings.h"
#include "editorhelp.h"
#include <time.h>

extern int					g_DefaultScreenWidth;
extern int					g_DefaultScreenHeight;
extern int					g_ActualScreenWidth;
extern int					g_ActualScreenHeight;

extern LPDIRECT3DDEVICE3	D3DDevice;

extern POPUP_MENU_STATES	PopupState;
extern POPUP_MENU_ACTIONS	PopupAction;
extern U16					PopupMenuPosition;
extern REAL					PopupMenuTime;
extern U16					PopupCursorFrame;
extern U16					PopupCursorPosition;
extern REAL					PopupCursorTime;
extern COMPILE_STATES		CompileState;
extern SAVE_STATES			SaveState;
extern U8					ClockFrame;

extern PLACEMENT_CAMERA_DIR	PlacementViewpoint;
extern REVOLTVECTOR			CameraPos;
extern REAL					CameraVelocity;
extern REVOLTVECTOR			Viewpoints[4];
extern REAL					ButtonFlashTime[4];

extern CURSORDESC			PickupCursor;
extern CURSORDESC			ModuleCursor;
extern CURSORDESC			TrackCursor;
extern TRACKDESC			TrackData;
extern U16					FileWindowStart;
extern U16					FileWindowOffset;
extern char**				FileList;
extern U16					FileCount;
extern char					SaveName[MAX_DESCRIPTION_LENGTH];
extern U16					SaveSymbol;
#ifdef NEW_WALLS
extern REVOLTMATRIX			WallMatrix[6];
#else
extern REVOLTMATRIX			WallMatrix[5];
#endif

//------------------------------------------------------------------

static const REAL CLOCK_LEFT = 240;
static const REAL CLOCK_TOP = 140;
static const REAL CLOCK_RIGHT = CLOCK_LEFT + 127;
static const REAL CLOCK_BOTTOM = CLOCK_TOP + 127;

static const U16  MENU_TOP = 32;
static const COIN_OFFSET = 19;

const U16 GROUP_TEXT_TOP = MENU_TOP + 33;
const U16 GROUP_SPACING = 24;

const U16 LOAD_MENU_LEFT = 160; //absolute position (in pixels) of the top
const U16 LOAD_MENU_TOP = 128;	//and left of the load menu
const U16 LOAD_MENU_WIDTH = 10; //# of 32 pixels blocks to construct load menu from
const U16 LOAD_MENU_HEIGHT = 8; //in the 2 dimensions
const U16 LOAD_MENU_ARROW_LEFT = (LOAD_MENU_LEFT + ((LOAD_MENU_WIDTH / 2) * 32)) - 16;
const U16 LOAD_MENU_ARROW_TOP = LOAD_MENU_TOP + 16;
const U16 LOAD_MENU_ARROW_BOTTOM = (LOAD_MENU_TOP + (LOAD_MENU_HEIGHT - 1) * 32) - 16;
const U16 LOAD_MENU_TEXT_LEFT = LOAD_MENU_LEFT + 16;
const U16 LOAD_MENU_TEXT_TOP = LOAD_MENU_TOP + 56;

const U16 SAVE_MENU_LEFT = LOAD_MENU_LEFT - 32; //absolute position (in pixels) of the top
const U16 SAVE_MENU_TOP = LOAD_MENU_TOP;	//and left of the load menu
const U16 SAVE_MENU_WIDTH = LOAD_MENU_WIDTH + 2; //# of 32 pixels blocks to construct load menu from
const U16 SAVE_MENU_HEIGHT = LOAD_MENU_HEIGHT; //in the 2 dimensions

const U16 ERROR_BOX_LEFT = 32;
const U16 ERROR_BOX_TOP = 288;
const U16 ERROR_BOX_WIDTH = 12;
const U16 ERROR_BOX_HEIGHT = 3;

const U16 HELP_TITLE_LEFT = 32;
const U16 HELP_TITLE_TOP = 16;
const U16 HELP_BOX_LEFT = 32;
const U16 HELP_BOX_TOP = 64;

extern FONTDESCRIPTION	SprueWire1Font;
extern FONTDESCRIPTION	SprueWire2Font;
extern FONTDESCRIPTION	SprueTextFont;

static INDEX ScreenStateText [] = 
{
	TEXT_MODE_MODULES,
	TEXT_MODE_PICKUPS,
	TEXT_MODE_ADJUST,
	TEXT_MODE_SAVE,
	TEXT_MODE_NEW,
	TEXT_MODE_LOAD,
	TEXT_MODE_EXPORT,
	TEXT_MODE_QUIT,
	TEXT_MODE_PLACING,
	TEXT_MODE_DELETING,
};

void UseIconTexture(void)
{
	SetCurrentTexture(TEX_ICONS);
}

void UseButtonTexture(void)
{
	SetCurrentTexture(TEX_SPRUE_BUTTONS);
}

void UseClockTexture(void)
{
	SetCurrentTexture(TEX_ALARM_CLOCK);
}

//**********************************************
//
// CalculateWallMatrices
//
// Simply calculate sthe matrices needed to
// move the walls in so that they enclose the
// track as tightly as possible
//
//**********************************************
void CalculateWallMatrices(const TRACKDESC* track)
{
	assert(track != NULL);

	MAKE_IDENTITY_MATRIX(WallMatrix[0]);
	MAKE_IDENTITY_MATRIX(WallMatrix[1]);
	MAKE_IDENTITY_MATRIX(WallMatrix[2]);
	MAKE_IDENTITY_MATRIX(WallMatrix[3]);
	MAKE_IDENTITY_MATRIX(WallMatrix[4]);
#ifdef NEW_WALLS
	MAKE_IDENTITY_MATRIX(WallMatrix[5]);
#endif

	REAL xshift = -(Real(20.0f) - track->Width) * SMALL_CUBE_SIZE;
	REAL zshift = -(Real(20.0f) - track->Height) * SMALL_CUBE_SIZE;

#ifdef NEW_WALLS
	MAKE_TRANSLATE_MATRIX(WallMatrix[1], 0, 0, zshift);
#endif
	
	MAKE_TRANSLATE_MATRIX(WallMatrix[3], xshift, 0, zshift);
#ifdef NEW_WALLS
	MAKE_TRANSLATE_MATRIX(WallMatrix[4], xshift /*-(8 * SMALL_CUBE_SIZE)*/, 0, zshift);
#else
	MAKE_TRANSLATE_MATRIX(WallMatrix[4], 0, 0, zshift);
#endif

#ifdef NEW_WALLS
	MAKE_TRANSLATE_MATRIX(WallMatrix[5], xshift, 0, 0);
#endif
}

void DrawErrorMessage(void)
{
	DrawFilledFrame(ERROR_BOX_LEFT, ERROR_BOX_TOP, ERROR_BOX_WIDTH, ERROR_BOX_HEIGHT);

	SetSpriteColor(PURE_WHITE);

	SetCurrentFont(&SprueWire1Font);
	DrawText(ERROR_BOX_LEFT, ERROR_BOX_TOP - 32, "/7QQQQQQ80");

	UseIconTexture();
	DrawSprite(ERROR_BOX_LEFT + 208, ERROR_BOX_TOP - 160, ERROR_BOX_LEFT + 336, ERROR_BOX_TOP - 32, ZERO, ZERO, HALF, HALF);

	SetCurrentFont(&SprueTextFont);

	const char* errortext = GetErrorText();
	assert(strlen(errortext) != 0);		//ensure that the user has specified a suitable error message
	SetSpriteColor(PURE_WHITE);
	DrawText( ERROR_BOX_LEFT + 16, ERROR_BOX_TOP + 32, errortext);

	DrawText(32, 434, TEXT_DIALOG_WARNING);

}

void DrawQueryMessage(void)
{
	DrawFilledFrame(ERROR_BOX_LEFT, ERROR_BOX_TOP, ERROR_BOX_WIDTH, ERROR_BOX_HEIGHT);

	SetSpriteColor(PURE_WHITE);

	SetCurrentFont(&SprueWire1Font);
	DrawText(ERROR_BOX_LEFT, ERROR_BOX_TOP - 32, "/7QQQQQQ80");

	UseIconTexture();
	DrawSprite(ERROR_BOX_LEFT + 208, ERROR_BOX_TOP - 160, ERROR_BOX_LEFT + 336, ERROR_BOX_TOP - 32, HALF, ZERO, ONE, HALF);

	SetCurrentFont(&SprueTextFont);
	const char* querytext = GetQueryText();
	assert(strlen(querytext) != 0);		//ensure that the user has specified a suitable query message
	DrawText( ERROR_BOX_LEFT + 16, ERROR_BOX_TOP + 32, querytext);

	SetCharBase(WHITE_FONT);
	if(QuerySetToYes())
	{
		SetCharBase(RED_FONT);
	}
	DrawMoreText(TEXT_OPTION_YES);
	SetCharBase(WHITE_FONT);
	DrawMoreText("/");
	if(!QuerySetToYes())
	{
		SetCharBase(RED_FONT);
	}
	DrawMoreText(TEXT_OPTION_NO);
	SetCharBase(WHITE_FONT);

	DrawText(32, 434, TEXT_DIALOG_QUESTION);

}

void DrawNotificationMessage(void)
{
	DrawFilledFrame(ERROR_BOX_LEFT, ERROR_BOX_TOP, ERROR_BOX_WIDTH, ERROR_BOX_HEIGHT);

	SetSpriteColor(PURE_WHITE);

	SetCurrentFont(&SprueWire1Font);
	DrawText(ERROR_BOX_LEFT, ERROR_BOX_TOP - 32, "/7QQQQQQQ0");

	SetCurrentFont(&SprueTextFont);
	const char* notficationtext = GetNotificationText();
	assert(strlen(notficationtext) != 0);		//ensure that the user has specified a suitable query message
	DrawText( ERROR_BOX_LEFT + 16, ERROR_BOX_TOP + 32, notficationtext);

	DrawText(32, 434, TEXT_DIALOG_INFO);

}

void DrawStaticClock(U16 bluespotcount)
{
	bluespotcount = min(bluespotcount, 12);

	//draw the main body of the clock
	DrawSprite(CLOCK_LEFT, CLOCK_TOP, CLOCK_RIGHT, CLOCK_BOTTOM, ZERO, ZERO, HALF, HALF);

	static const REAL spotdiameter = Real(0.0625f);
	
	//now draw the spots indicating current processing stage
	REAL srcleft = HALF;	//initialize the source UVs to point to the yellow spot
	REAL srctop = ZERO;
	REAL srcright = srcleft + spotdiameter;
	REAL srcbottom = srctop + spotdiameter;
	REAL destleft;
	REAL desttop;
	REAL destright;
	REAL destbottom;
	static const REAL spotleft[12] = {63, 72, 75, 72, 64, 52, 40, 31, 28, 32, 41, 52};
	static const REAL spottop[12] = {44, 52, 63, 74, 83, 86, 83, 75, 63, 52, 44, 41};

	U16 spot = 12;
	while(spot--)
	{
		if(bluespotcount == (spot + 1)) //if we\"ve reached the first blue spot, change the source UVs to point to the blue spot
		{
			srcleft += spotdiameter;
			srcright += spotdiameter;
		}
		destleft = spotleft[spot] + CLOCK_LEFT;
		desttop = spottop[spot] + CLOCK_TOP;
		destright = destleft + 16;
		destbottom = desttop + 16;
		DrawSprite(destleft, desttop, destright, destbottom, srcleft, srctop, srcright, srcbottom);
	}
}

void DrawRingingClock(void)
{
	REAL srcleft = ZERO;
	REAL srctop = HALF;
	REAL srcright = HALF;
	REAL srcbottom = ONE;
	if(ClockFrame & 0x04)
	{
		srcleft = HALF;
		srcright = ONE;
	}

	DrawSprite(CLOCK_LEFT, CLOCK_TOP, CLOCK_RIGHT, CLOCK_BOTTOM, srcleft, srctop, srcright, srcbottom);
}

void DrawClock(void)
{
	UseClockTexture();

	switch(CompileState)
	{
		case TCS_RINGING_CLOCK:
			DrawRingingClock();
		break;

		case TCS_FINISHED:
			DrawStaticClock(12);
		break;

		case TCS_UNIT_CUBES:
		case TCS_SMALL_CUBES:
		case TCS_BIG_CUBES:
		case TCS_MAKING_COLLISONS:
		case TCS_OPTIMIZING:
		case TCS_WRITING_WORLD:
		case TCS_WRITING_INF:
		case TCS_WRITING_BITMAPS:
		case TCS_WRITING_NCP:
		case TCS_WRITING_TAZ:
		case TCS_CLEANUP:
			DrawStaticClock(CompileState);
		break;
	}
}

void DrawExportingScreen(void)
{
    static INDEX compile_state_text[]=
	{
		TEXT_COMPILE_STAGE_1,
		TEXT_COMPILE_STAGE_2,
		TEXT_COMPILE_STAGE_3,
		TEXT_COMPILE_STAGE_4,
		TEXT_COMPILE_STAGE_5,
		TEXT_COMPILE_STAGE_6,
		TEXT_COMPILE_STAGE_7,
		TEXT_COMPILE_STAGE_8,
		TEXT_COMPILE_STAGE_9,
		TEXT_COMPILE_STAGE_10,
		TEXT_COMPILE_STAGE_11,
		TEXT_COMPILE_STAGE_12,
		TEXT_COMPILE_FINISHED,
		TEXT_COMPILE_FINISHED
	};
	if(CompileState != TCS_VALIDATING)
	{
		SetSpriteColor(PURE_WHITE);

		SetCurrentFont(&SprueWire1Font);
		DrawText(ERROR_BOX_LEFT, ERROR_BOX_TOP - 32, "/7QQQQQQ80");
		DrawFilledFrame(32, 288, 4, 2);

		SetCurrentFont(&SprueTextFont);
		DrawText( 48, 312, compile_state_text[CompileState]);
		DrawClock();
	}
}

void DrawLoadMenu(void)
{
	DrawFilledFrame(LOAD_MENU_LEFT, LOAD_MENU_TOP, LOAD_MENU_WIDTH, LOAD_MENU_HEIGHT);

	CHAR menuentry[80];

	REAL arrowoffsets[4] = {ZERO, ZERO, ZERO, ZERO};
	
	for(U16 n = 0; n < 4; n++)
	{
		if(ButtonFlashTime[n] > ZERO)
		{
			arrowoffsets[n] = 0.125f;
		}
	}

	SetCurrentFont(&SprueTextFont);

    U32 color;
	for(U16 offset = 0; offset < MAX_FILES_IN_WINDOW; offset++)
	{
		color = PURE_WHITE;
		U16 n = FileWindowStart + offset;
		if(n < FileCount)
		{
			strcpy(menuentry, FileList[n]);
			if(offset == FileWindowOffset)
			{
				color = PURE_RED;
			}
			SetSpriteColor(color);
			DrawText( LOAD_MENU_TEXT_LEFT , LOAD_MENU_TEXT_TOP + (offset * FontHeight()), FileList[n] );
		}
	}
	SetSpriteColor(PURE_WHITE);

	SetCurrentFont(&SprueWire2Font);

	if(FileWindowStart > 0)
	{
		DrawSprite(LOAD_MENU_ARROW_LEFT, LOAD_MENU_ARROW_TOP, LOAD_MENU_ARROW_LEFT + 32, LOAD_MENU_ARROW_TOP + 32, ZERO + arrowoffsets[NORTH], 0.75f, 0.125f + arrowoffsets[NORTH], 0.875f);
	}
	if((FileWindowStart + MAX_FILES_IN_WINDOW) < FileCount)
	{
		DrawSprite(LOAD_MENU_ARROW_LEFT, LOAD_MENU_ARROW_BOTTOM, LOAD_MENU_ARROW_LEFT + 32, LOAD_MENU_ARROW_BOTTOM + 32, ZERO + arrowoffsets[SOUTH], 0.875f, 0.125f + arrowoffsets[SOUTH], 0.75f);
	}
}

void DrawSaveMenu(void)
{
	DrawFilledFrame(SAVE_MENU_LEFT, SAVE_MENU_TOP, SAVE_MENU_WIDTH, SAVE_MENU_HEIGHT);

	SetCurrentFont(&SprueWire1Font);
	
	DrawText(SAVE_MENU_LEFT + 32, SAVE_MENU_TOP + 32, "<========>");

	SetCurrentFont(&SprueTextFont);

	CHAR savenamebuffer[80];
	static const long MARK = 600;
	static const long SPACE = 400;

	SetSpriteColor(PURE_WHITE);
	strcpy(savenamebuffer, SaveName);
	long time = clock();
	if((time % (MARK + SPACE)) < MARK)
	{
		strcat(savenamebuffer, "_");
	}
	else
	{
		strcat(savenamebuffer, " ");
	}
	DrawText(SAVE_MENU_LEFT + 52, SAVE_MENU_TOP + 40, savenamebuffer );

	if(SaveState == SS_CONFIRM)
	{
		SetSpriteColor(PURE_RED);
	}
	DrawText(SAVE_MENU_LEFT + 160, SAVE_MENU_TOP + 184, TEXT_CONFIRM_SAVE);
	SetSpriteColor(PURE_WHITE);
	
	char symbol[] = "*"; //symbol to be plotted at a position in the save name grid (initializes with placeholder char)
	for(U16 c = 0; c < 96; c++)	//for all of the chars from '!' thru ('~' + 2)
	{
		U16 x = (c % 16) * 16;
		U16 y = (c / 16) * 16;
		symbol[0] = c + 33;
		if((c == SaveSymbol) && (SaveState == SS_PICK))
		{
			SetSpriteColor(PURE_RED);
		}
		DrawText(SAVE_MENU_LEFT + 72 + x, SAVE_MENU_TOP + 72 + y, symbol);
		if(c == SaveSymbol)
		{
			SetSpriteColor(PURE_WHITE);
		}
	}

	SetSpriteColor(PURE_WHITE);
}

void DrawChoosingScreen(const TRACKTHEME* theme)
{
	
	DrawFrame(96, 128, 9, 9);

	SetCurrentFont(&SprueWire1Font);
	U16 x = (g_DefaultScreenWidth - PopupSlideDistance);
	U16 y = MENU_TOP;
	DrawText(x, y,		"$%%%&");
	for(U16 n = 1; n < 12; n++)
	{
		y += 32;
		DrawText(x, y , ",---.");
	}
	DrawText(x, y + 32, "45556");

	U16 groupcount = theme->Lookup->GroupCount;
	while(groupcount--)
	{
		DrawText(x , GROUP_TEXT_TOP + (groupcount * GROUP_SPACING) - 8, "'===");
		if(theme->Lookup->Groups[groupcount].ModuleID != theme->Lookup->Groups[groupcount].AlternateID)
		{
			DrawText(x + 23 , GROUP_TEXT_TOP + (groupcount * GROUP_SPACING) - 8, "   (");
		}
		else
		{
			DrawText(x + 23 , GROUP_TEXT_TOP + (groupcount * GROUP_SPACING) - 8, "   X");
		}
	}

	SetCurrentFont(&SprueTextFont);

	SetSpriteColor(PURE_WHITE);
	groupcount = theme->Lookup->GroupCount;
	while(groupcount--)
	{
		if(groupcount == ModuleCursor.Y)
		{
			SetCharBase(RED_FONT);
		}
		DrawText(x + 16, GROUP_TEXT_TOP + (groupcount * GROUP_SPACING), theme->Lookup->Groups[groupcount].TextIndex);
		if(groupcount == ModuleCursor.Y)
		{
			SetCharBase(WHITE_FONT);
		}
	}

	SetCurrentFont(&SprueWire2Font);

	groupcount = theme->Lookup->GroupCount;
	y =  GROUP_TEXT_TOP + (groupcount * GROUP_SPACING);
	y--;
	x += 119;
	while(groupcount--)
	{
		y -= GROUP_SPACING;
		if(theme->Lookup->Groups[groupcount].ModuleID != theme->Lookup->Groups[groupcount].AlternateID)
		{
			DrawSprite(x, y, x + 16, y + 18, 0.6875f, 0.75f, 0.75f, 0.8125f);
			DrawSprite(x + 12, y, x + 28, y + 18, 0.625f, 0.75f, 0.6875f, 0.8125f);
			if(groupcount == ModuleCursor.Y)
			{
				U16 cx = (x - 1) + (ModuleCursor.X * 12);
				DrawSprite(cx, y, cx + 16, y + 18, 0.75f, 0.75f, 0.8125f, 0.8125f);
			}
		}
	}

	DrawSpinningModule(theme);
}

void DrawPlacingScreen(const TRACKTHEME* theme)
{
	static REVOLTVECTOR	up = {ZERO, ONE, ZERO};
	REVOLTMATRIX		camera;
	REVOLTVECTOR		from;
	REVOLTVECTOR		at = {ZERO, ZERO, ZERO};

	REVOLTMATRIX TransMatrix;
	REVOLTMATRIX IdentityMatrix;

	at.X = TrackCursor.X * SMALL_CUBE_SIZE;
	at.Y = 0;
	at.Z = TrackCursor.Y * SMALL_CUBE_SIZE;
	
	MAKE_TRANSLATE_MATRIX(TransMatrix, at.X, 0, at.Z);
	
	TRANSFORM_VECTOR(from, CameraPos, TransMatrix);
	MAKE_VIEW_MATRIX(camera, from, at, up);
	SET_VIEW_MATRIX(camera);

	DrawTrack(theme, &TrackData);
	
	SetCurrentTexture(TEX_NOT_ASSIGNED);
	SET_WORLD_MATRIX(TransMatrix);
	DrawCursorBox();
}

void DrawPlanViewOfTrack(const TRACKTHEME* theme)
{
	static REVOLTVECTOR	up = {ZERO, ZERO, -ONE};
	static REVOLTVECTOR	from = {ZERO, -CameraHeight * REAL(6.5f), ZERO};
	static REVOLTVECTOR	at = {ZERO, ZERO, ZERO};
	REVOLTMATRIX			camera;

	REAL trackwidth = TrackData.Width * SMALL_CUBE_SIZE;
	REAL trackheight = TrackData.Height * SMALL_CUBE_SIZE;
	
	at.X = from.X = (trackwidth / REAL(2.0f)) + (SMALL_CUBE_SIZE * 5);
	at.Z = from.Z = (trackheight / REAL(2.0f)) + (SMALL_CUBE_SIZE * 2.25f);

	MAKE_VIEW_MATRIX(camera, from, at, up);
	SET_VIEW_MATRIX(camera);

	DrawTrack(theme, &TrackData);

}

void DrawAdjustmentScreen(const TRACKTHEME* theme)
{
	REAL arrowoffsets[4] = {ZERO, ZERO, ZERO, ZERO};
	
	for(U16 n = 0; n < 4; n++)
	{
		if(ButtonFlashTime[n] > ZERO)
		{
			arrowoffsets[n] = 0.125f;
		}
	}
	
	DrawPlanViewOfTrack(theme);
	
	if(!HelpExists() && ((GetAsyncKeyState(VK_SHIFT) & 0x8000) != 0))
	{
		SetCurrentFont(&SprueWire2Font);
	
		//plot the icons for shifting the track around
		DrawSprite(240, 120, 271, 151, 0.0f + arrowoffsets[NORTH], 0.75f, 0.125f + arrowoffsets[NORTH], 0.875f);
		DrawSprite(376, 258, 407, 289, 0.0f + arrowoffsets[EAST], 0.875f, 0.125f + arrowoffsets[EAST], ONE);
		DrawSprite(102, 258, 133, 289, 0.75f - arrowoffsets[WEST], 0.875f, 0.875f - arrowoffsets[WEST], ONE);
		DrawSprite(240, 396, 271, 427, 0.0f + arrowoffsets[SOUTH], 0.875f, 0.125f + arrowoffsets[SOUTH], 0.75f);
	}
	else
	{
		DrawFilledFrame(448, 352, 5, 3);
	
		SetCurrentFont(&SprueWire2Font);

		//plot the icons for resizing the track
		DrawSprite(240, 120, 271, 136, 0.25f + arrowoffsets[NORTH], 0.75f, 0.375f + arrowoffsets[NORTH], 0.8125f);
		DrawSprite(240, 136, 271, 152, 0.25f + arrowoffsets[SOUTH], 0.8125f, 0.375f + arrowoffsets[SOUTH], 0.875f);
		DrawSprite(376, 258, 392, 289, 0.25f + arrowoffsets[WEST], 0.875f, 0.3125f + arrowoffsets[WEST], ONE);
		DrawSprite(392, 258, 408, 289, 0.3125f + arrowoffsets[EAST], 0.875f, 0.375f + arrowoffsets[EAST], ONE);

		SetCurrentFont(&SprueTextFont);
		char sizeinfo[40];
		sprintf(sizeinfo, GetTextString(TEXT_ADJUST_LEGEND), TrackData.Width, TrackData.Height);
		DrawText(480, 384, sizeinfo);
	}
}

void DrawMenuSprue(U16 x)
{
	SpriteClippingOn();
	U16 y = MENU_TOP;
	SetCurrentFont(&SprueWire1Font);
	DrawText(x, y,       "!\"\"\"#");
	DrawText(x, y + 32,  "1   3");
	DrawText(x, y + 64,  "1   3");
	DrawText(x, y + 96,  "1   3");
	DrawText(x, y + 128, "1   3");
	DrawText(x, y + 160, "1   3");
	DrawText(x, y + 192, "1   3");
	DrawText(x, y + 224, "1   3");
	DrawText(x, y + 256, "1   3");
	DrawText(x, y + 288, "1   3");
	DrawText(x, y + 320, "1   3");
	DrawText(x, y + 352, "D   3");
	DrawText(x, y + 384, "9:::;");

	DrawText(x, y + 32,  ") 2*+");
	DrawText(x, y + 80,  ") 2*+");
	DrawText(x, y + 128, ") 2*+");
	DrawText(x, y + 176, ") 2*+");
	DrawText(x, y + 224, ") 2*+");
	DrawText(x, y + 272, ") 2*+");
	DrawText(x, y + 320, ") 2*+");
	DrawText(x, y + 367, "  2*");
	DrawText(x, y + 352, "    @");
	DrawText(x, y + 384, "    H");

	x += 82;
	y += 44;
	SetCurrentFont(&SprueTextFont);
	for(U16 n = 0; n < ESS_PLACING_MODULE; n++)
	{
		DrawText(x, y, ScreenStateText[n]);
		y += 48;
	}

	SpriteClippingOff();

	if(PopupState != PMS_INACTIVE)
	{
		DrawText(24, 434, TEXT_MODE_MENU);
	}
}

void DrawMainSprue(void)
{
	SetCurrentFont(&SprueWire2Font);

	SetSpriteColor(PURE_WHITE);

	DrawText( 0, 32, "!\"#$%&'");
	DrawText( 0, 64, ")*+,-./");
	DrawText( 0, 96, "1234567");

	SetCurrentFont(&SprueWire1Font);
	
	DrawText( 64, 0, " YYYYYYYYYYYYYYYY?");
	DrawText( 70, 10, "!");
	DrawText( 608, 32, "Z\n\rZ\n\rZ\n\rZ\n\rZ\n\rZ\n\rZ\n\rZ\n\rZ\n\rZ\n\rZ\n\rZ\n\rZ");
	DrawText( 384, 448, "/QQQQQQG");
	DrawText( 0, 416, "BEEEEF  A");
	DrawText( 0, 426, "         QQQQ0");
	DrawText( 0, 448, "JKLMMNPQI^VW");
	DrawText( 0, 128, "C\n\rR\n\rR\n\rR\n\rR\n\rR\n\rR\n\rR\n\rR");

	if(!ErrorExists() && !QueryExists() && !NotificationExists() && (PopupState == PMS_INACTIVE))
	{
		SetCurrentFont(&SprueTextFont);

		DrawText(24, 434, ScreenStateText[GetScreenState()]);

		if(GetScreenState() == ESS_ADJUSTING_TRACK)
		{
			if(!HelpExists() && ((GetAsyncKeyState(VK_SHIFT) & 0x8000) != 0))
			{
				DrawMoreText(TEXT_ADJUST_POSITION);
			}
			else
			{
				DrawMoreText(TEXT_ADJUST_SIZE);
			}
		}
	}
}

void DrawCursorCoin(U16 x, U16 y, U16 frame)
{
	static const REAL cointexturesize = Real(0.1875f);
	static const REAL coinpixelsize = Real(48.0f);

	x += COIN_OFFSET;

	if(x < g_DefaultScreenWidth)
	{
		REAL coinpixelwidth = coinpixelsize;
		if((x + coinpixelsize) > g_DefaultScreenWidth)
		{
			coinpixelwidth = g_DefaultScreenWidth - x;
		}

		REAL cointexturewidth = cointexturesize * (coinpixelwidth / coinpixelsize);
		frame = 15 - frame;
		REAL u = (frame % 4) * cointexturesize;
		REAL v = (frame / 4) * cointexturesize;

		DrawSprite(x, y, x + coinpixelwidth, y + coinpixelsize, u, v, u + cointexturewidth, v + cointexturesize);
	}
}

void DrawPopupMenu(void)
{
	static const U16 buttonspacing = 48;
	static const REAL coinpixelsize = Real(48.0f);
	REAL cursorcolumn = (g_DefaultScreenWidth - PopupSlideDistance) + PopupMenuPosition;
	REAL cursorrow = (g_DefaultScreenHeight - ((7 * buttonspacing) + 48)) / 2;

	U16 frame;
	if((PopupState == PMS_ACTIVE) || (PopupState == PMS_APPEARING) || (PopupState == PMS_DISAPPEARING))
	{
		UseButtonTexture();
		for(U16 s = 0; s < ESS_PLACING_MODULE; s++)
		{
			frame = 8;
			if(s == PopupCursorPosition)
			{
				frame = 0;
			}
			DrawCursorCoin(cursorcolumn, (s * buttonspacing) + cursorrow, frame);
		}
		DrawMenuSprue(cursorcolumn);
	}
	if(PopupState == PMS_MOVING_UP)
	{
		UseButtonTexture();
		for(U16 s = 0; s < ESS_PLACING_MODULE; s++)
		{
			frame = 8;
			if(s == (PopupCursorPosition + 1) % (ESS_QUITTING + 1))
			{
				frame = PopupCursorFrame;
			}
			else
			{
				if(s == PopupCursorPosition)
				{
					frame = PopupCursorFrame + 8;
				}
			}
			DrawCursorCoin(cursorcolumn, (s * buttonspacing) + cursorrow, frame);
		}
		DrawMenuSprue(cursorcolumn);
	}
	if(PopupState == PMS_MOVING_DOWN)
	{
		UseButtonTexture();
		for(U16 s = 0; s < ESS_PLACING_MODULE; s++)
		{
			frame = 8;
			if(s == (PopupCursorPosition + ESS_QUITTING) % (ESS_QUITTING + 1))
			{
				frame = 15 - PopupCursorFrame;
			}
			else
			{
				if(s == PopupCursorPosition)
				{
					frame = 7 - PopupCursorFrame;
				}
			}
			DrawCursorCoin(cursorcolumn, (s * buttonspacing) + cursorrow, frame);
		}
		DrawMenuSprue(cursorcolumn);
	}
}

void DrawFrame(REAL xpix, REAL ypix, U16 cellwidth, U16 cellheight)
{
	assert(cellwidth <= 20);
	assert(cellheight <=15);

	static const spritesize = 32;
	
	static char top[]	 = "!\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"#";
	static char bottom[] = "9::::::::::::::::::;";
	static char left[]	 = "!\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r1\n\r9\n\r";
	static char right[]	 = "#\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r3\n\r;\n\r";
	
	char framebuffer[60];
	
	SetCurrentFont(&SprueWire1Font);

	//top of frame (minus top left corner)
	DrawText(xpix + spritesize, ypix, &top[21 - cellwidth]);

	//bottom of frame (minus bottom right corner)
	strncpy(framebuffer, bottom, cellwidth - 1);
	framebuffer[cellwidth - 1] = '\0';
	DrawText(xpix, ypix + ((cellheight - 1) * spritesize), framebuffer);

	int charstocut = (cellheight - 1) * 3;

	//left of frame (minus bottom left corner)
	strcpy(framebuffer, "");
	strncat(framebuffer, left, charstocut);
	DrawText(xpix, ypix , framebuffer);

	//right of frame (minus bottom right corner)
	strcpy(framebuffer, "");
	strncat(framebuffer, &right[strlen(right) - charstocut], charstocut);
	DrawText(xpix + ((cellwidth - 1) * spritesize), ypix + spritesize, framebuffer);
}

void DrawFilledFrame(REAL xpix, REAL ypix, U16 cellwidth, U16 cellheight)
{
	assert(cellwidth <= 20);
	assert(cellheight <= 15);

	static const spritesize = 32;
	
	static char top[]	 = "$%%%%%%%%%%%%%%%%%%&";
	static char bottom[] = "45555555555555555556";
	static char left[]	 = "$\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r,\n\r4\n\r";
	static char right[]	 = "&\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r.\n\r6\n\r";
	static char filler[] = "------------------";
		
	char framebuffer[60];
	
	SetCurrentFont(&SprueWire1Font);

	//top of frame (minus top left corner)
	DrawText(xpix + spritesize, ypix, &top[21 - cellwidth]);

	//bottom of frame (minus bottom right corner)
	strncpy(framebuffer, bottom, cellwidth - 1);
	framebuffer[cellwidth - 1] = '\0';
	DrawText(xpix, ypix + ((cellheight - 1) * spritesize), framebuffer);

	int charstocut = (cellheight - 1) * 3;

	//left of frame (minus bottom left corner)
	strcpy(framebuffer, "");
	strncat(framebuffer, left, charstocut);
	DrawText(xpix, ypix , framebuffer);

	//right of frame (minus bottom right corner)
	strcpy(framebuffer, "");
	strncat(framebuffer, &right[strlen(right) - charstocut], charstocut);
	DrawText(xpix + ((cellwidth - 1) * spritesize), ypix + spritesize, framebuffer);

	//now fill in centre of frame with solid purple
	for(U16 row = 1 ; row < cellheight - 1; row++)
	{
		DrawText(xpix + spritesize, ypix + (row * spritesize), &filler[18 - (cellwidth - 2)]);
	}
}

void DrawHelpInfo(void)
{
	if(HelpExists())
	{
		const HELP_DIALOG_INFO* helpinfo = CurrentHelp();
		if(helpinfo != NULL)
		{
			REAL x = helpinfo->DialogXPos;
			REAL y = helpinfo->DialogYPos;
			DrawFilledFrame(x, y, helpinfo->DialogWidth, helpinfo->DialogHeight);

			SetCurrentFont(&SprueTextFont);

			SetSpriteColor(PURE_WHITE);
			
			DrawText( HELP_TITLE_LEFT + x, HELP_TITLE_TOP + y, TEXT_HELP_TITLE);
			
			DrawText( HELP_BOX_LEFT + x, HELP_BOX_TOP + y, helpinfo->KeyText);
			DrawText( HELP_BOX_LEFT + x + helpinfo->ActionXPos, HELP_BOX_TOP + y, helpinfo->ActionText);
		}
	}
}