#ifndef _PCINCL_H
#define _PCINCL_H

#ifdef _PC

#ifndef D3D_OVERLOADS
#define D3D_OVERLOADS
#endif
#include <ddraw.h>
#include <d3d.h>
#include <d3drm.h>
#include <dinput.h>
#include <d3dutil.h>
#include <d3dmath.h>
#include <d3dtextr.h>
#include <d3dframe.h>
#include <d3dclass.h>

#define REVOLTMATRIX D3DMATRIX
#define REVOLTVECTOR RevoltVertex
#define TRANSFORM_VECTOR(dest, src, matrix) D3DMath_VectorMatrixMultiply((D3DVECTOR&)(dest), (D3DVECTOR&)(src), (D3DMATRIX&)(matrix))
#define MATRIX_MULTIPLY(dest, src1, src2) D3DMath_MatrixMultiply((D3DMATRIX&)(dest), (D3DMATRIX&)(src1), (D3DMATRIX&)(src2))
#define MAKE_IDENTITY_MATRIX(dest) D3DUtil_SetIdentityMatrix((D3DMATRIX&)(dest))
#define MAKE_ROTATE_Y_MATRIX(dest, angle) D3DUtil_SetRotateYMatrix((D3DMATRIX&)(dest), (angle))
#define MAKE_TRANSLATE_MATRIX(dest, x, y, z) D3DUtil_SetTranslateMatrix((D3DMATRIX&)(dest), (x), (y), (z))
#define MAKE_SCALE_MATRIX(dest, xscale, yscale, zscale) D3DUtil_SetScaleMatrix((D3DMATRIX&)(dest), (xscale), (yscale), (zscale))
#define MAKE_VIEW_MATRIX(dest, from, at, up) D3DUtil_SetViewMatrix((D3DMATRIX&)(dest), (D3DVECTOR&)(from), (D3DVECTOR&)(at), (D3DVECTOR&)(up))
#define	SET_VIEW_MATRIX(target) D3DDevice->SetTransform(D3DTRANSFORMSTATE_VIEW, &(target))
#define	SET_WORLD_MATRIX(target) D3DDevice->SetTransform(D3DTRANSFORMSTATE_WORLD, &(target))
#define CROSS_PRODUCT(dest, src1, src2) D3DRMVectorCrossProduct((D3DVECTOR*)(dest), (D3DVECTOR*)(src1), (D3DVECTOR*)(src2))
#define NORMALIZE_VECTOR(vec) D3DRMVectorNormalize((D3DVECTOR*)(vec))
#define VECTOR_SUBTRACT(dest, src1, src2) D3DRMVectorSubtract((D3DVECTOR*)(dest), (D3DVECTOR*)(src1), (D3DVECTOR*)(src2))

#endif

#endif // _PCINCL_H