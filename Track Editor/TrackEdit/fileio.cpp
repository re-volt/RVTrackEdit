#include <revolt.h>

#include "fileio.h"
#include "editor.h"
#include "textstrings.h"
#include <light.h>

//the following 2 lines cancel out the effect of the redefinition
//of fopen that occurs in load.h (one of the game files)
#undef	fopen
#define fopen(_f, _m) fopen(_f, _m)

#define USE_COLLISION_CELLS	1 //make this a zero to make the game display ALL collision polys
							 //this is useful ONLY for debugging collision polygon generation
REAL ExportScale;

#include "editor.h"
#include "compile.h"
#include <obj_init.h>

#include <endstrm.h>
#include <assert.h>
#include <shellapi.h>

using namespace std;

extern char**			FileList;
extern U16				FileCount;
extern DIRECTION		ModuleDirection;
extern S16				ModuleElevation;
extern INDEX			ModuleRenumber[];
extern char				ModulePriority[];
extern INDEX			ModuleNeutralize[];
extern U16				SmallCubeCount;
extern SMALLCUBE*		SmallCubes;
extern U16				BigCubeCount;
extern BIGCUBE*			BigCubes;
extern NCPDATA			CollisionData;
extern U16				CollisionCellCount;
extern TRACKZONE*		ZoneBuffer;
extern ZONESEQENTRY*	ZoneSequence;
extern INDEX			ZonesVisited;
extern POSNODE*			PosNodeBuffer;
extern INDEX			PosnNodeCount;
extern REAL				LapDistance;
extern U16				AINodesPlaced;
extern AINODEINFO*		AINodes;
extern U16				LightCount;
extern FILELIGHT*		Lights;

void ReadModules(EndianInputStream& is, TRACKTHEME* theme);
void ReadUnits(EndianInputStream& is, TRACKTHEME* theme);
void ReadMeshes(EndianInputStream& is, TRACKTHEME* theme);
void ReadPolygons(EndianInputStream& is, TRACKTHEME* theme);
void ReadPolySets(EndianInputStream& is, TRACKTHEME* theme);
void ReadRGBPolys(EndianInputStream& is, TRACKTHEME* theme);
void ReadVertices(EndianInputStream& is, TRACKTHEME* theme);
void ReadUVCoords(EndianInputStream& is, TRACKTHEME* theme);
void ReadUVPolygons(EndianInputStream& is, TRACKTHEME* theme);

#ifdef _DEBUG
	char LevelFolder[] = "..\\game\\levels";
#else
	char LevelFolder[] = "..\\levels";
#endif

//-----------------------------------------------------------------------------
//  Name:       ReadRTUFile
//  Purpose:	Loads data from RTU (Revolt Track Unit) file into memory
//-----------------------------------------------------------------------------


bool ReadRTUFile(const string& pathname, TRACKTHEME* theme)
{
	bool fileloaded = false;
	
	char filename[MAX_PATH];

	sprintf(filename, "%s\\trackunit.rtu", pathname.c_str());
	EndianInputStream is(filename);
	if(is.is_open())
	{
		U32 fileid = is.GetU32();
		if(fileid == MAKE_ID('R','T','U',' '))
		{
			U16 version = is.GetU16();
			if(version == RTU_READ_VERSION)
			{
				VALID_TARGETS targets = is.GetU16();
				
				ReadVertices(is, theme);
				ReadPolygons(is, theme);
				ReadRGBPolys(is, theme);
				ReadPolySets(is, theme);
				ReadMeshes(is, theme);
				ReadUVCoords(is, theme);
				ReadUVPolygons(is, theme);
				ReadUnits(is, theme);
				ReadModules(is, theme);
				theme->TPageCount = is.GetU16();
				theme->WallIndex = is.GetU16();

				fileloaded = true;
			}
		}
	}
	return fileloaded;
}

void ReadUVCoords(EndianInputStream& is, TRACKTHEME* theme)
{
	theme->UVCoordCount = is.GetU16();
	theme->UVCoords = new RevoltUVCoord[theme->UVCoordCount];
	for(U16 v = 0; v < theme->UVCoordCount; v++)
	{
		theme->UVCoords[v].U = is.GetFloat();
		theme->UVCoords[v].V = is.GetFloat();
	}
}

void ReadUVPolygons(EndianInputStream& is, TRACKTHEME* theme)
{
	theme->UVPolyCount = is.GetU16();
	theme->UVPolys = new BASICPOLY[theme->UVPolyCount];
	BASICPOLY* polyptr = theme->UVPolys;
	for(U16 p = 0; p < theme->UVPolyCount; p++)
	{
		U16 vertexcount = is.GetU16();
		polyptr->Vertices[0] = is.GetU16();
		polyptr->Vertices[1] = is.GetU16();
		polyptr->Vertices[2] = is.GetU16();
		if(vertexcount > 3)
		{
			polyptr->IsTriangle = FALSE;
			do
			{
				polyptr->Vertices[3] = is.GetU16();
			}while(--vertexcount > 3);
		}
		else
		{
			polyptr->IsTriangle = TRUE;
		}
		polyptr++;			
	}
}

void ReadVertices(EndianInputStream& is, TRACKTHEME* theme)
{
	theme->VertCount = is.GetU16();
	theme->Verts = new REVOLTVECTOR[theme->VertCount];
	for(U16 v = 0; v < theme->VertCount; v++)
	{
		theme->Verts[v].X = is.GetFloat();
		theme->Verts[v].Y = is.GetFloat();
		theme->Verts[v].Z = is.GetFloat();
	}
}

void ReadPolygons(EndianInputStream& is, TRACKTHEME* theme)
{
	theme->PolyCount = is.GetU16();
	theme->Polys = new BASICPOLY[theme->PolyCount];
	BASICPOLY* polyptr = theme->Polys;
	for(U16 p = 0; p < theme->PolyCount; p++)
	{
		U16 vertexcount = is.GetU16();
		polyptr->Vertices[0] = is.GetU16();
		polyptr->Vertices[1] = is.GetU16();
		polyptr->Vertices[2] = is.GetU16();
		if(vertexcount > 3)
		{
			polyptr->IsTriangle = FALSE;
			do
			{
				polyptr->Vertices[3] = is.GetU16();
			}while(--vertexcount > 3);
		}
		else
		{
			polyptr->IsTriangle = TRUE;
		}
		polyptr++;			
	}
}

void ReadRGBPolys(EndianInputStream& is, TRACKTHEME* theme)
{
	theme->RGBPolyCount = is.GetU16();
	theme->RGBPolys = new BASICPOLY[theme->RGBPolyCount];
	BASICPOLY* polyptr = theme->RGBPolys;
	for(U16 p = 0; p < theme->RGBPolyCount; p++)
	{
		U16 vertexcount = is.GetU16();
		polyptr->Vertices[0] = is.GetU32();
		polyptr->Vertices[1] = is.GetU32();
		polyptr->Vertices[2] = is.GetU32();
		if(vertexcount > 3)
		{
			polyptr->IsTriangle = FALSE;
			do
			{
				polyptr->Vertices[3] = is.GetU32();
			}while(--vertexcount > 3);
		}
		else
		{
			polyptr->IsTriangle = TRUE;
		}
		polyptr++;			
	}
}

void ReadMeshes(EndianInputStream& is, TRACKTHEME* theme)
{
	theme->MeshCount = is.GetU16();
	theme->Meshes = new MESH[theme->MeshCount];
	for(U16 m = 0; m < theme->MeshCount; m++)
	{
		U16 polysetcount = theme->Meshes[m].PolySetCount = is.GetU16();
		U16* polysetindex = theme->Meshes[m].PolySetIndices = new U16[polysetcount];
		for(U16 ps = 0; ps < polysetcount; ps++)
		{
			*polysetindex = is.GetU16();
			polysetindex++;
		}
	}
}

void ReadPolySets(EndianInputStream& is, TRACKTHEME* theme)
{
	U16 polysetcount = theme->PolySetCount = is.GetU16();
	POLYSET* polyset = theme->PolySets = new POLYSET[polysetcount];
	for(U16 ps = 0; ps < polysetcount; ps++)
	{
		polyset->PolygonCount = is.GetU16();
		polyset->VertexCount = 0;
		polyset->Indices = new U16[polyset->PolygonCount];
		for(U32 p = 0; p < polyset->PolygonCount; p++)
		{
			polyset->Indices[p] = is.GetU16();
			polyset->VertexCount += 3;
			if(theme->Polys[polyset->Indices[p] & ~GOURAUD_SHIFTED].IsTriangle == FALSE)
			{
				polyset->VertexCount += 3;
			}
		}
		polyset++;
	}
}

void ReadUnits(EndianInputStream& is, TRACKTHEME* theme)
{
	theme->UnitCount = is.GetU16();
	theme->Units = new TRACKUNIT*[theme->UnitCount];
	for(U16 i = 0; i < theme->UnitCount; i++)
	{
		TRACKUNIT* unit = new TRACKUNIT;
		theme->Units[i] = unit;
		unit->MeshID = is.GetU16();
		unit->UVPolyCount = is.GetU16();
		unit->UVPolys = new UVPOLYINSTANCE[unit->UVPolyCount];
		for(U16 p = 0; p < unit->UVPolyCount; p++)
		{
			unit->UVPolys[p].TPageID = is.GetU8();
			unit->UVPolys[p].PolyID = is.GetU16();
			unit->UVPolys[p].Rotation = is.GetU8();
		}
		unit->SurfaceCount = is.GetU16();
		unit->Surfaces = new U16[unit->SurfaceCount];
		for(U16 s = 0; s < unit->SurfaceCount; s++)
		{
			unit->Surfaces[s] = is.GetU8();
		}
		unit->PickupPos.X = is.GetFloat();
		unit->PickupPos.Y = is.GetFloat();
		unit->PickupPos.Z = is.GetFloat();
		unit->RGBCount = is.GetU16();
		unit->RGBs = new U16[unit->RGBCount];
		for(U16 r = 0; r < unit->RGBCount; r++)
		{
			unit->RGBs[r] = is.GetU16();
		}
		unit->RootEdges = is.GetU16();
	}
}

void ReadModules(EndianInputStream& is, TRACKTHEME* theme)
{
	theme->ModuleCount = is.GetU16();

	theme->Modules = new TRACKMODULE*[theme->ModuleCount];
	for(U16 m = 0; m < theme->ModuleCount; m++)
	{
		TRACKMODULE* module = new TRACKMODULE;
		theme->Modules[m] = module;
		
		module->InstanceCount = is.GetU16();
		module->Instances = new RevoltTrackUnitInstance*[module->InstanceCount];
		for(U16 i = 0; i < module->InstanceCount; i++)
		{
			RevoltTrackUnitInstance* instance = new RevoltTrackUnitInstance;
			module->Instances[i] = instance;
			instance->UnitID = is.GetU16();
			instance->Direction = (DIRECTION)is.GetU16();
			instance->XPos = is.GetS16();
			instance->YPos = is.GetS16();
			instance->Elevation = is.GetS16();
		}
		module->ZoneCount = is.GetU16();
		module->Zones = new TRACKZONE*[module->ZoneCount];
		for(i = 0; i < module->ZoneCount; i++)
		{
			TRACKZONE* zone = new TRACKZONE;
			module->Zones[i] = zone;
			zone->Centre.X = is.GetFloat();
			zone->Centre.Y = is.GetFloat();
			zone->Centre.Z = is.GetFloat();
			zone->XSize = is.GetFloat();
			zone->YSize = is.GetFloat();
			zone->ZSize = is.GetFloat();
			zone->Links[0].Position.X = is.GetFloat();
			zone->Links[0].Position.Y = is.GetFloat();
			zone->Links[0].Position.Z = is.GetFloat();
			zone->Links[1].Position.X = is.GetFloat();
			zone->Links[1].Position.Y = is.GetFloat();
			zone->Links[1].Position.Z = is.GetFloat();
		}
		for(U16 g = 0; g < MAX_MODULE_ROUTES; g++)
		{
			module->NodeCount[g] = is.GetU16();
			module->Nodes[g] = new AINODEINFO*[module->NodeCount[g]];
			for(i = 0; i < module->NodeCount[g]; i++)
			{
				AINODEINFO* node = new AINODEINFO;
				module->Nodes[g][i] = node;
				node->Ends[AI_GREEN_NODE].X = is.GetFloat();
				node->Ends[AI_GREEN_NODE].Y = is.GetFloat();
				node->Ends[AI_GREEN_NODE].Z = is.GetFloat();
				node->Ends[AI_RED_NODE].X = is.GetFloat();
				node->Ends[AI_RED_NODE].Y = is.GetFloat();
				node->Ends[AI_RED_NODE].Z = is.GetFloat();
				node->RacingLine = is.GetFloat();
				node->Priority = ModulePriority[m];
			}
		}
		module->LightCount = is.GetU16();
		if(module->LightCount == 0)
		{
			module->Lights = NULL;
		}
		else
		{
			module->Lights = new LIGHTINFO*[module->LightCount];
			for(i = 0; i < module->LightCount; i++)
			{
				LIGHTINFO* light = new LIGHTINFO;
				module->Lights[i] = light;
				light->Position.X = is.GetFloat();
				light->Position.Y = is.GetFloat();
				light->Position.Z = is.GetFloat();
				light->Reach	  = is.GetFloat();
				light->Up.X		  = is.GetFloat();
				light->Up.Y		  = is.GetFloat();
				light->Up.Z		  = is.GetFloat();
				light->Forward.X  = is.GetFloat();
				light->Forward.Y  = is.GetFloat();
				light->Forward.Z  = is.GetFloat();
				light->Cone		  = is.GetFloat();
				light->Red		  = is.GetS16();
				light->Green	  = is.GetS16();
				light->Blue		  = is.GetS16();
				light->Type		  = is.GetU16();
			}
		}
	}
}

//*****************************************************************

void SpaceToUnderScore(char * text)
{
	char* space;
	do
	{
		space = strchr(text, ' ');
		if(space != NULL)
		{
			*space = '_';
		}
	}while(space != NULL);
}

bool NamedFileExists(const char* filename)
{
	WIN32_FIND_DATA find_data;
	HANDLE h = FindFirstFile((LPCTSTR)filename, &find_data);
	if(h == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	FindClose(h);
	return true;
}

bool GetFileDescription(const char* filename, char* description)
{
	assert(description != NULL);

	bool success = false;
	EndianInputStream is(filename);
	if(is.is_open())
	{
		U32 fileid = is.GetU32();
		if(fileid == MAKE_ID('T','D','F',' '))
		{
			is.GetU16(); //skip version info
			is.GetPaddedText(description, MAX_DESCRIPTION_LENGTH - 1);
			success = true;
		}
	}
	return success;
}

bool GetDescribedFilename(const char* description, char* filename)
{
	assert(filename != NULL);

	sprintf(filename, "TDF\\!%s.TDF", description);
	SpaceToUnderScore(filename);
	return true;
}

bool DescribedFileExists(const char* description)
{
	char filename[MAX_PATH];
	GetDescribedFilename(description, filename);
	return NamedFileExists(filename);
}

bool NextFreeFile(char* filename)
{
	assert(filename != NULL);

	U16		n = 0;
	bool	foundfreefile = false;
	while((foundfreefile == false) && (n < 10000))
	{
		sprintf(filename, "TDF\\FILE%04d.TDF", n);
		foundfreefile = (NamedFileExists(filename) == false);
		n++;
	}
	if(foundfreefile == false)
	{
		strcpy(filename, "");
	}
	return foundfreefile;
}

bool NextFreeDescription(char* description)
{
	assert(description != NULL);	//must supply a description buffer

	U16 n = 0;	//start at the beginning
	bool foundfreedescription = false;
	do
	{
		sprintf(description, GetTextString(TEXT_DEFAULT_TRACK_NAME), n++); //form the description
		foundfreedescription = (DescribedFileExists(description) == false);	 //and check if it is free for use

	}while( (foundfreedescription == false) && (n < 10000) );	//if file is being used and we haven't yet checked all files, repeat

	if(foundfreedescription == false)	//if we could find an unused description
	{	
		strcpy(description, "");	//blank out the description buffer
	}
	return foundfreedescription;	//flag status back to user
}

bool WriteTDF(const char* filename, TRACKDESC* track)
{
	bool success = false;
	CreateDirectory("TDF", NULL);
	EndianOutputStream os(filename);
	if(os.is_open())
	{
		os.PutU32(MAKE_ID('T','D','F',' '));
		os.PutU16(TDF_READ_VERSION);

		os.PutPaddedText(track->Name);
		os.PutU16(track->ThemeType);
		
		os.PutU32(SECTION_MODULE_GRID);
		os.PutU16(track->Width);
		os.PutU16(track->Height);

		U32 m = track->Width * track->Height;
		for(U32 i = 0; i < m; i++)
		{
			RevoltTrackModuleInstance* module;
			module = &track->Modules[i];
			os.PutS16(module->ModuleID);
			os.PutU16((U16)module->Direction);
			os.PutU16(module->Elevation);
		}
		os.PutU16(track->PickupsUsed);
		for(i = 0; i < track->PickupsUsed; i++)
		{
			os.PutU16(track->Pickups[i].X);
			os.PutU16(track->Pickups[i].Y);
		}
		track->Dirty = false;
		success = true;
	}
	return success;
}

bool ReadTDF(const char* filename, TRACKDESC* track, bool convert)
{
	bool success = false;
	EndianInputStream is(filename);
	if(is.is_open())
	{
		DestroyTrack(track);
		U32 fileid = is.GetU32();
		if(fileid == MAKE_ID('T','D','F',' '))
		{
			U16 version = is.GetU16();
			if((version == TDF_READ_VERSION) || (version == (TDF_READ_VERSION - 1)))	
			{
				char name[MAX_DESCRIPTION_LENGTH];
				is.GetPaddedText(name, MAX_DESCRIPTION_LENGTH);
				is.GetU16();	//skip Theme type for now
				is.GetU32();	//skip Section Flags
				U16 width = is.GetU16();
				U16 height = is.GetU16();
				CreateTrackAndCursor(track, width, height, name);
				U32 m = track->Width * track->Height;
				for(U32 i = 0; i < m; i++)
				{
					RevoltTrackModuleInstance* module;
					module = &track->Modules[i];
					INDEX mod = is.GetU16();	//get module ID from file
					ModuleDirection = (DIRECTION)is.GetU16();
					ModuleElevation = is.GetU16();
					if(convert == true)
					{
						if(mod != MAX_INDEX)
						{
							mod = ModuleNeutralize[mod];
						}
					}
					if(mod != MAX_INDEX)
					{
						U16 xpos = i % track->Width;
						U16 ypos = i / track->Width;
						PlaceModuleNumber(track, mod, xpos, ypos);
					}
					module->ModuleID = mod;
					module->Direction = ModuleDirection;
					module->Elevation = ModuleElevation;
				}
				if(version == TDF_READ_VERSION_2)
				{
					U16 pickupsinfile = is.GetU16();
					track->PickupsUsed = min(pickupsinfile, MAX_PICKUPS);
					for(U16 n = 0; n < pickupsinfile; n++)
					{
						if(n < MAX_PICKUPS)
						{
							track->Pickups[n].X = is.GetU16();
							track->Pickups[n].Y = is.GetU16();
						}
						else
						{
							is.SkipBytes(sizeof(PICKUPINFO));
						}
					}
				}
				else
				{
					//create default set of pickups
					track->PickupsUsed = MAX_PICKUPS;
					for(U16 n = 0; n< MAX_PICKUPS; n++)
					{
						track->Pickups[n].X = track->Pickups[n].Y = n;
					}
				}
				track->Dirty = false;
				success = true;
			}
		}
	}
	return success;
}

U16 CountTDFFiles(void)
{
	U16 filecount = 0;
	bool filefound = false;
	WIN32_FIND_DATA find_data;
	HANDLE h = FindFirstFile((LPCTSTR)"TDF\\!*.TDF", &find_data);
	if(h != INVALID_HANDLE_VALUE)
	{
		filefound = true;
		while(filefound == true)
		{
			filefound = (FindNextFile(h, &find_data) == TRUE);
			filecount++;
		}
		FindClose(h);
	}
	return filecount;
}

void EraseFileList()
{
	if(FileList != NULL)
	{
		while(FileCount--)
		{
			delete[] FileList[FileCount];
		}
		delete[] FileList;
		FileCount = 0;
		FileList = NULL;
	}
}

bool MakeFileList(void)
{
	EraseFileList();

	U16 numberoffiles = CountTDFFiles();
	FileList = new char*[numberoffiles];
	if(FileList != NULL)
	{
		char filename[MAX_PATH];
		WIN32_FIND_DATA find_data;
		HANDLE h = FindFirstFile((LPCTSTR)"TDF\\!*.TDF", &find_data);
		if(h != INVALID_HANDLE_VALUE)
		{
			bool filefound = true;
			while((FileCount < numberoffiles) && (filefound == true))
			{
				FileList[FileCount] = new char[MAX_DESCRIPTION_LENGTH];
				if(FileList[FileCount] != NULL)
				{
					sprintf(filename, "TDF\\%s", find_data.cFileName); 
					if(GetFileDescription(filename, FileList[FileCount]) == true)
					{
						FileCount++;
					}
					else
					{	//was an N64 format file so give back the memory for this entry 
						//as it will be re-allocated next time around
						delete[] FileList[FileCount];
					}
					filefound = (FindNextFile(h, &find_data) == TRUE);
				}
				else
				{
					EraseFileList();
					break;
				}
			}
			FindClose(h);
		}
	}
	return (FileList != NULL);
}

//*****************************************************************
bool NamedLevelExists(const char* levelname)
{
	assert(levelname != NULL);
	
	WIN32_FIND_DATA find_data;
	char levelpath[MAX_PATH];
	sprintf(levelpath, "%s\\%s", LevelFolder, levelname);

	HANDLE h = FindFirstFile((LPCTSTR)levelpath, &find_data);
	if(h == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	FindClose(h);
	return true;
}

bool GetLevelDescription(const char* levelname, char* description)
{
	assert(description != NULL);

	char linebuffer[MAX_DESCRIPTION_LENGTH + 40];
	static char nametoken[] = "NAME "; //note - the trailing space is important
	
	char infospec[MAX_PATH];
	sprintf(infospec, "%s\\%s\\%s.inf", LevelFolder, levelname, levelname);

	bool success = false;
	FILE* fi;
	fi = fopen(infospec, "rt");
	if(fi != NULL)
	{
		bool done = false;
		while((done == false) && (success == false))
		{
			char* lineptr = fgets(linebuffer, sizeof(linebuffer), fi);
			if(lineptr == NULL)
			{	
				done = true;
			}
			else
			{
				if(strnicmp(nametoken, linebuffer, strlen(nametoken)) == 0)
				{
					char* openquote = strchr(linebuffer, '\'');
					if(openquote != NULL)
					{
						char* dash = strchr(openquote+1, '-');
						if(dash != NULL)
						{
							char* closequote = strchr(dash+1, '\'');
							if(closequote != NULL)
							{
								*closequote = '\0';
								strncpy(description, dash+1, MAX_DESCRIPTION_LENGTH - 1);
								success = true;
							}
						}
					}
				}
			}
		}
		fclose(fi);
	}
	return success;
}

unsigned long GetMemChecksum(long *mem, long bytes)
{
	long crc, bits, i, flag;
	long *pos;

// calc crc

	bytes >>= 2;

	crc = 0xffffffff;
	pos = mem;
	for ( ; bytes ; bytes--, pos++)
	{
		bits = *pos;
		for (i = 32 ; i ; i--)
		{
			flag = crc & 0x80000000;
			crc  = (crc << 1) | (bits & 1);
			bits >>= 1;

			if (flag)
			{
				crc ^= 0x04c11db7;	// polynomial
			}
		}
	}

	crc ^= 0xffffffff;

// return crc

	return crc;
}

void MakeChksumText(char* text, long* mem, long bytes)
{
	long chksum = GetMemChecksum(mem, bytes);
	for(int i=0; i<8; i++)
	{
		text[i] = ((chksum >> 28) & 0x0f) + 'a';
		chksum <<= 4;
	}
}

bool GetDescribedLevelName(const char* description, char* levelname)
{
	assert(levelname != NULL);

	char chksum[9] = "12345678";
	char tempdesc[MAX_DESCRIPTION_LENGTH + 8];

	memset(tempdesc, '\0', MAX_DESCRIPTION_LENGTH + 8);
	sprintf(tempdesc, "%s %2.2f", description, ExportScale);
	SpaceToUnderScore(tempdesc);
	MakeChksumText(chksum, (long*)tempdesc, MAX_DESCRIPTION_LENGTH);

	sprintf(levelname, "USER_%s", chksum);
	return true;
}

bool DescribedLevelExists(const char* description)
{
	char levelname[MAX_PATH];
	return GetDescribedLevelName(description, levelname);
}

bool NextFreeLevel(char* levelname)
{
	assert(levelname != NULL);

	U16	 n = 0;
	bool foundfreelevel = false;

	while((foundfreelevel == false) && (n < 1000))
	{
		sprintf(levelname, "USER%03d", n);
		foundfreelevel = (NamedLevelExists(levelname) == false);
		n++;
	}
	if(foundfreelevel == false)
	{
		strcpy(levelname, "");
	}
	return foundfreelevel;
}

//*****************************************************************

//**********************************************
//
//
//
//**********************************************
void WriteSmallCubes(EndianOutputStream& os)
{
	os.PutU32(SmallCubeCount);
	SMALLCUBE* cube = SmallCubes;
	for(U32 n = 0; n < SmallCubeCount; n++)
	{
		os.PutFloat(cube->Header.CentreX * ExportScale);
		os.PutFloat(cube->Header.CentreY * ExportScale);
		os.PutFloat(cube->Header.CentreZ * ExportScale);
		os.PutFloat(cube->Header.Radius * ExportScale);
		os.PutFloat(cube->Header.Xmin * ExportScale);
		os.PutFloat(cube->Header.Xmax * ExportScale);
		os.PutFloat(cube->Header.Ymin * ExportScale);
		os.PutFloat(cube->Header.Ymax * ExportScale);
		os.PutFloat(cube->Header.Zmin * ExportScale);
		os.PutFloat(cube->Header.Zmax * ExportScale);
		os.PutS16(cube->Header.PolyNum);
		os.PutS16(cube->Header.VertNum);

		WORLD_POLY_LOAD* poly = cube->Polys;
		for(S16 p = 0; p < cube->Header.PolyNum; p++)
		{
			os.PutS16(poly->Type);
			os.PutS16(poly->Tpage);
			os.PutS16(poly->vi0);
			os.PutS16(poly->vi1);
			os.PutS16(poly->vi2);
			os.PutS16(poly->vi3);
			os.PutS32(poly->c0);
			os.PutS32(poly->c1);
			os.PutS32(poly->c2);
			os.PutS32(poly->c3);
			os.PutFloat(poly->u0);
			os.PutFloat(poly->v0);
			os.PutFloat(poly->u1);
			os.PutFloat(poly->v1);
			os.PutFloat(poly->u2);
			os.PutFloat(poly->v2);
			os.PutFloat(poly->u3);
			os.PutFloat(poly->v3);
			poly++;
		}

		WORLD_VERTEX_LOAD* vert = cube->Verts;
		for(S16 v = 0; v < cube->Header.VertNum; v++)
		{
			os.PutFloat(vert->x * ExportScale);
			os.PutFloat(vert->y * ExportScale);
			os.PutFloat(vert->z * ExportScale);
			os.PutFloat(vert->nx);
			os.PutFloat(vert->ny);
			os.PutFloat(vert->nz);
			vert++;
		}
		cube++;
	}
}

//**********************************************
//
//
//
//**********************************************
void WriteBigCubes(EndianOutputStream& os)
{
	os.PutU32(BigCubeCount);
	BIGCUBE* cube = BigCubes;
	for(U32 n = 0; n < BigCubeCount; n++)
	{
		os.PutFloat(cube->Header.x * ExportScale);
		os.PutFloat(cube->Header.y * ExportScale);
		os.PutFloat(cube->Header.z * ExportScale);
		os.PutFloat(cube->Header.Radius * ExportScale);
		os.PutU32(cube->Header.CubeNum);
		os.write((char*)cube->CubeIndices, sizeof(U32) * cube->Header.CubeNum);
		cube++;
	}
}

//**********************************************
//
//
//
//**********************************************
void WriteWorldFile(const char *filename)
{
	EndianOutputStream os(filename);
	if(os.is_open())
	{
		WriteSmallCubes(os);
		WriteBigCubes(os);
	}
}

void WriteLITFile(const char *filename, const RevoltVertex& startpos)
{
	EndianOutputStream os(filename);
	if(os.is_open())
	{
#if 1
		os.PutU32(LightCount);
		for(U16 i = 0; i < LightCount; i++)
		{
			FILELIGHT* light = &Lights[i];
			os.PutFloat(light->x * ExportScale);
			os.PutFloat(light->y * ExportScale);
			os.PutFloat(light->z * ExportScale);
			os.PutFloat(light->Reach * ExportScale);
			os.PutFloat(light->DirMatrix.m[0]);
			os.PutFloat(light->DirMatrix.m[1]);
			os.PutFloat(light->DirMatrix.m[2]);
			os.PutFloat(light->DirMatrix.m[3]);
			os.PutFloat(light->DirMatrix.m[4]);
			os.PutFloat(light->DirMatrix.m[5]);
			os.PutFloat(light->DirMatrix.m[6]);
			os.PutFloat(light->DirMatrix.m[7]);
			os.PutFloat(light->DirMatrix.m[8]);
			os.PutFloat(light->Cone);
			os.PutFloat(light->r);
			os.PutFloat(light->g);
			os.PutFloat(light->b);
			os.PutS8(LIGHT_FILE | LIGHT_FIXED | LIGHT_MOVING);
			os.PutS8(light->Type);
			os.PutS8(0);
			os.PutS8(0);
		}
#else
		os.PutS32(1);
		os.PutFloat(startpos.X * ExportScale);
		os.PutFloat((startpos.Y - 1000) * ExportScale);
		os.PutFloat(startpos.Z * ExportScale);
		os.PutFloat(1500);
		MAT dirmatrix;
		RotMatrixZYX(&dirmatrix, 0.25f, 0, 0.125f);
		os.PutFloat(dirmatrix.m[0]);
		os.PutFloat(dirmatrix.m[1]);
		os.PutFloat(dirmatrix.m[2]);
		os.PutFloat(dirmatrix.m[3]);
		os.PutFloat(dirmatrix.m[4]);
		os.PutFloat(dirmatrix.m[5]);
		os.PutFloat(dirmatrix.m[6]);
		os.PutFloat(dirmatrix.m[7]);
		os.PutFloat(dirmatrix.m[8]);
		os.PutFloat(90);
		os.PutFloat(255);
		os.PutFloat(0);
		os.PutFloat(0);
		os.PutS8(LIGHT_FILE | LIGHT_FIXED);
		os.PutS8(LIGHT_SPOT);
		os.PutS8(0);
		os.PutS8(0);
#endif
	}
}

//**********************************************
//
//
//
//**********************************************
void WriteInfFile(const char *filename, const char* description, const RevoltVertex& startpos, REAL startdir)
{
	char linebuffer[256];

	FILE* fo;
	fo = fopen(filename, "wt");
	if(fo != NULL)
	{
		fputs(";----------------------------\n", fo);
		fputs("; Revolt .INF file structure\n", fo);
		fputs(";----------------------------\n", fo);
		fputs("\n", fo);
		fputs("NAME            \'User-", fo);
		fputs(description, fo);
		if(ExportScale > (ONE))
		{
			fputs(" (Double)", fo);
		}
		fputs("\'\n", fo);
		sprintf(linebuffer, "STARTPOS        %f %f %f\n", startpos.X * ExportScale, startpos.Y * ExportScale, startpos.Z * ExportScale);
		fputs(linebuffer, fo);
		fputs("STARTPOSREV     0 0 0\n", fo);
		sprintf(linebuffer, "STARTROT        %f\n", startdir);
		fputs(linebuffer, fo);
		fputs("STARTROTREV     0.0\n", fo);
		fputs("STARTGRID       3\n", fo);
		fputs("STARTGRIDREV    0\n", fo);
		fputs("FARCLIP         10000\n", fo);
		fputs("FOGSTART        8000\n", fo);
		fputs("FOGCOLOR        0 0 0\n", fo);
		fputs("VERTFOGSTART    0\n", fo);
		fputs("VERTFOGEND      0\n", fo);
		fputs("WORLDRGBPER     100\n", fo);
		fputs("MODELRGBPER     100\n", fo);
		fputs("INSTANCERGBPER  100\n", fo);
		fclose(fo);
	}
}

//**********************************************
//
//
//
//**********************************************
void WriteNCPFile(const char* destfile)
{
	EndianOutputStream os(destfile);
	if(os.is_open())
	{
		os.PutS16(CollisionData.NumUsedPolys);
		NEWCOLLPOLY* poly = CollisionData.Polys;
		S16 newindex = 0;
		for(S16 n = 0; n < CollisionData.NumUsedPolys; n++)
		{
			os.PutS32(poly->Type);
			os.PutS32(poly->Material);
			os.PutFloat(poly->Plane.v[0]);
			os.PutFloat(poly->Plane.v[1]);
			os.PutFloat(poly->Plane.v[2]);
			os.PutFloat(poly->Plane.v[3] * ExportScale);
			for(U16 v = 0; v < 4; v++)
			{
				os.PutFloat(poly->EdgePlane[v].v[0]);
				os.PutFloat(poly->EdgePlane[v].v[1]);
				os.PutFloat(poly->EdgePlane[v].v[2]);
				os.PutFloat(poly->EdgePlane[v].v[3] * ExportScale);
			}
			os.PutFloat(poly->BBox.XMin * ExportScale);
			os.PutFloat(poly->BBox.XMax * ExportScale);
			os.PutFloat(poly->BBox.YMin * ExportScale);
			os.PutFloat(poly->BBox.YMax * ExportScale);
			os.PutFloat(poly->BBox.ZMin * ExportScale);
			os.PutFloat(poly->BBox.ZMax * ExportScale);
			poly++;
		}
#if USE_COLLISION_CELLS
		os.PutFloat(CollisionData.Header.XStart * ExportScale);
		os.PutFloat(CollisionData.Header.ZStart * ExportScale);
		os.PutFloat(CollisionData.Header.XNum);
		os.PutFloat(CollisionData.Header.ZNum);
		os.PutFloat(CollisionData.Header.GridSize * ExportScale);
		U16 cellcount = CollisionCellCount;
		CELLDATA* cell = CollisionData.Cells;
		while(cellcount--)
		{
			S32 polycount = cell->NumEntries;
			S32* polyindex = cell->CollPolyIndices;
			
			os.PutS32(polycount);
			while(polycount--)
			{
				os.PutS32(*polyindex++);
			}
			cell++;
		}
#endif
	}
}

//**********************************************
//
//
//
//**********************************************
void WriteTAZFile(const char* destfile)
{
	EndianOutputStream os(destfile);
	if(os.is_open())
	{
		os.PutU32(ZonesVisited);
		for(U16 z = 0; z < ZonesVisited; z++)
		{
			TRACKZONE* zone = &ZoneBuffer[ZoneSequence[(z+1) % ZonesVisited].ZoneID];
			os.PutU32(z);
			os.PutFloat(zone->Centre.X * ExportScale);
			os.PutFloat(zone->Centre.Y * ExportScale);
			os.PutFloat(zone->Centre.Z * ExportScale);
			os.PutFloat(ONE);
			os.PutFloat(ZERO);
			os.PutFloat(ZERO);
			os.PutFloat(ZERO);
			os.PutFloat(ONE);
			os.PutFloat(ZERO);
			os.PutFloat(ZERO);
			os.PutFloat(ZERO);
			os.PutFloat(ONE);
			os.PutFloat(zone->XSize * ExportScale);
			os.PutFloat(zone->YSize * ExportScale);
			os.PutFloat(zone->ZSize * ExportScale);
		}
	}
}

//**********************************************
//
//
//
//**********************************************
void WritePANFile(const char* destfile)
{
	EndianOutputStream os(destfile);
	if(os.is_open())
	{
		os.PutU32(PosnNodeCount);	//this also equates to PosNodeCount
		os.PutU32(0);				//we always start at node 0
		os.PutFloat(LapDistance * ExportScale);
		for(INDEX n = 0; n < PosnNodeCount; n++)
		{
			POSNODE* node = &PosNodeBuffer[n];
			os.PutFloat(node->Position.X * ExportScale);
			os.PutFloat(node->Position.Y * ExportScale);
			os.PutFloat(node->Position.Z * ExportScale);
			os.PutFloat(node->Distance * ExportScale);
			os.PutU32((n + (PosnNodeCount - 1)) % PosnNodeCount);
			os.PutU32(MAX_U32);
			os.PutU32(MAX_U32);
			os.PutU32(MAX_U32);
			os.PutU32((n + 1) % PosnNodeCount);
			os.PutU32(MAX_U32);
			os.PutU32(MAX_U32);
			os.PutU32(MAX_U32);
		}
	}
}

//**********************************************
//
//
//
//**********************************************
void WriteFOBFile(const char* destfile, const TRACKDESC* track, const TRACKTHEME* theme)
{
	EndianOutputStream os(destfile);
	if(os.is_open())
	{
		os.PutU32(track->PickupsUsed);

		for(U16 n = 0; n < track->PickupsUsed; n++)
		{
			os.PutU32(OBJECT_TYPE_PICKUP);
			os.PutU32(0);
			os.PutU32(0);
			os.PutU32(0);
			os.PutU32(0);
			REAL x = track->Pickups[n].X;
			REAL y;
			REAL z = track->Pickups[n].Y;
			U16 g = x + (track->Width * z);
			TRACKUNIT*	unit = theme->Units[track->Units[g].UnitID];
			x *= SMALL_CUBE_SIZE;
			y = track->Units[g].Elevation * -ElevationStep;
			z *= SMALL_CUBE_SIZE;
			x += unit->PickupPos.X;
			y += unit->PickupPos.Y;
			z += unit->PickupPos.Z;
			os.PutFloat(x * ExportScale);
			os.PutFloat(y * ExportScale);
			os.PutFloat(z * ExportScale);
			os.PutFloat(ZERO);
			os.PutFloat(ONE);
			os.PutFloat(ZERO);
			os.PutFloat(ZERO);
			os.PutFloat(ZERO);
			os.PutFloat(ONE);
		}
	}
}

//**********************************************
//
//
//
//**********************************************
void WriteFANFile(const char* destfile)
{
	EndianOutputStream os(destfile);
	if(os.is_open())
	{
		os.PutU16(AINodesPlaced);
		os.PutU16(0);
		for(U16 n = 0; n < AINodesPlaced; n++)
		{
			AINODEINFO* node = &AINodes[n];
			os.PutU8(node->Priority);
			os.PutU8('\1');
			os.PutU8('\0');
			os.PutU8('\0');

			os.PutFloat(ONE - node->RacingLine);
			os.PutFloat(ZERO);
			os.PutFloat(ONE - node->RacingLine);	//set overtaking line to be same as racing line
			os.PutFloat(ZERO);

			os.PutU32(30);
			os.PutU32(30);
			os.PutU32((n + (AINodesPlaced - 1)) % AINodesPlaced);
			os.PutU32(MAX_U32);
			os.PutU32((n + 1) % AINodesPlaced);
			os.PutU32(MAX_U32);

			os.PutU32(30);
			os.PutFloat(node->Ends[AI_RED_NODE].X * ExportScale);
			os.PutFloat(node->Ends[AI_RED_NODE].Y * ExportScale);
			os.PutFloat(node->Ends[AI_RED_NODE].Z * ExportScale);
			os.PutU32(30);
			os.PutFloat(node->Ends[AI_GREEN_NODE].X * ExportScale);
			os.PutFloat(node->Ends[AI_GREEN_NODE].Y * ExportScale);
			os.PutFloat(node->Ends[AI_GREEN_NODE].Z * ExportScale);
		}
		os.PutU32(1);
		os.PutFloat(LapDistance * ExportScale);
	}
}

//**********************************************
//
//
//
//**********************************************
void CopyBitmaps(const char* levelroot)
{
	char srcfile[MAX_PATH];
	char destfile[MAX_PATH];
	
	SHFILEOPSTRUCT fos;

	fos.hwnd	= NULL;
	fos.wFunc	= FO_COPY;
	fos.fFlags	= FOF_NOCONFIRMMKDIR |
				  FOF_SILENT |
				  FOF_NOCONFIRMATION;
	fos.pFrom	= srcfile;
	fos.pTo		= destfile;

	int n = 0;
	while(n < 16)	//should be enough for all of the bitmaps in a theme
	{
		sprintf(srcfile, "tpage_%02d.bmp", n + 1);
		srcfile[strlen(srcfile) + 1] = '\0';	// add the extra trailing zero reqd by SHFileOperation
		sprintf(destfile, "%s%c.bmp", levelroot, 'a' + n);
		SHFileOperation(&fos);
		n++;
	}

}

void DeleteDirContents(const char* dirname)
{
	assert(dirname != NULL);

	char searchname[MAX_PATH];
	sprintf(searchname, "%s\\*.*", dirname);

	WIN32_FIND_DATA find_data;
	HANDLE h = FindFirstFile(searchname, &find_data);
	if(h != INVALID_HANDLE_VALUE)
	{
		char targetname[MAX_PATH];
		do
		{
			sprintf(targetname, "%s\\%s", dirname, find_data.cFileName);

			if(find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if(find_data.cFileName[0] != '.')
				{
					DeleteDirectory(targetname);
				}
			}
			else
			{
				DeleteFile(targetname);
			}
		}while(FindNextFile(h, &find_data) == TRUE);
	}
}

void DeleteDirectory(const char* dirname)
{
	assert(dirname != NULL);

	DeleteDirContents(dirname);
	RemoveDirectory(dirname);
}

void DeleteTrackFiles(INDEX index)
{
	bool filelistcreatedlocally = false;
	if(FileList == NULL)
	{
		MakeFileList();	//you must call 'MakeFileList' before calling this function
		filelistcreatedlocally = true;
	}
	assert(index < FileCount);	//file index is out of range

	char filename[MAX_PATH];
	GetDescribedFilename(FileList[index], filename);
	DeleteFile(filename);
	char levelname[MAX_PATH];
	if(GetDescribedLevelName(FileList[index], levelname))
	{
		char levelpath[MAX_PATH];
		sprintf(levelpath, "%s\\%s", LevelFolder, levelname);
		DeleteDirectory(levelpath);
	}

	if(filelistcreatedlocally == true)
	{
		EraseFileList();
	}
}