#include "textstrings.h"

static INDEX Language = LANGUAGE_ENGLISH;

//���������������������񺿡ߌ�	- these are all of the supported special chars (displayed here for reference)


const char* TextStrings[TEXT_STRING_MAX_INDEX][LANGUAGE_COUNT] =
{
	//main menu text
	{"modules",
	"modules",
	"module",
	"pezzi",
	"piezas"},
	
	{"pickups",
	"bonus",
	"pick-ups",
	"oggetti",
	"pickups"},
	
	{"adjust",
	"Ajuster",
	"justieren",
	"regolazione",
	"ajustar"},
	
	{"save",
	"sauv",
	"speichern",
	"salva",
	"grabar"},
	
	{"new",
	"nouveau",
	"neu",
	"nuovo",
	"nuevo"},
	
	{"load",
	"charger",
	"laden",
	"carica",
	"cargar"},
	
	{"export",
	"export",
	"export",
	"esporta",
	"exportar"},
	
	{"quit",
	"quitter",
	"beenden",
	"esci",
	"salir"},
	
	{"place modules",
	"placez les modules",
	"module einsetzen",
	"sistema pezzo",
	"poner piezas"},
	
	{"deleting",
	"effacer",
	"l�schen",
	"eliminazione",
	"borrando"},

	//mode status words for bottom left of sprue
	{"menu",
	"menu",
	"menu",
	"menu",
	"menu"},
	
	{"warning",
	"avertissement",
	"warnung",
	"avvertimento",
	"aviso"},
	
	{"question",
	"question",
	"frage",
	"domanda",
	"pregunta"},
	
	{"info",
	"information",
	"info",
	"info",
	"info"},

	//module group names
	{"start grid",
	"d�part",
	"start",
	"griglia partenza",
	"lin. salida"},
	
	{"straights",
	"tout droit",
	"gerade",
	"rettilinei",
	"rectas"},
	
	{"dips",
	"trou",
	"mulden",
	"pozze",
	"baches 1"},
	
	{"humps",
	"bosses",
	"h�gel",
	"gobbe",
	"baches 2"},
	
	{"square corner",
	"virage 90�",
	"steile Kurve",
	"angolo quadrato",
	"esqu.cerradas"},
	
	{"round corner",
	"virage",
	"runde Kurve",
	"angolo rotondo",
	"esqu.abiertas"},
	
	{"diagonal",
	"diagonale",
	"diagonale",
	"diagonale",
	"diagonal"},
	
	{"bank",
	"pentu",
	"bank",
	"terrapieno",
	"inclinadas"},
	
	{"rumble strip",
	"dos d'�ne",
	"rumpeln",
	"sterrato",
	"comb. baches"},
	
	{"narrow section",
	"�troit",
	"enge",
	"strettoia",
	"estrechez"},
	
	{"pipe",
	"tunnel",
	"rohr",
	"raccordo",
	"tuber�a"},
	
	{"bridge",
	"passerelle",
	"br�cke",
	"ponte",
	"puente"},
	
	{"crossroad",
	"carrefour",
	"kreuzung",
	"incrocio",
	"cruce"},
	
	{"jump",
	"tremplin",
	"springen",
	"salto",
	"salto"},
	
	{"chicane",
	"chicane",
	"zichzack",
	"chicane",
	"chicane"},

	//query dialog
	{"YES",
	"OUI",
	"JA",
	"SI",
	"SI"},
	
	{"NO",
	"NON",
	"NEIN",
	"NO",
	"NO"},

	//compilation status
	{"stage : 1",
	"�tape : 1",
	"stufe : 1",
	"parte : 1",
	"fase : 1"},
	
	{"stage : 2",
	"�tape : 2",
	"stufe : 2",
	"parte : 2",
	"fase : 2"},
	
	{"stage : 3",
	"�tape : 3",
	"stufe : 3",
	"parte : 3",
	"fase : 3"},
	
	{"stage : 4",
	"�tape : 4",
	"stufe : 4",
	"parte : 4",
	"fase : 4"},
	
	{"stage : 5",
	"�tape : 5",
	"stufe : 5",
	"parte : 5",
	"fase : 5"},
	
	{"stage : 6",
	"�tape : 6",
	"stufe : 6",
	"parte : 6",
	"fase : 6"},
	
	{"stage : 7",
	"�tape : 7",
	"stufe : 7",
	"parte : 7",
	"fase : 7"},
	
	{"stage : 8",
	"�tape : 8",
	"stufe : 8",
	"parte : 8",
	"fase : 8"},
	
	{"stage : 9",
	"�tape : 9",
	"stufe : 9",
	"parte : 9",
	"fase : 9"},
	
	{"stage : 10",
	"�tape : 10",
	"stufe : 10",
	"parte : 10",
	"fase : 10"},
	
	{"stage : 11",
	"�tape : 11",
	"stufe : 11",
	"parte : 11",
	"fase : 11"},
	
	{"stage : 12",
	"�tape : 12",
	"stufe : 12",
	"parte : 12",
	"fase : 12"},
	
	{"Finished!!",
	"Termin�!!",
	"Geschafft!!",
	"Ultimata!!",
	"�Concluido!"},

	{"  SAVE",
	"  SAUV",
	"SPEICHERN",
	"  SALVA",
	"GRABAR"},

	//default description for all tracks
	{"Track %d",
	"Piste %d",
	"Strecke %d",
	"Pista %d",
	"Pista %d"},

#ifdef _PC
	{"pickups placed:\n\r%d of 20",
	"bonus plac�s:\n\r%d de 20",
	"pick-ups plaziert:\n\r%d von 20",
	"Raccolte disposto:\n\r%d di 20",
	"pickups puestos:\n\r%d de 20"},
#else
	{"pickups placed:\n\r%d of 6",
	"bonus plac�s:\n\r%d de 6",
	"pick-ups plaziert:\n\r%d von 6",
	"Raccolte disposto:\n\r%d di 6",
	"pickups puestos:\n\r%d de 6"},
#endif
	{": position",
	": placent",
	": position",
	": posizione",
	"posici�n"},
	
	{": size",
	": taille",
	": gr��e",
	": dimensione",
	"tama�o"},

	{"width : %d\n\rheight : %d",
	"largeur : %d\n\rtaille : %d",
	"weite  : %d\n\rh�he : %d",
	"larghezza  : %d\n\raltezza  : %d",
	"anchura  : %d\n\raltura  : %d"},
	
	//warnings & errors
	{"You have not saved the current track.\n\rDo you want to save it now?\n\r",
	"Vous n'avez pas sauvegard� la piste actuelle.\n\rVoulez-vous la sauvegarder maintenant?\n\r",
	"Du hast die aktuelle strecke nicht gesichert.\n\rM�chtest Du jetzt speichern?\n\r",
	"Non hai salvato la pista corrente.\n\rVuoi salvarla adesso?\n\r",
	"No ha grabado la pista\n\r�Desea salvarla ahora?\n\r"},
	
	{"Do you really want to delete\n\r'%s'\n\r",
	"Voulez-vous effacer\n\r'%s'\n\r",
	"M�chtest Du wirklich l�schen\n\r'%s'\n\r",
	"Vuoi veramente eliminare\n\r'%s'\n\r",
	"No pudo salvar la pista. Recomprobaci�n?"},
	
	{"Do you want to keep the changes\n\ryou've just made?\n\r",
	"Voulez-vous garder vos derniers\n\r changements?\n\r",
	"M�chtest Du die �nderungen speichern,\n\r die Du gerade gemacht hast?\n\r",
	"Volete mantenere i cambiamenti che avete appena fatto?\n\r",
	"�Reemplazo La Pista Existente?\n\r"},
	
	{"Track Saved!",
	"Circuit sauv�!",
	"Strecke Gesichert!",
	"Pista salvata!",
	"�Circuito grabado!"},
	
	{"Error Saving Track!",
	"Erreur sauvegarde circuit!",
	"Fehler beim Speichern der Strecke!",
	"Errore durante il salvataggio della pista!",
	"�Error grabando la pista!"},
	
	{"Track doesn't form a loop, or has a step in a pipe.\n\rThe cursor will be placed at the last valid position",
	"Le circuit ne forme pas une boucle, ou a une pente pr�s\n\rd'un tunnel. Le curseur sera plac� � la derni�re\n\rposition valide.",
	"Die Strecke bildet keine komplette runde oder hat eine\n\rUnebenheit in einem Rohr. Der Cursor ist an der letzten\n\rg�ltigen position plaziert.",
	"La pista non forma un anello, o ha un raccordo non valido.\n\rIl cursore verr� posizionato nell'ultima posizione\n\rvalida.",
	"La pista no forma un bucle, o tienen un escal�n en una\n\rtuber�a.step in a pipe. El cursor indicar� la �ltima\n\rposici�n v�lida."},
	
	{"Track doesn't form a loop, or has an upwards step in it.\n\rThe cursor will be placed at the last valid position",
	"Le circuit ne forme pas une boucle, ou a une pente non\n\rfinie. Le curseur sera plac� � la derni�re position\n\rvalide.",
	"Die Strecke bildet keine komplette runde oder hat eine\n\rUnebenheit. Der Cursor ist an der letzten g�ltigen\n\rposition plaziert.",
	"La pista non forma un anello, o ha un pezzo non valido.\n\rIl cursore verr� posizionato nell'ultima posizione\n\rvalida.",
	"La pista no forma un bucle, o tiene un escal�n.\n\rEl cursor indicar� la �ltima posici�n v�lida."},
	
	{"Track doesn't have a start grid",
	"Le circuit n'a pas de d�part",
	"Die strecke hat keine start",
	"La pista non ha una griglia di partenza",
	"La Pista no tiene parrila de salida"},
	
	{"Track has more than one start grid",
	"Le circuit a plus d'un d�part",
	"Die strecke hat mehr als eine start",
	"La pista ha pi� di una griglia di partenza",
	"La Pista tiene m�s de una parrila de salida"},

	{"You do not have enough memory to compile this track.\n\rRemove some modules and try again.",
	"Vous n'avez pas assez de m�moire pour compiler ce\n\rcircuit. Enlevez des �l�ments.",
	"Sie haben nicht genuegend speicher um diese strecke zu\n\rentwerfen. Entfernen sie einige einheiten und versuchen\n\rsie es erneut.",
	"Non hai abbastanza memoria per compilare questa pista.\n\rRimuovi alcuni moduli e prova ancora.",
	"No tienes espacio suficiente para compilar este circuito.\n\rBorra algunas unidades e int�ntalo de nuevo."},

	//help system text (apart from the first string, these occur in pairs - first for the key description, second for the action description)
	{"Help\n\r-----",
	"Aide\n\r-----",
	"Hilfe\n\r------",
	"Aiuto\n\r------",
	"Ayuda\n\r------"},

	{"Up/Down\n\rReturn\n\rEscape",
	"Haut/Bas\n\rReturn\n\rEsc",
	"Oben/Unten\n\rEnter\n\rEsc-Taste",
	"Su/Gi�\n\rInvio\n\rEsci",
	"Arriba/Abajo\n\rEnter\n\rEscape"},

	{"Move cursor\n\rSelect menu option\n\rExit menu",
	"Bouger curseur\n\rS�lectionner le menu option\n\rEsc du menu",
	"Bewege den Cursor\n\rW�hle Men�punkt aus\n\rVerlasse Men�",
	"Muovi cursore\n\rSeleziona menu opzioni\n\rEsci menu",
	"Mover cursor\n\rSeleccionar opci�n del men�\n\rSalir del men�"},

	{"Up/Down\n\rLeft\n\rRight\n\rReturn\n\rEscape",
	"Haut/Bas\n\rGauche\n\rDroite\n\rReturn\n\rEsc",
	"Oben/Unten\n\rLinks\n\rRechts\n\rEnter\n\rEsc-Taste",
	"Su/Gi�\n\rSinistra\n\rDestra\n\rInvio\n\rEsci",
	"Arriba/Abajo\n\rIzquierda\n\rDerecha\n\rEnter\n\rEscape"},
	
	{"Move cursor\n\rWood graphics\n\rCarpet graphics\n\rSelect menu option\n\rGo back",
	"Bouger curseur\n\rTexture bois\n\rTexture tapis\n\rS�lectionner le menu option\n\rRevenir",
	"Bewege den Cursor\n\rHolz Textur\n\rTeppich Textur\n\rW�hle Men�punkt aus\n\rGeh zur�ck",
	"Muovi cursore\n\rWood texture\n\rCarpet texture\n\rSeleziona menu opzioni\n\rIndietro",
	"Mover cursor\n\rGr�ficos madera\n\rGr�ficos moqueta\n\rSeleccionar opci�n del men�\n\rVolver"},

	{"Cursor keys\n\rReturn\n\rEscape",
	"Fl�ches directionelles\n\rReturn\n\rEsc",
	"Pfeiltasten\n\rEnter\n\rEsc-Taste",
	"Tasti cursore\n\rInvio\n\rEsci",
	"Cursores\n\rEnter\n\rEscape"},
	
	{"Move cursor\n\rPlace/Delete pickup\n\rGo back",
	"Bouger curseur\n\rPlacer/Effacer bonus\n\rRevenir",
	"Bewege den Cursor\n\rEinf�gen/L�schen Pick-Up\n\rGeh zur�ck",
	"Muovi cursore\n\rDisponi/Elimina pezzo\n\rIndietro",
	"Mover cursor\n\rColocar/Borrar selecci�n\n\rVolver"},
	
	{"Up\n\rDown\n\rRight\n\rLeft\n\rShift & Up\n\rShift & Down\n\rShift & Right\n\rShift & Left\n\rEscape",
	"Haut\n\rBas\n\rDroite\n\rGauche\n\rShift & Haut\n\rShift & Bas\n\rShift & Droite\n\rShift & Gauche\n\rEsc",
	"Oben\n\rUnten\n\rRechts\n\rLinks\n\rUmschalt + Oben\n\rUmschalt + Unten\n\rShift & Right\n\rShift & Left\n\rEsc-Taste",
	"Su\n\rGi�\n\rDestra\n\rSinistra\n\rMaiuscolo & Su\n\rMaiuscolo & Gi�\n\rMaiuscolo & Destra\n\rMaiuscolo & Sinistra\n\rEsci",
	"Arriba\n\rAbajo\n\rDerecha\n\rIzquierda\n\rShift y Arriba\n\rShift y Abajo\n\rShift y Derecha\n\rShift e Izquierda\n\rEscape"},
	
	{"Increase height\n\rDecrease height\n\rIncrease width\n\rDecrease width\n\rSlide up\n\rSlide down\n\rSlide right\n\rSlide left\n\r Go back",
	"Augmenter la hauter\n\rR�duire la hauteur\n\rAugmenter la largeur\n\rR�duire la largeur\n\rGlisser vers le haut\n\rGlisser vers le bas\n\rGlisser vers la droite\n\rGlisser vers la gauche\n\rRevenir",
	"H�he steigern\n\rH�he reduzieren\n\rWeite steigern\n\rWeite reduzieren\n\rNach oben bewegen\n\rNach unten bewegen\n\rNach links bewegen\n\rNach rechts bewegen\n\rGeh zur�ck",
	"Aumenta altezza\n\rDiminuisci altezza\n\rAumenta larghezza\n\rDiminuisci larghezza\n\rSposta su\n\rSposta Gi�\n\rSposta destra\n\rSposta sinistra\n\rIndietro",
	"Aumentar altura\n\rReducir altura\n\rAumentar ancho\n\rReducir ancho\n\rCuesta arriba\n\rCuesta abajo\n\rCurva derecha\n\rCurva izquierda\n\r Volver"},

	{"Cursor Keys\n\rReturn\n\rBackspace\n\rEscape",
	"Fl�ches directionelles\n\rReturn\n\rBackspace\n\rEsc",
	"Pfeiltasten\n\rEnter\n\rR�cktaste\n\rEsc-Taste",
	"Tasti cursore\n\rInvio\n\rBackspace\n\rEsci",
	"Cursores\n\rEnter\n\rBorrar\n\rEscape"},
	
	{"Move cursor\n\rAdd character/Save track\n\rDelete character\n\rGo back",
	"Bouger le curseur\n\rAjouter une lettre/Sauvegarder le circuit\n\rEffacer lettre\n\rRevenir",
	"Bewege den Cursor\n\rZeichen einf�gen/Strecke speichern\n\rZeichen l�schen\n\rGeh zur�ck",
	"Muovi cursore\n\rAggiungi lettera/Salva pista\n\rCancella lettera\n\rIndietro",
	"Mover cursor\n\rAgregar personaje/Guardar circuito\n\rBorrar personaje\n\rVolver"},

	{"Up/Down\n\rReturn\n\rDelete\n\rEscape",
	"Haut/Bas\n\rReturn\n\rEffacer\n\rEsc",
	"Oben/Unten\n\rEnter\n\rEntf\n\rEsc-Taste",
	"Su/Gi�\n\rInvio\n\rCancella\n\rEsci",
	"Arriba/Abajo\n\rEnter\n\rBorrar\n\rEscape"},
	
	{"Move cursor\n\rSelect track\n\r\n\rDelete Track\n\rGo back",
	"Bouger le curseur\n\rS�lectionner circuit\n\rEffacer circuit\n\rRevenir",
	"Bewege den Cursor\n\rStrecke w�hlen\n\rStrecke l�schen\n\rGeh zur�ck",
	"Muovi cursore\n\rSeleziona pista\n\rCancella pistaIndietro",
	"Mover cursor\n\rSeleccionar circuito\n\rBorrar pista\n\rVolver"},

	{"Cursor keys\n\rReturn\n\rC\n\rDelete\n\rZ\n\rX\n\rPage up\n\rPage down\n\rSpace\n\r+/-\n\rEscape",
	"Fl�ches directionelles\n\rReturn\n\rC\n\rEffacer\n\rZ\n\rX\n\rPg up\n\rPg dn\n\rEspace\n\r+/-\n\rEsc",
	"Pfeiltasten\n\rEnter\n\rC\n\rEntf\n\rZ\n\rX\n\rBild Auf\n\rBild Ab\n\rLeertaste\n\r+/-\n\rEsc-Taste",
	"Tasti cursore\n\rInvio\n\rC\n\rCancella\n\rZ\n\rX\n\rPage up\n\rPage Down\n\rSpazio\n\r+/-\n\rEsci",
	"Cursores\n\rEnter\n\rC\n\rBorrar\n\rZ\n\rX\n\rRePag\n\rAvPag\n\rEspacio\n\r+/-\n\rEscape"},
	
	{"Move cursor\n\rPlace module\n\rCopy module\n\rDelete module\n\rPrevious variant\n\rNext variant\n\rRaise module\n\rLower module\n\rRotate module\n\rRotate track\n\rMenu",
	"Bouger le curseur\n\rPlacer le module\n\rCopier le module\n\rEffacer le module\n\rRevenir config. pr�c�dente\n\rAutre config.\n\rElever le module\n\rAbaisser le module\n\rFaire tourner le module\n\rFaire tourner le circuit\n\rMenu",
	"Bewege den Cursor\n\rElement einsetzen\n\rElement kopieren\n\rElement l�schen\n\rVorschau der Streckenelement\n\rN�chstes Streckenelement\n\rElemente erh�hen\n\rElemente reduzieren\n\rRotierendes Element\n\rRotierende Strecke\n\rMen�",
	"Muovi cursore\n\rDisponi modulo\n\rCopia modulo\n\rCancella modulo\n\rVariante precedente\n\rVariante successiva\n\rAlza modulo\n\rAbbassa modulo\n\rRuota modulo\n\rRuota pista\n\rMenu",
	"Mover cursor\n\rColocar m�dulo\n\rCopiar m�dulo\n\rBorrar m�dulo\n\rVariante anterior\n\rSiguiente variante\n\rElevar m�dulo\n\rBajar m�dulo\n\rGirar m�dulo\n\rGirar circuito\n\rMen�"}, 

	{"Cursor keys\n\rReturn",
	"Fl�ches directionelles\n\rReturn",
	"Pfeiltasten\n\rEnter",
	"Tasti cursore\n\rInvio",
	"Cursores\n\rEnter"},
	
	{"Change selection\n\rAccept selection",
	"Changer la s�lection\n\rAccepter la s�lection",
	"Auswahl ver�ndern\n\rAuswahl treffen",
	"Cambia selezione\n\rAccetta selezione",
	"Cambiar selecci�n\n\rAceptar selecci�n"},

	{"Export double size track?\n\r",
	"Export double size track?\n\r",
	"Export double size track?\n\r",
	"Export double size track?\n\r",
	"Export double size track?\n\r"},

};

const char* TextStringErrorMessage[LANGUAGE_COUNT] =
{
	"Bad string index!",
	"Bad string index!",
	"Bad string index!",
	"Bad string index!",
	"Bad string index!"
};

bool SetLanguage(INDEX requiredlanguage)
{
	if(requiredlanguage < LANGUAGE_COUNT)
	{
		Language = requiredlanguage;
		return true;
	}
	return false;
}

INDEX GetLanguage(void)
{
	if(Language < LANGUAGE_COUNT)
	{
		return Language;
	}
	return LANGUAGE_ENGLISH;
}

const char* GetTextString(INDEX index)
{
	if(index < TEXT_STRING_MAX_INDEX)
	{
		return TextStrings[index][GetLanguage()];
	}
	return TextStringErrorMessage[GetLanguage()];
}

