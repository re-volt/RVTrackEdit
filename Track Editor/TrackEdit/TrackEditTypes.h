#ifndef _TRACKEDITTYPES_H
#define _TRACKEDITTYPES_H

#ifdef _PC
#include "pcincl.h"
#endif

#include <structs.h>
#include <typedefs.h>

#include "editorconstants.h"

/*
**-----------------------------------------------------------------------------
**	Typedefs for use with the Re-Volt Track Editor
**-----------------------------------------------------------------------------
*/


enum PLACEMENT_CAMERA_DIR
{
	PCD_NORTH,
	PCD_EAST,
	PCD_SOUTH,
	PCD_WEST
};

enum THEME_TYPE
{
	TT_TOY,
	TT_RC
};

enum EDIT_SCREEN_STATES
{
	ESS_CHOOSING_MODULE,
	ESS_PLACING_PICKUPS,
	ESS_ADJUSTING_TRACK,
	ESS_SAVING_TRACK,
	ESS_CREATING_TRACK,
	ESS_LOADING_TRACK,
	ESS_EXPORTING_TRACK,
	ESS_QUITTING,
	ESS_PLACING_MODULE,
	ESS_DELETING_TRACK
};

enum POPUP_MENU_STATES
{
	PMS_INACTIVE,
	PMS_APPEARING,
	PMS_DISAPPEARING,
	PMS_ACTIVE,
	PMS_MOVING_UP,
	PMS_MOVING_DOWN
};

enum POPUP_MENU_ACTIONS
{
	PMA_DISMISSED,
	PMA_SELECTED
};

enum COMPILE_STATES
{
	TCS_VALIDATING,
	TCS_UNIT_CUBES,
	TCS_SMALL_CUBES,
	TCS_BIG_CUBES,
	TCS_MAKING_COLLISONS,
	TCS_OPTIMIZING,
	TCS_WRITING_WORLD,
	TCS_WRITING_INF,
	TCS_WRITING_BITMAPS,
	TCS_WRITING_NCP,
	TCS_WRITING_TAZ,
	TCS_CLEANUP,
	TCS_RINGING_CLOCK,
	TCS_FINISHED
};

enum SAVE_STATES
{
	SS_CONFIRM,
	SS_PICK
};

enum {SECTION_MODULE_GRID = 1};	//specifies the bit values for each of the possible sections within a TDF file

const U8 MAX_DESCRIPTION_LENGTH = 40; //longest size of description field within a TDF file
const U8 MAX_FILES_IN_WINDOW = 9; //number of files visible within the file selection window
const U16 FILE_WINDOW_LEFT = 48;
const U16 FILE_WINDOW_TOP = 168;
const U16 CANCEL_WINDOW_LEFT = FILE_WINDOW_LEFT + 328;
const U16 CANCEL_WINDOW_TOP = FILE_WINDOW_TOP;


typedef struct
{
	INDEX	PolyID;
	U8		TPageID;
	U8		Rotation;
}UVPOLYINSTANCE;
const U8	UV_REVERSED = 0x80;

typedef struct
{

	INDEX			MeshID;
	U16				UVPolyCount;

	U16				SurfaceCount;
	U16				RGBCount;

	U16				RootEdges;
	U16				pad;
	

	UVPOLYINSTANCE* UVPolys;
	U16*			Surfaces;
	RevoltVertex	PickupPos;
	U16*			RGBs;
	
}TRACKUNIT;

typedef struct
{
	U16							InstanceCount;
	U16							ZoneCount;
	U16							LightCount;
	U16							pad;

	RevoltTrackUnitInstance**	Instances;
	TRACKZONE**					Zones;
	U16							NodeCount[MAX_MODULE_ROUTES];
	AINODEINFO**				Nodes[MAX_MODULE_ROUTES];
	LIGHTINFO**					Lights;
}TRACKMODULE;

typedef struct
{
	U32		Vertices[4];
	U8		IsTriangle;
	U8		pad[3];

}BASICPOLY;

typedef struct
{
	U32		PolygonCount;
	U32		VertexCount;
	U16*	Indices;
}POLYSET;

typedef struct
{
	U16		PolySetCount;
	U16		pad;
	U16*	PolySetIndices;
}MESH;			

typedef struct
{
	U32         VertexCount;
	D3DVERTEX*	Vertices;
}PRIMITIVE;		//represents a subset of a COMPONENT which share a material

typedef struct
{
	U32			PrimitiveCount;
	PRIMITIVE** Primitives;
}COMPONENT;		//represents the geometry of a single component within a track unit (peg, pan or hull)

typedef struct
{
	U32			ComponentCount;
	COMPONENT** Components;
}DXUNIT;		//represents the geometry of a complete track unit

typedef struct
{
	U16 X;
	U16 Y;
	U16 XMax;
	U16 YMax;
	U32 AbsMax;
	S16 XSize;
	S16 YSize;
}CURSORDESC;

typedef struct
{
	U16 X;
	U16 Y;
}PICKUPINFO;

const U16 MAX_PICKUPS = 20;

#ifdef _PC
typedef struct
{
	U32							Width;
	U32							Height;
	char						Name[MAX_DESCRIPTION_LENGTH];
	RevoltTrackUnitInstance*	Units;
	RevoltTrackModuleInstance*	Modules;
	THEME_TYPE					ThemeType;
	U16							PickupsUsed;
	PICKUPINFO					Pickups[MAX_PICKUPS];
	bool						Dirty;
}TRACKDESC;
#endif

#ifdef _N64
typedef struct
{
	U32							Width;
	U32							Height;
	char						Name[MAX_DESCRIPTION_LENGTH];
	RevoltTrackUnitInstance*	Units;
	RevoltTrackModuleInstance*	Modules;
	THEME_TYPE					ThemeType;
	U16							PickupsUsed;
	U8							Dirty;
	U8							pad;
	PICKUPINFO					Pickups[MAX_PICKUPS];
}TRACKDESC;
#endif

#endif //_TRACKEDITTYPES_H