#include <assert.h>
#include <time.h>

#include <revolt.h>
#include "fileio.h"
#include "compile.h"
#include "editor.h"
#include "states.h"
#include "platform.h"
#include "textstrings.h"

extern void				QuitApp(void);

extern REAL					ExportScale;
extern TRACKTHEME			Theme;
extern TRACKDESC			TrackData;
extern TRACKDESC			UndoTrack;
extern CURSORDESC			PickupCursor;
extern CURSORDESC			TrackCursor;
extern U16					FileWindowStart;
extern U16					FileWindowOffset;
extern PLACEMENT_CAMERA_DIR	PlacementViewpoint;
extern REVOLTVECTOR			CameraPos;
extern REAL					CameraVelocity;
extern REAL					CameraDeflection;
extern REVOLTVECTOR			Viewpoints[4];
extern REAL					RunningTime;

POPUP_MENU_STATES		PopupState			= PMS_INACTIVE;
POPUP_MENU_ACTIONS		PopupAction			= PMA_DISMISSED;
U16						PopupMenuPosition;
REAL					PopupMenuTime;
U16						PopupCursorFrame;
U16						PopupCursorPosition;
REAL					PopupCursorTime;

REAL					ButtonFlashTime[4] = {ZERO, ZERO, ZERO, ZERO};

REAL					CursorFlashTime = ZERO;
REAL					PickupSpin = ZERO;

COMPILE_STATES			CompileState		= TCS_VALIDATING;
SAVE_STATES				SaveState			= SS_CONFIRM;
U8						ClockFrame			= 0;

char					SaveName[MAX_DESCRIPTION_LENGTH] 	= "";
U16						SaveSymbol;

//***********************************************************

static EDIT_SCREEN_STATES	ScreenState	= ESS_PLACING_MODULE;
static EDIT_SCREEN_STATES	ScreenStateStack[10];
static U16					ScreenStateStackPos = 0;
static bool					ErrorPresent = false;
static char					ErrorText[MAX_ERROR_LENGTH];

static bool					NotificationPresent = false;
static char					NotificationText[MAX_NOTIFICATION_LENGTH];

static bool					QueryPresent = false;
static bool					QueryYes = true;
static char					QueryText[MAX_ERROR_LENGTH];

//***********************************************************

void QueryCancelled(void);

//**********************************************************************************************************************
// Error State Handlers
//**********************************************************************************************************************

//***********************************************************
// ErrorExists
//
// This simply returns true or false to indicate whether an
// error has been flagged. This is done in preference to
// making the ErrorPresent variable global.
//***********************************************************
bool ErrorExists(void)
{
	return ErrorPresent;
}

//***********************************************************
// FlagError
//
// Call this function to trigger the display of an error
// message. Supply a pointer the text that you wish to
// display. The text can contain embedded cr/lf chars
// and they will be handled appropriately
//
//***********************************************************

void FlagError(const char* message)
{
	assert(ErrorPresent == false); //user may have made excessive calls to this function

	StartSound(SND_WARNING);
	strncpy(ErrorText, message, sizeof(ErrorText));
	ErrorPresent = true;
}

void FlagError(INDEX index)
{
	FlagError(GetTextString(index));
}

//***********************************************************
// CancelError
//
// Simply sets ErrorPresent to false.
// Again - used in preference to alowing global access.
//***********************************************************

void CancelError(void)
{
	assert(ErrorPresent == true); //user may have made excessive calls to this function

	ErrorPresent = false;
}

//***********************************************************
// GetErrorText
//
// Returns a const pointer to the current error message text
// This is preferential to giving global access to the text
//***********************************************************

const char* GetErrorText(void)
{
	return ErrorText;
}

//**********************************************************************************************************************
// Notification State Handlers
//**********************************************************************************************************************

//***********************************************************
// NotificationExists
//
// This simply returns true or false to indicate whether an
// Notification has been flagged. This is done in preference to
// making the NotificationPresent variable global.
//***********************************************************
bool NotificationExists(void)
{
	return NotificationPresent;
}

//***********************************************************
// FlagNotification
//
// Call this function to trigger the display of an Notification
// message. Supply a pointer the text that you wish to
// display. The text can contain embedded cr/lf chars
// and they will be handled appropriately
//
//***********************************************************

void FlagNotification(const char* message)
{
	assert(NotificationPresent == false); //user may have made excessive calls to this function

	StartSound(SND_PLACE_PICKUP);
	strncpy(NotificationText, message, sizeof(NotificationText));
	NotificationPresent = true;
}

void FlagNotification(INDEX index)
{
	FlagNotification(GetTextString(index));
}

//***********************************************************
// CancelNotification
//
// Simply sets NotificationPresent to false.
// Again - used in preference to alowing global access.
//***********************************************************

void CancelNotification(void)
{
	assert(NotificationPresent == true); //user may have made excessive calls to this function

	NotificationPresent = false;
}

//***********************************************************
// GetNotificationText
//
// Returns a const pointer to the current Notification message text
// This is preferential to giving global access to the text
//***********************************************************

const char* GetNotificationText(void)
{
	return NotificationText;
}

//**********************************************************************************************************************
// Query State Handlers
//**********************************************************************************************************************

//***********************************************************
// QueryExists
//
// This simply returns true or false to indicate whether an
// Query has been flagged. This is done in preference to
// making the QueryPresent variable global.
//***********************************************************
bool QueryExists(void)
{
	return QueryPresent;
}

//***********************************************************
// FlagQuery
//
// Call this function to trigger the display of an Query
// message. Supply a pointer the text that you wish to
// display. The text can contain embedded cr/lf chars
// and they will be handled appropriately
//
//***********************************************************

void FlagQuery(const char* message, bool initialstate)
{
	assert(QueryPresent == false); //user may have made excessive calls to this function

	StartSound(SND_QUERY);
	strncpy(QueryText, message, sizeof(QueryText));
	QueryPresent = true;
	QueryYes = initialstate;
}

void FlagQuery(INDEX index, bool initialstate)
{
	FlagQuery(GetTextString(index), initialstate);
}

//***********************************************************
// CancelQuery
//
// Simply sets QueryPresent to false.
// Again - used in preference to alowing global access.
//***********************************************************

void CancelQuery(void)
{
	assert(QueryPresent == true); //user may have made excessive calls to this function

	QueryPresent = false;
	QueryCancelled();
}

//***********************************************************
// GetQueryText
//
// Returns a const pointer to the current Query message text
// This is preferential to giving global access to the text
//***********************************************************

const char* GetQueryText(void)
{
	return QueryText;
}

//***********************************************************
// NextQueryState
//
// Set the yes/no status to the next setting (the one to the right)
// Again - used in preference to alowing global access.
//***********************************************************

void NextQueryState(void)
{
	QueryYes = !QueryYes;
}

//***********************************************************
// PreviousQueryState
//
// Set the yes/no status to the previous setting (the one to the left)
// Again - used in preference to alowing global access.
//***********************************************************

void PreviousQueryState(void)
{
	QueryYes = !QueryYes;
}

void SetQueryToYes(void)
{
	QueryYes = true;
}

void SetQueryToNo(void)
{
	QueryYes = false;
}

bool QuerySetToYes(void)
{
	return QueryYes;
}

//**********************************************************************************************************************
// ScreenState Handlers
//**********************************************************************************************************************

//***********************************************************
// EnteringState
//
// This function is called JUST AFTER ScreenState changes
// value and is the point where any one-off initialization
// can be done for the particular state.
//***********************************************************
void EnteringState(EDIT_SCREEN_STATES priorstate)
{
	switch(GetScreenState())
	{
		case ESS_LOADING_TRACK:	//all of these modes behave (mostly) the same
			MakeFileList();
		case ESS_CREATING_TRACK: //so its OK to drop through
		case ESS_QUITTING:		
			if(TrackData.Dirty == true)
			{
				if(priorstate == ESS_PLACING_MODULE)
				{
					FlagQuery(TEXT_QUERY_SAVE, true);				
				}
				if(priorstate == ESS_SAVING_TRACK)
				{
					if(TrackData.Dirty == true)
					{
						PopScreenState();
					}
				}
			}
		break;

		case ESS_ADJUSTING_TRACK:
			MakeTrackFromModules(&TrackData);
			CloneTrack(&UndoTrack, &TrackData);
		break;

		case ESS_PLACING_PICKUPS:
			CopyCursorInfo(&PickupCursor, &TrackCursor);
			MakeTrackFromModules(&TrackData);
			CloneTrack(&UndoTrack, &TrackData);
		break;

		case ESS_SAVING_TRACK:
			strcpy(SaveName, TrackData.Name);
			SaveState = SS_CONFIRM;
			SaveSymbol = 0;
		break;

		case ESS_EXPORTING_TRACK:
			FlagQuery(TEXT_QUERY_EXPORT_SIZE, true);				
		break;
	}
}

//***********************************************************
// ExitingState
//
// This function is called JUST BEFORE ScreenState changes
// value and is the point where data needed only for the
// duration of a particular state can be deleted.
//***********************************************************
void ExitingState(void)
{
	switch(GetScreenState())
	{
		case ESS_LOADING_TRACK:
			EraseFileList();
		break;
	}
}

//***********************************************************
// QueryCancelled
//
// This function is called AFTER the query screen is cancelled
// This gives us the chance to decide what to do, based
// on the users choice
//***********************************************************
void QueryCancelled(void)
{
	switch(GetScreenState())
	{
		case ESS_DELETING_TRACK:
			if(QuerySetToYes())
			{
				DeleteTrackFiles(FileWindowStart + FileWindowOffset);
			}
			PopScreenState();
		break;
			
		case ESS_LOADING_TRACK:
		case ESS_CREATING_TRACK:
			if(QuerySetToYes())
			{
				SetScreenState(ESS_SAVING_TRACK);
			}
		break;

		case ESS_PLACING_PICKUPS:
		case ESS_ADJUSTING_TRACK:
			if(!QuerySetToYes())
			{
				//undo the changes
				CloneTrack(&TrackData, &UndoTrack);
			}
			PopScreenState();
		break;

		case ESS_QUITTING:		
			if(QuerySetToYes())
			{
				SetScreenState(ESS_SAVING_TRACK);
			}
			else
			{
				QuitApp();
			}
		break;

		case ESS_EXPORTING_TRACK:
			if(QuerySetToYes())
			{
				ExportScale = 2.0f;
			}
			else
			{
				ExportScale = 1.0f;
			}
		break;
	}
}

void SetScreenState(EDIT_SCREEN_STATES newstate)
{
	ExitingState();
	assert(ScreenStateStackPos < (sizeof(ScreenStateStack) / sizeof(EDIT_SCREEN_STATES)));
	ScreenStateStack[ScreenStateStackPos++] = ScreenState;
	EDIT_SCREEN_STATES priorstate = ScreenState;
	ScreenState = newstate;
	EnteringState(priorstate);
}

void PopScreenState(void)
{
	ExitingState();
	assert(ScreenStateStackPos > 0);
	EDIT_SCREEN_STATES priorstate = ScreenState;
	ScreenState = ScreenStateStack[--ScreenStateStackPos];
	EnteringState(priorstate);
}

EDIT_SCREEN_STATES GetScreenState(void)
{
	return ScreenState;
}

//**********************************************************************************************************************
// 
//**********************************************************************************************************************

void AnimatePopup(REAL elapsedtime)
{
	if(PopupState != PMS_INACTIVE)
	{
		if((PopupState == PMS_MOVING_UP) || (PopupState == PMS_MOVING_DOWN))
		{
			if(PopupCursorTime > elapsedtime)
			{
				PopupCursorTime -= elapsedtime;
			}
			else
			{
				PopupCursorTime = 0.0f;
				PopupState = PMS_ACTIVE;
			}
			PopupCursorFrame = 7 - (((U16)(PopupCursorTime * 20)) % 8);
		}

		if(PopupState == PMS_APPEARING)
		{
			if(PopupMenuTime > elapsedtime)
			{
				PopupMenuTime -= elapsedtime;
			}
			else
			{
				PopupMenuTime = 0.0f;
				PopupState = PMS_ACTIVE;
			}
			REAL portion = PopupMenuTime / PopupMenuDuration;
			PopupMenuPosition = (U16)(PopupSlideDistance * portion);
		}

		if(PopupState == PMS_DISAPPEARING)
		{
			if(PopupMenuTime > elapsedtime)
			{
				PopupMenuTime -= elapsedtime;
			}
			else
			{
				PopupMenuTime = 0.0f;
				PopupState = PMS_INACTIVE;
				if(PopupAction == PMA_SELECTED)
				{
					SetScreenState(static_cast<EDIT_SCREEN_STATES>(PopupCursorPosition));
					PopupAction = PMA_DISMISSED;
				}
			}
			REAL portion = PopupMenuTime / PopupMenuDuration;
			PopupMenuPosition = (U16)(PopupSlideDistance - (PopupSlideDistance * portion));
		}
	}
}

void AnimateCursor(REAL elapsedtime)
{
	REAL interval = Real(0.5f);
	REAL timescale = ONE / interval;

	int progress = (int)(RunningTime * timescale);
	if(progress & 0x01)
	{
		UpdateTrackAndCursor(&TrackData);
	}
	else
	{
		MakeTrackFromModules(&TrackData);
	}

	if(CursorFlashTime > elapsedtime)
	{
		CursorFlashTime -= elapsedtime;
	}
	else
	{
		CursorFlashTime = ZERO;
	}
}

void UpdatePlacementCamera(REAL elapsedtime)
{
	static REVOLTVECTOR		up = {ZERO, ONE, ZERO};
	REAL adjustedvelocity = CameraVelocity * elapsedtime;
	if(fabs(CameraDeflection) > fabs(adjustedvelocity))
	{
		CameraDeflection -= adjustedvelocity;
		D3DRMVectorRotate((D3DVECTOR*)&CameraPos, (D3DVECTOR*)&CameraPos, (D3DVECTOR*)&up, adjustedvelocity);
		D3DRMVectorScale((D3DVECTOR*)&CameraPos, (D3DVECTOR*)&CameraPos, CameraDistance);
	}
	else
	{
		CameraVelocity = ZERO;
		CameraDeflection = ZERO;
		CameraPos = Viewpoints[PlacementViewpoint];
	}
}

void HandleArrowFlashes(REAL elapsedtime)
{
	for(U16 n = 0; n < 4; n++)
	{
		if(ButtonFlashTime[n] > elapsedtime)
		{
			ButtonFlashTime[n] -= elapsedtime;
		}
		else
		{
			ButtonFlashTime[n] = ZERO;
		}
	}
}

//**********************************************************************************************************************
// 
//**********************************************************************************************************************

void HeartBeat(REAL elapsedtime)
{
	if(!QueryExists())
	{
		switch(GetScreenState())
		{
			case ESS_EXPORTING_TRACK:
				CompileTrack(&TrackData, &Theme, &TrackCursor);
			break;

			case ESS_PLACING_MODULE:
				AnimateCursor(elapsedtime);
				UpdatePlacementCamera(elapsedtime);
			break;

			case ESS_CREATING_TRACK:
				NewTrack(&TrackData);
				PopScreenState();
			break;

			case ESS_ADJUSTING_TRACK:
			case ESS_LOADING_TRACK:
				HandleArrowFlashes(elapsedtime);
			break;
			
			case ESS_PLACING_PICKUPS:
				PickupSpin += elapsedtime;
			break;
			
			case ESS_QUITTING:
				if(TrackData.Dirty == false)
				{
					QuitApp();
				}
			break;
		}
		
		AnimatePopup(elapsedtime);

	}
}
