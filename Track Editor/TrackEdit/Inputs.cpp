#include <revolt.h>

#include "TrackEditTypes.h"
#include "UnitInfo.h"
#include "Inputs.h"
#include "Editor.h"
#include "fileio.h"
#include "compile.h"
#include "states.h"
#include "platform.h"
#include "textstrings.h"
#include "editorhelp.h"

extern POPUP_MENU_STATES		PopupState;
extern POPUP_MENU_ACTIONS		PopupAction;
extern U16						PopupMenuPosition;
extern REAL						PopupMenuTime;
extern U16						PopupCursorFrame;
extern U16						PopupCursorPosition;
extern REAL						PopupCursorTime;
extern SAVE_STATES				SaveState;

extern PLACEMENT_CAMERA_DIR		PlacementViewpoint;
extern CURSORDESC				PickupCursor;
extern CURSORDESC				ModuleCursor;
extern CURSORDESC				TrackCursor;
extern INDEX					CurrentModule;
extern S16						ModuleElevation;
extern TRACKDESC				TrackData;
extern TRACKDESC				UndoTrack;
extern TRACKTHEME				Theme;
extern U16						FileWindowStart;
extern U16						FileWindowOffset;
extern char**					FileList;
extern U16						FileCount;
extern char						SaveName[MAX_DESCRIPTION_LENGTH];
extern U16						SaveSymbol;
extern REVOLTVECTOR				CameraPos;
extern REAL						CameraVelocity;
extern REAL						CameraDeflection;
extern REAL						ButtonFlashTime[4];
extern REAL						CursorFlashTime;

void	Handle_New_Track_Input(void);
void	Handle_Module_Placement(void);
void	Handle_Module_Selection(void);
void	Handle_Load_Menu_Input(void);
void	Handle_Save_Track_Input(void);
void	Handle_Adjust_Track_Input(void);
void	Handle_Pickup_Placement(void);
void	Process_Placement_Keys(void);
void	Process_Popup_Keys(void);
void	HandleQueryKeys(void);

void HandleQueryKeys(void)
{
	if(ButtonJustWentDown(BUTTON_LEFT))
	{
		PreviousQueryState();
	}

	if(ButtonJustWentDown(BUTTON_RIGHT))
	{
		NextQueryState();
	}

	if(ButtonJustWentDown(BUTTON_CONFIRM))
	{
		CancelQuery();
	}
}

CURSOR_DIR ViewpointAdjustedKey(CURSOR_DIR direction)
{
	CURSOR_DIR adjustedkeys[] = {CURSOR_UP, CURSOR_LEFT, CURSOR_DOWN, CURSOR_RIGHT, CURSOR_UP, CURSOR_LEFT, CURSOR_DOWN};

	int offset = PlacementViewpoint;
	switch(direction)
	{
		case CURSOR_UP:
		break;

		case CURSOR_RIGHT:
			offset += 3;
		break;

		case CURSOR_DOWN:
			offset += 2;
		break;

		case CURSOR_LEFT:
			offset += 1;
		break;
	}
	return adjustedkeys[offset];
}

void Handle_Pickup_Placement(void)
{
	if(ButtonJustWentDown(BUTTON_LEFT))
	{
		StartSound(SND_MENU_MOVE);
		MoveCursor(&PickupCursor, CURSOR_LEFT);
	}
	if(ButtonJustWentDown(BUTTON_RIGHT))
	{
		StartSound(SND_MENU_MOVE);
		MoveCursor(&PickupCursor, CURSOR_RIGHT);
	}
	if(ButtonJustWentDown(BUTTON_DOWN))
	{
		StartSound(SND_MENU_MOVE);
		MoveCursor(&PickupCursor, CURSOR_DOWN);
	}
	if(ButtonJustWentDown(BUTTON_UP))
	{
		StartSound(SND_MENU_MOVE);
		MoveCursor(&PickupCursor, CURSOR_UP);
	}
	if(ButtonJustWentDown(BUTTON_CANCEL))
	{
		FlagQuery(TEXT_QUERY_KEEP_CHANGES, true);
	}
	if(ButtonJustWentDown(BUTTON_PLACE_MODULE))
	{
		TogglePickup(&TrackData, PickupCursor.X, PickupCursor.Y);
	}
}

void Process_Popup_Keys(void)
{
	if(ButtonJustWentDown(BUTTON_CANCEL))
	{
		if(PopupState == PMS_ACTIVE)
		{
			StartSound(SND_MENU_OUT);
			PopupState = PMS_DISAPPEARING;
			PopupAction = PMA_DISMISSED;
			PopupMenuPosition = 0;
			PopupMenuTime = PopupMenuDuration;
		}
	}

	if(ButtonJustWentDown(BUTTON_DOWN))
	{
		StartSound(SND_MENU_MOVE);
		PopupCursorPosition++;
		PopupCursorPosition %= (ESS_QUITTING + 1);
		PopupState = PMS_MOVING_DOWN;
		PopupCursorTime = 0.4f;
	}

	if(ButtonJustWentDown(BUTTON_UP))
	{
		StartSound(SND_MENU_MOVE);
		PopupCursorPosition += ESS_QUITTING;
		PopupCursorPosition %= (ESS_QUITTING + 1);
		PopupState = PMS_MOVING_UP;
		PopupCursorTime = 0.4f;
	}

	if(ButtonJustWentDown(BUTTON_CONFIRM))
	{
		StartSound(SND_MENU_OUT);
		PopupState = PMS_DISAPPEARING;
		PopupAction = PMA_SELECTED;
		PopupMenuPosition = 0;
		PopupMenuTime = PopupMenuDuration;
	}
}

void Handle_Module_Placement(void)
{
	if(PopupState == PMS_INACTIVE)
	{
		Process_Placement_Keys();
	}
	else
	{
		if((PopupState != PMS_APPEARING) && (PopupState != PMS_DISAPPEARING))
		{
			Process_Popup_Keys();
		}
	}
}

void Handle_Module_Selection(void)
{
	if(ButtonJustWentDown(BUTTON_DOWN))
	{
		StartSound(SND_MENU_MOVE);
		ModuleCursor.Y++;
		ModuleCursor.Y %= ModuleCursor.YMax;
	}
	if(ButtonJustWentDown(BUTTON_UP))
	{
		StartSound(SND_MENU_MOVE);
		ModuleCursor.Y += ModuleCursor.YMax - 1;
		ModuleCursor.Y %= ModuleCursor.YMax;
	}
	if(ButtonJustWentDown(BUTTON_LEFT))
	{
		StartSound(SND_MENU_MOVE);
		MoveCursor(&ModuleCursor, CURSOR_LEFT);
	}
	if(ButtonJustWentDown(BUTTON_RIGHT))
	{
		StartSound(SND_MENU_MOVE);
		MoveCursor(&ModuleCursor, CURSOR_RIGHT);
	}
	if(ButtonJustWentDown(BUTTON_CANCEL))
	{
		StartSound(SND_MENU_MOVE);
		PopScreenState();
	}
	if(ButtonJustWentDown(BUTTON_CONFIRM))
	{
		StartSound(SND_MENU_MOVE);
		CurrentModule = Theme.Lookup->Groups[ModuleCursor.Y].ModuleID;
		if(ModuleCursor.X > 0)
		{
			if(CurrentModule != Theme.Lookup->Groups[ModuleCursor.Y].AlternateID)
			{
				CurrentModule = Theme.Lookup->Groups[ModuleCursor.Y].AlternateID;
			}
		}
		SetTrackCursorSize();
		PopScreenState();
	}
}

void Handle_Adjust_Track_Input(void)
{
	if(ButtonIsDown(BUTTON_SHIFT))
	{
		if(ButtonJustWentDown(BUTTON_LEFT))
		{
			SlideTrackLeft(&TrackData);
			ButtonFlashTime[WEST] = ArrowFlashTime;
		}
		if(ButtonJustWentDown(BUTTON_RIGHT))
		{
			SlideTrackRight(&TrackData);
			ButtonFlashTime[EAST] = ArrowFlashTime;
		}
		if(ButtonJustWentDown(BUTTON_DOWN))
		{
			SlideTrackDown(&TrackData);
			ButtonFlashTime[SOUTH] = ArrowFlashTime;
		}
		if(ButtonJustWentDown(BUTTON_UP))
		{
			SlideTrackUp(&TrackData);
			ButtonFlashTime[NORTH] = ArrowFlashTime;
		}
	}
	else
	{
		if(ButtonJustWentDown(BUTTON_LEFT))
		{
			DecreaseTrackWidth(&TrackData);
			ButtonFlashTime[WEST] = ArrowFlashTime;
		}
		if(ButtonJustWentDown(BUTTON_RIGHT))
		{
			IncreaseTrackWidth(&TrackData);
			ButtonFlashTime[EAST] = ArrowFlashTime;
		}
		if(ButtonJustWentDown(BUTTON_DOWN))
		{
			DecreaseTrackHeight(&TrackData);
			ButtonFlashTime[SOUTH] = ArrowFlashTime;
		}
		if(ButtonJustWentDown(BUTTON_UP))
		{
			IncreaseTrackHeight(&TrackData);
			ButtonFlashTime[NORTH] = ArrowFlashTime;
		}
	}
	if(ButtonJustWentDown(BUTTON_CANCEL))
	{
		FlagQuery(TEXT_QUERY_KEEP_CHANGES, true);
	}
}

void Handle_Load_Menu_Input()
{
	if((FileWindowOffset + FileWindowStart) >= FileCount)
	{
		if(FileWindowOffset > 0)
		{
			FileWindowOffset--;
		}
		else
		{
			if(FileWindowStart > 0)
			{
				FileWindowStart--;
			}
		}
	}
	if(ButtonJustWentDown(BUTTON_UP))
	{
		if(FileWindowOffset > 0)
		{
			StartSound(SND_MENU_MOVE);
			FileWindowOffset--;
		}
		else
		{
			if(FileWindowStart > 0)
			{
				StartSound(SND_MENU_MOVE);
				ButtonFlashTime[NORTH] = ArrowFlashTime;
				FileWindowStart--;
			}
			else
			{
				StartSound(SND_WARNING);
			}
		}
	}

	if(ButtonJustWentDown(BUTTON_DOWN))
	{
		if(FileWindowOffset < (MAX_FILES_IN_WINDOW - 1))
		{
			if((FileWindowOffset + FileWindowStart) < (FileCount - 1))
			{
				StartSound(SND_MENU_MOVE);
				FileWindowOffset++;
			}
			else
			{
				StartSound(SND_WARNING);
			}
		}
		else
		{
			if(FileWindowStart < (FileCount - MAX_FILES_IN_WINDOW))
			{
				StartSound(SND_MENU_MOVE);
				ButtonFlashTime[SOUTH] = ArrowFlashTime;
				FileWindowStart++;
			}
			else
			{
				StartSound(SND_WARNING);
			}
		}
	}

	if(ButtonJustWentDown(BUTTON_NEXT))
	{
		StartSound(SND_MENU_MOVE);
		ButtonFlashTime[SOUTH] = ArrowFlashTime;
		FileWindowStart = min(FileWindowStart + MAX_FILES_IN_WINDOW, FileCount - MAX_FILES_IN_WINDOW);
	}

	if(ButtonJustWentDown(BUTTON_PRIOR))
	{
		StartSound(SND_MENU_MOVE);
		ButtonFlashTime[NORTH] = ArrowFlashTime;
		if(FileWindowStart >= MAX_FILES_IN_WINDOW)
		{
			FileWindowStart -= MAX_FILES_IN_WINDOW;
		}
		else
		{
			FileWindowStart = 0;
		}
	}

	if(ButtonJustWentDown(BUTTON_CONFIRM) || ButtonJustWentDown(BUTTON_PLACE_MODULE))
	{
		if(DescribedFileExists(FileList[FileWindowStart + FileWindowOffset]))
		{
			char filename[MAX_PATH];
			GetDescribedFilename(FileList[FileWindowStart + FileWindowOffset], filename);
			ReadTDF(filename, &TrackData, false);
			CurrentModule = MODULE_STARTGRID;
			ModuleElevation = 0;
			PopScreenState();
		}
	}

#if 1
	if(ButtonJustWentDown(BUTTON_ROT_LEFT))
	{
		char filename[MAX_PATH];
		for(int i = 0; i < FileCount; i++)
		{
			if(GetDescribedFilename(FileList[i], filename))
			{
				ReadTDF(filename, &TrackData, true);
				WriteTDF(filename, &TrackData);
			}
		}
	}
#endif

	if(ButtonJustWentDown(BUTTON_CANCEL))
	{
		PopScreenState();
	}

	if(ButtonJustWentDown(BUTTON_DELETE))
	{
		char querytext[256];
		sprintf(querytext, GetTextString(TEXT_QUERY_DELETE), FileList[FileWindowStart + FileWindowOffset]);
		//can only switch to deleting state after we have used the FileList entry
		//as the FileList will be destroyed as a result of switching
		SetScreenState(ESS_DELETING_TRACK);
		FlagQuery(querytext, false);
	}
}

void Handle_Save_Track_Input(void)
{
	U16 textindex = strlen(SaveName);

	if(SaveState == SS_PICK)
	{
		if(ButtonJustWentDown(BUTTON_LEFT))
		{
			StartSound(SND_TYPING);
			SaveSymbol += (96 - 1);
		}
		if(ButtonJustWentDown(BUTTON_RIGHT))
		{
			StartSound(SND_TYPING);
			SaveSymbol += 1;
		}
		if(ButtonJustWentDown(BUTTON_DOWN))
		{
			StartSound(SND_TYPING);
			if(SaveSymbol >= (96 - 16))
			{
				SaveState = SS_CONFIRM;
			}
			else
			{
				SaveSymbol += 16;
			}
		}
		if(ButtonJustWentDown(BUTTON_UP))
		{
			StartSound(SND_TYPING);
			SaveSymbol += (96 - 16);
		}
		if(ButtonJustWentDown(BUTTON_CONFIRM))
		{
			StartSound(SND_TYPING);
			switch(SaveSymbol)
			{
				case 94:
					if(textindex > 0)
					{
						SaveName[--textindex] = '\0';
					}
				break;

				case 95:
					if(textindex < (MAX_DESCRIPTION_LENGTH - 1))
					{
						SaveName[textindex++] = ' ';
						SaveName[textindex] = '\0';
					}
				break;
				
				default:
					if(textindex < (MAX_DESCRIPTION_LENGTH - 1))
					{
						SaveName[textindex++] = '!' + SaveSymbol;
						SaveName[textindex] = '\0';
					}
				break;
			}
		}

		SaveSymbol %= 96;
	}

	if(SaveState == SS_CONFIRM)
	{
		if(ButtonJustWentDown(BUTTON_CONFIRM))
		{
			strcpy(TrackData.Name, SaveName);
			if(SaveTrack(&TrackData))
			{
				FlagNotification(TEXT_INFO_SAVED);
			}
			else
			{
				FlagError(TEXT_WARNING_SAVE_ERROR);
			}
			PopScreenState();
		}

		if(ButtonJustWentDown(BUTTON_UP))
		{
			StartSound(SND_TYPING);
			SaveState = SS_PICK;
		}
	}
	
	if(ButtonJustWentDown(BUTTON_ERASE))
	{
		StartSound(SND_TYPING);
		if(textindex > 0)
		{
			SaveName[--textindex] = '\0';
		}
	}
	
	if(ButtonJustWentDown(BUTTON_CANCEL))
	{
		PopScreenState();
	}
}

bool CursorPosIsValid(CURSORDESC* cursor, U16 testx, U16 testy)
{
	bool isvalid = true;
	if((testx >= cursor->XMax) && (testy >= cursor->YMax))
	{
		isvalid = false;
	}
	else
	{
		S16 var1, var2, xsize, ysize;

		var1 = max(cursor->XSize, 1);
		var2 = min(cursor->XSize, 1);
		var2 = abs(var2);
		xsize = var1 - var2;

		var1 = max(cursor->YSize, 1);
		var2 = min(cursor->YSize, 1);
		var2 = abs(var2);
		ysize = var1 - var2;

		S16 left = min(testx, testx + xsize);
		S16 right = max(testx, testx + xsize);
		S16 bottom = min(testy, testy + ysize);
		S16 top = max(testy, testy + ysize);

		if((left < 0) || (right >= cursor->XMax))
		{
			isvalid = false;
		}
		else
		{
			if((bottom < 0) || (top >= cursor->YMax))
			{
				isvalid = false;
			}
		}
	}
	return isvalid;
}

void MoveCursor(CURSORDESC* cursor, CURSOR_DIR direction)
{
	if(cursor != NULL)
	{
		S16 newx = cursor->X;
		S16 newy = cursor->Y;
		switch(direction)
		{
			case CURSOR_LEFT:
				if(newx > 0)
				{
					newx--;
				}
			break;

			case CURSOR_RIGHT:
				newx++;
			break;

			case CURSOR_DOWN:
				if(newy > 0)
				{
					newy--;
				}
			break;

			case CURSOR_UP:
				newy++;
			break;
		}
		if(CursorPosIsValid(cursor, newx, newy))
		{
			cursor->X = newx;
			cursor->Y = newy;
		}
	}
}

void App_HandleInputs(void)
{
	if(HelpExists())
	{
		if(ButtonJustWentDown(BUTTON_CANCEL))
		{
			HideHelp();
		}
	}
	else
	{
		if(ErrorExists())
		{
			if(ButtonJustWentDown(BUTTON_CONFIRM) || ButtonJustWentDown(BUTTON_CANCEL))
			{
				CancelError();
			}
		}
		else
		{
			if(QueryExists())
			{
				HandleQueryKeys();
			}
			else
			{
				if(NotificationExists())
				{
					if(ButtonJustWentDown(BUTTON_CONFIRM) || ButtonJustWentDown(BUTTON_CANCEL))
					{
						CancelNotification();
					}
				}
				else
				{
					switch(GetScreenState())
					{
						case ESS_PLACING_MODULE:
							Handle_Module_Placement();
						break;

						case ESS_CHOOSING_MODULE:
							Handle_Module_Selection();
						break;

						case ESS_LOADING_TRACK:	//user is selecting a pre-created track
							Handle_Load_Menu_Input();
						break;

						case ESS_SAVING_TRACK:	//user is saving a track
							Handle_Save_Track_Input();
						break;

						case ESS_PLACING_PICKUPS:	//user is plaing pickups
							Handle_Pickup_Placement();
						break;

						case ESS_ADJUSTING_TRACK:	//user is resizing/shifting the track
							Handle_Adjust_Track_Input();
						break;

					}
				}
			}
		}
	}
}

void Process_Placement_Keys(void)
{
	static PLACEMENT_CAMERA_DIR nextviewpoint[4] = {PCD_EAST, PCD_SOUTH, PCD_WEST, PCD_NORTH};
	static PLACEMENT_CAMERA_DIR prevviewpoint[4] = {PCD_WEST, PCD_NORTH, PCD_EAST, PCD_SOUTH};

	if(ButtonJustWentDown(BUTTON_LEFT))
	{
		StartSound(SND_MENU_MOVE);
		MoveCursor(&TrackCursor, ViewpointAdjustedKey(CURSOR_LEFT));
	}
	if(ButtonJustWentDown(BUTTON_RIGHT))
	{
		StartSound(SND_MENU_MOVE);
		MoveCursor(&TrackCursor, ViewpointAdjustedKey(CURSOR_RIGHT));
	}
	if(ButtonJustWentDown(BUTTON_UP))
	{
		StartSound(SND_MENU_MOVE);
		MoveCursor(&TrackCursor, ViewpointAdjustedKey(CURSOR_UP));
	}
	if(ButtonJustWentDown(BUTTON_DOWN))
	{
		StartSound(SND_MENU_MOVE);
		MoveCursor(&TrackCursor, ViewpointAdjustedKey(CURSOR_DOWN));
	}

	if(ButtonJustWentDown(BUTTON_ROT_LEFT))
	{
		StartSound(SND_ROTATE);
		if(CameraDeflection > ZERO)
		{
			while(CameraDeflection > (PI / 2))
			{
				PlacementViewpoint = nextviewpoint[PlacementViewpoint];
				CameraDeflection -= (PI / 2);
			}
			if(CameraDeflection > ZERO)
			{
				PlacementViewpoint = nextviewpoint[PlacementViewpoint];
				CameraDeflection = CameraDeflection - (PI / 2);
				CameraVelocity = -PI;
			}
		}
		else
		{
			PlacementViewpoint = nextviewpoint[PlacementViewpoint];
			CameraDeflection -= (PI / 2);
			CameraVelocity = -PI;
		}
	}

	if(ButtonJustWentDown(BUTTON_ROT_RIGHT))
	{
		StartSound(SND_ROTATE);
		if(CameraDeflection < ZERO)
		{
			while(CameraDeflection < -(PI / 2))
			{
				PlacementViewpoint = prevviewpoint[PlacementViewpoint];
				CameraDeflection += (PI / 2);
			}
			if(CameraDeflection < ZERO)
			{
				PlacementViewpoint = prevviewpoint[PlacementViewpoint];
				CameraDeflection = CameraDeflection + (PI / 2);
				CameraVelocity = PI;
			}
		}
		else
		{
			PlacementViewpoint = prevviewpoint[PlacementViewpoint];
			CameraDeflection += (PI / 2);
			CameraVelocity = PI;
		}
	}
	
	if(ButtonJustWentDown(BUTTON_PRIOR))
	{
		IncreaseElevation();
		if(IncreaseElevation())
		{
			StartSound(SND_RAISE);
		}
		else
		{
			StartSound(SND_WARNING);
		}
	}

	if(ButtonJustWentDown(BUTTON_NEXT))
	{
		if(DecreaseElevation())
		{
			StartSound(SND_LOWER);
		}
		else
		{
			StartSound(SND_WARNING);
		}
	}

	if(ButtonJustWentDown(BUTTON_ROTATE_MODULE))
	{
		RotateModule();
	}

	if(ButtonJustWentDown(BUTTON_PLACE_MODULE))
	{
		StartSound(SND_PLACE_MODULE);
		PlaceModule(&TrackData);	//place the currently selected module onto the track
		CursorFlashTime = 0.2f;
	}

	if(ButtonJustWentDown(BUTTON_DELETE))
	{
		DeleteModule(&TrackData);
	}

	if(ButtonJustWentDown(BUTTON_PICKUP))
	{
		if(SelectModuleUnderCursor(&TrackData))
		{
			StartSound(SND_ADJUST);
		}
		else
		{
			StartSound(SND_WARNING);
		}
		CursorFlashTime = 0.2f;
	}

	if(ButtonJustWentDown(BUTTON_CANCEL))
	{
		StartSound(SND_MENU_IN);
		PopupState = PMS_APPEARING;
		PopupMenuPosition = PopupSlideDistance;
		PopupMenuTime = PopupMenuDuration;
		PopupCursorPosition = 0;
		PopupCursorFrame = 0;
	}

	if(ButtonJustWentDown(BUTTON_NEXT_VARIANT))
	{
		if(NextVariant())
		{
			StartSound(SND_LOWER);
		}
		else
		{
			StartSound(SND_WARNING);
		}
	}

	if(ButtonJustWentDown(BUTTON_PREVIOUS_VARIANT))
	{
		
		if(PreviousVariant())
		{
			StartSound(SND_RAISE);
		}
		else
		{
			StartSound(SND_WARNING);
		}
	}

	if(ButtonJustWentDown(BUTTON_TEXTURE_TOGGLE))
	{
		if(ToggleSurface())
		{
			StartSound(SND_RAISE);
		}
		else
		{
			StartSound(SND_WARNING);
		}
	}
}

#ifdef _PC
void Handle_Query_Typing(HWND /*hWnd*/, int vKeyCode)
{
	StartSound(SND_TYPING);
	int key = (int)MapVirtualKey(vKeyCode, 2);

	switch(key)
	{
		case 'Y':
		case 'y':
			SetQueryToYes();
			CancelQuery();
		break;
		
		case 'N':
		case 'n':
			SetQueryToNo();
			CancelQuery();
		break;
	}
}

void Handle_Save_Name_Typing(HWND /*hWnd*/, int vKeyCode)
{
	if(!HelpExists())
	{
		StartSound(SND_TYPING);
		U16 textindex = strlen(SaveName);
		int key = (int)MapVirtualKey(vKeyCode, 2);

		switch(key)
		{
			case VK_BACK:
				if(textindex > 0)
				{
					SaveName[--textindex] = '\0';
				}
			break;
			
			default:
				if(isalnum(key) || (key == ' '))
				{
					if((GetAsyncKeyState(VK_SHIFT) & 0x8000) == 0)
					{
						key = tolower(key);
					}
					if(textindex < (MAX_DESCRIPTION_LENGTH - 1))
					{
						SaveName[textindex++] = key;
						SaveName[textindex] = '\0';
					}
				}
			break;
		}
	}
}
#endif
