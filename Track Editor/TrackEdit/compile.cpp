//**********************************************
//
// Track compiler module
//
// All of the code concerned with compiling a
// track description into a game-ready track
//
//**********************************************

#include <assert.h>
#include <revolt.h>
#include "UnitInfo.h"
#include <world.h>
#include <newcoll.h>
#include <util.h>
#include <object.h>
#include <light.h>
#include "fileio.h"
#include "editor.h"
#include "compile.h"
#include "render.h"
#include "states.h"
#include "modules.h"
#include "platform.h"
#include "textstrings.h"

#define OUTPUT_WALLS 1

#define ApproxWithin(value1, value2, tolerance) ( (abs((value1) - (value2)) < (tolerance))? TRUE: FALSE)

static const REAL PLANE_TOLERANCE = 0.01f;
static const REAL AXIS_TOLERANCE = 0.01f;
static const REAL NORMAL_TOLERANCE = 0.01f;

//**********************************************
//
//**********************************************
#define POLY_QUAD 1		
#define TPAGE_NONE -1

//**********************************************
//
//**********************************************
extern COMPILE_STATES		CompileState;
extern U8					ClockFrame;
extern MODULE_CHANGES		ToyChanges[];
#ifdef NEW_WALLS
extern REVOLTMATRIX			WallMatrix[6];
#else
extern REVOLTMATRIX			WallMatrix[5];
#endif
extern char					LevelFolder[];

//**********************************************
//
//**********************************************
U16							SmallCubeCount;
SMALLCUBE*					SmallCubes = NULL;
U16							BigCubeCount;
BIGCUBE*					BigCubes = NULL;
NCPDATA						CollisionData = {0, 0, NULL, NULL, {ZERO, ZERO, ZERO, ZERO, ZERO}, NULL};
U16							CollisionCellCount;
TRACKZONE*					ZoneBuffer = NULL;
ZONESEQENTRY*				ZoneSequence = NULL;
INDEX						ZonesVisited;
POSNODE*					PosNodeBuffer = NULL;
INDEX						PosnNodeCount;
REAL						LapDistance;
U16							AINodeCount;
U16							AINodesPlaced;
AINODEINFO*					AINodes = NULL;
bool						AINodesValid;
U16							LightCount;
U16							LightsPlaced;
FILELIGHT*					Lights = NULL;

static TRACKDESC*			LocalTrack = NULL;
static const TRACKTHEME*	LocalTheme = NULL;
static CURSORDESC*			LocalCursor = NULL;
static U16					ZonesInTrack;

static INDEX				CurrentZone;
static CUBE_HEADER_LOAD*	UnitCuboids = NULL;
static CUBE_HEADER_LOAD		WorldCuboid;
static U16					CubesInWorldX;
static U16					CubesInWorldY;
static U16					CubesInWorldZ;
static INDEX*				LinkTable = NULL;
static INDEX				PreviousModuleIndex;
static INDEX				PreviousZoneIndex;
static bool					PreviouslyInAPipe;

static PIPEWELD				PipeWelds[] = {
							{TWM_PIPE_2, TWM_PIPE_1, TWM_PIPE_1A},
							{TWM_PIPE_1, TWM_PIPE_1, TWM_PIPE_0},
							{TWM_PIPE_1A, TWM_PIPE_0, TWM_PIPE_1A},
							{TWM_PIPEC_2, TWM_PIPEC_1, TWM_PIPEC_1A},
							{TWM_PIPEC_1, TWM_PIPEC_1, TWM_PIPEC_0},
							{TWM_PIPEC_1A, TWM_PIPEC_0, TWM_PIPEC_1A},
							{TWM_PIPE_20_2, TWM_PIPE_20_1_BOT, TWM_PIPE_20_1_TOP},
							{TWM_PIPE_20_1_BOT, TWM_PIPE_20_1_BOT, TWM_PIPE_20_0},
							{TWM_PIPE_20_1_TOP, TWM_PIPE_20_0, TWM_PIPE_20_1_TOP},
							};

//entries in this table give the amount of non-zero bits in the index value
static U16					EdgeCount[16] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};
static U16					RootEdges;

static RevoltVertex			StartPos;
static REAL					StartDir;

static REVOLTVECTOR			UnitRootVects[16];
static const REAL			unitradius = SMALL_CUBE_SIZE / Real(2.0f);	//distance along x or z axis from centre of unit to its edge
																		//this isn't really the radius but is the most appropriate name 
static const REVOLTVECTOR	InitUnitRootVects[16] = {
							{-unitradius, unitradius, unitradius},
							{unitradius, unitradius, unitradius},
							{unitradius, unitradius, unitradius},
							{unitradius, unitradius, -unitradius},
							{unitradius, unitradius, -unitradius},
							{-unitradius, unitradius, -unitradius},
							{-unitradius, unitradius, -unitradius},
							{-unitradius, unitradius, unitradius},
							{-unitradius, 0, unitradius},
							{unitradius, 0, unitradius},
							{unitradius, 0, unitradius},
							{unitradius, 0, -unitradius},
							{unitradius, 0, -unitradius},
							{-unitradius, 0, -unitradius},
							{-unitradius, 0, -unitradius},
							{-unitradius, 0, unitradius},
							};

static const U8				CLOCK_RING_DURATION = 86;		//amount of time that the clock is ringing for
static const U8				CLOCK_STATIC_DURATION = 192;		//amount of time that the clock if static for - after ringing

static const int			LOWER_DECK_REVERSED = 1;
static const int			UPPER_DECK_REVERSED = 2;
static DIRECTION			ReverseDirection[4] = {SOUTH, WEST, NORTH, EAST};

void	ForEachModule(void (*ModuleFunc)(INDEX moduleindex, INDEX zoneindex) );
void	ForEachUnit(void (*UnitFunc)(const CUBE_HEADER_LOAD* srccuboid, const TRACKUNIT* unit, REVOLTMATRIX* matrix, U16 Elevation) );
void	CreateUnitCuboids(void);
void	FillCollisionCells(void);
bool	CreateAINodes(void);

//********************************************************************************************
//
//	First we have the general helper functions - those which carry out a commonly used task
//
//********************************************************************************************

void RotMatrixZYX(MAT *mat, REAL x, REAL y, REAL z)
{
	REAL cx, cy, cz, sx, sy, sz;

	cx = (REAL)cos(x * RAD);
	cy = (REAL)cos(-y * RAD);
	cz = (REAL)cos(z * RAD);
	sx = (REAL)sin(x * RAD);
	sy = (REAL)sin(-y * RAD);
	sz = (REAL)sin(z * RAD);

	mat->m[RX] = cy * cz;
	mat->m[RY] = -cy * sz;
	mat->m[RZ] = -sy;

	mat->m[UX] = cx * sz - sx * sy * cz;
	mat->m[UY] = sx * sy * sz + cx * cz;
	mat->m[UZ] = -sx * cy;

	mat->m[LX] = cx * sy * cz + sx * sz;
	mat->m[LY] = sx * cz - cx * sy * sz;
	mat->m[LZ] = cx * cy;
}

//**********************************************
//
// NullifyCuboid
//
// Initializes cuboid to an 'inside-out, maximized'
// cuboid. This is not a normal configuration
// as all of the min value are greater than all
// of the max values and is mainly used to
// indicate an illegal cuboid.
//
//**********************************************
void NullifyCuboid(CUBE_HEADER_LOAD* cuboid)
{
	assert(cuboid != NULL);

	cuboid->Xmin = FLT_MAX;
	cuboid->Ymin = FLT_MAX;
	cuboid->Zmin = FLT_MAX;

	cuboid->Xmax = -FLT_MAX;
	cuboid->Ymax = -FLT_MAX;
	cuboid->Zmax = -FLT_MAX;
}

//**********************************************
//
// AddVectorToCuboid
//
// If the vector specified is outside of the
// cuboid, the cuboid is expanded to a size which 
// includes it
//
//**********************************************
void AddVectorToCuboid(const REVOLTVECTOR* vect,  CUBE_HEADER_LOAD* cuboid)
{
	assert(vect != NULL);
	assert(cuboid != NULL);

	cuboid->Xmin = min(cuboid->Xmin, vect->X);
	cuboid->Ymin = min(cuboid->Ymin, vect->Y);
	cuboid->Zmin = min(cuboid->Zmin, vect->Z);

	cuboid->Xmax = max(cuboid->Xmax, vect->X);
	cuboid->Ymax = max(cuboid->Ymax, vect->Y);
	cuboid->Zmax = max(cuboid->Zmax, vect->Z);
}

S32 RoundDown(S32 value, S32 unit)
{
	S32 half = unit / 2;
	if(value == 0)
	{
		value = -half;
	}
	else
	{
		S16 frac = abs(value % unit);
		if(value > 0)
		{
			if(frac >= half)
			{
				value -= (frac - half);
			}
			else
			{
				value -= (frac + half);
			}
		}
		else
		{
			if(frac > half)
			{
				value -= ((unit + half) - frac);
			}
			else
			{
				value -= (half - frac);
			}
		}
	}
	return value;
}

S32 RoundUp(S32 value, S32 unit)
{
	S32 half = unit / 2;
	if(value == 0)
	{
		value = half;
	}
	else
	{
		S16 frac = abs(value % unit);
		if(value > 0)
		{
			if(frac > half)
			{
				value += (unit - (frac - half));
			}
			else
			{
				value += (half - frac);
			}
		}
		else
		{
			if(frac >= half)
			{
				value += (unit - (frac - half));
			}
			else
			{
				value += (half - frac);
			}
		}
	}
	return value;
}

void ExpandCuboid(CUBE_HEADER_LOAD* cuboid, REAL unit)
{
	assert(unit != ZERO);	//zero is not desirable - divide by zero will result
	
	cuboid->Xmin = RoundDown(cuboid->Xmin, SMALL_CUBE_SIZE);
	cuboid->Ymin = RoundDown(cuboid->Ymin, SMALL_CUBE_SIZE);
	cuboid->Zmin = RoundDown(cuboid->Zmin, SMALL_CUBE_SIZE);
	cuboid->Xmax = RoundUp(cuboid->Xmax, SMALL_CUBE_SIZE);
	cuboid->Ymax = RoundUp(cuboid->Ymax, SMALL_CUBE_SIZE);
	cuboid->Zmax = RoundUp(cuboid->Zmax, SMALL_CUBE_SIZE);
}

//**********************************************
//
// VertsInPoly
// 
// Simply returns the number of verts in a poly
//
//**********************************************
U16 VertsInPoly(const BASICPOLY* poly)
{
	assert(poly != NULL);
	return poly->IsTriangle ? (U16)3 : (U16)4;	
}

//**********************************************
//
// CopyCuboid - does exactly what it says on the tin
//
//**********************************************
void CopyCuboid(CUBE_HEADER_LOAD* dest, const CUBE_HEADER_LOAD* src)
{
	assert(dest != NULL);
	assert(src != NULL);

	dest->CentreX = src->CentreX;
	dest->CentreY = src->CentreY;
	dest->CentreZ = src->CentreZ;
	dest->Radius  = src->Radius;
	dest->Xmin	  = src->Xmin;
	dest->Ymin	  = src->Ymin;
	dest->Zmin	  = src->Zmin;
	dest->Xmax	  = src->Xmax;
	dest->Ymax	  = src->Ymax;
	dest->Zmax	  = src->Zmax;
	dest->PolyNum = src->PolyNum;
	dest->VertNum = src->VertNum;
}

//**********************************************
//
// CubesInCuboid
//
// Counts the number of 4 metre cubes within
// a cuboid - at present assumes that the cuboid
// is 4 metres square at the base, so only counts
// the number of cubes in the height (rounded up)
//
//**********************************************
U16 CubesInCuboid(const CUBE_HEADER_LOAD* cuboid)
{
	assert(cuboid != NULL);

	REAL y = cuboid->Ymin;
	U16 count = 0;

	if(y == cuboid->Ymax)
	{
		count = 1;
	}
	else
	{
		while(y < cuboid->Ymax)
		{
			y += SMALL_CUBE_SIZE;
			count++;
		}
	}
	return count;
}

//**********************************************
//
// TranslateCuboid
//
// Simple really - translate the specified 
// cuboid by the vector supplied.
//
//**********************************************
void TranslateCuboid(const REVOLTVECTOR* vect,  CUBE_HEADER_LOAD* cuboid)
{
	assert(vect != NULL);
	assert(cuboid != NULL);

	cuboid->Xmin += vect->X;
	cuboid->Ymin += vect->Y;
	cuboid->Zmin += vect->Z;
	cuboid->Xmax += vect->X;
	cuboid->Ymax += vect->Y;
	cuboid->Zmax += vect->Z;
}

//**********************************************
//
// TransformCuboid
//
// Simple really - transform the specified 
// cuboid by the matrix supplied.
//
//**********************************************
void TransformCuboid(CUBE_HEADER_LOAD* cuboid, REVOLTMATRIX* matrix)
{
	assert(cuboid != NULL);
	assert(matrix != NULL);

	REVOLTVECTOR temp;
	temp.X = cuboid->Xmin;
	temp.Y = cuboid->Ymin;
	temp.Z = cuboid->Zmin;

	TRANSFORM_VECTOR(temp, temp, *matrix);
	if(fabs(temp.X) < Real(0.25f))
	{
		temp.X = ZERO;
	}
	if(fabs(temp.Y) < Real(0.25f))
	{
		temp.Y = ZERO;
	}
	if(fabs(temp.Z) < Real(0.25f))
	{
		temp.Z = ZERO;
	}
	cuboid->Xmin = temp.X;
	cuboid->Ymin = temp.Y;
	cuboid->Zmin = temp.Z;

	temp.X = cuboid->Xmax;
	temp.Y = cuboid->Ymax;
	temp.Z = cuboid->Zmax;
	TRANSFORM_VECTOR( temp, temp, *matrix);
	if(fabs(temp.X) < Real(0.25f))
	{
		temp.X = ZERO;
	}
	if(fabs(temp.Y) < Real(0.25f))
	{
		temp.Y = ZERO;
	}
	if(fabs(temp.Z) < Real(0.25f))
	{
		temp.Z = ZERO;
	}
	cuboid->Xmax = temp.X;
	cuboid->Ymax = temp.Y;
	cuboid->Zmax = temp.Z;
}

//**********************************************
//
// AddPolysToCuboid
//
// Ensures that the cuboid contains all of the
// specified polygons by calling AddVectorToCuboid
// for each vertex of each polygon
//
//**********************************************
void AddPolysToCuboid(const POLYSET* polyset, CUBE_HEADER_LOAD* cuboid)
{
	assert(LocalTheme != NULL);
	assert(polyset != NULL);
	assert(cuboid != NULL);

	for(U32 p = 0; p < polyset->PolygonCount; p++)
	{
		BASICPOLY* poly = &LocalTheme->Polys[polyset->Indices[p] & ~GOURAUD_SHIFTED];

		U16 vertcount = VertsInPoly(poly);

		for(U16 v=0; v < vertcount; v++)
		{
			AddVectorToCuboid((RevoltVertex*)&LocalTheme->Verts[poly->Vertices[v]], cuboid);
		}
	}
}

//**********************************************
//
// CuboidOfUnit
//
// Calculates the bounding cuboid of the specified
// track unit.
// Note that only the first 2 components of the
// unit are added (PEG & PAN) as the collison
// polygons
//
//**********************************************
void CuboidOfUnit(const TRACKUNIT* unit, CUBE_HEADER_LOAD* cuboid)
{
	assert(LocalTheme != NULL);
	assert(unit != NULL);
	assert(cuboid != NULL);

	//first initialize cuboid to an 'inside-out, maximized' cuboid
	//this assures that the first point added causes the cuboid to be a singularity
	//(any point is 'outside' the cuboid and therefore causes it to change to include it)
	NullifyCuboid(cuboid);

	MESH* mesh = &LocalTheme->Meshes[unit->MeshID];			//get the appropriate mesh

	AddPolysToCuboid(&LocalTheme->PolySets[mesh->PolySetIndices[PEG_INDEX]], cuboid);	//and make the cuboid big enough to hold
	AddPolysToCuboid(&LocalTheme->PolySets[mesh->PolySetIndices[PAN_INDEX]], cuboid);	//all the polys from both the peg and the pan
}

//**********************************************
//
// CuboidFromVectors
//
// Calculates the bounding cuboid for the 
// supplied list of vertices
//
//**********************************************
void CuboidFromVectors(const REVOLTVECTOR* vect, U32 vcount, CUBE_HEADER_LOAD* cuboid)
{
	assert(vect != NULL);
	assert(cuboid != NULL);

	NullifyCuboid(cuboid);

	while(vcount--)
	{
		AddVectorToCuboid(vect++, cuboid);
	}
}

//********************************************************************************************
//
//	Now we have the more task-specific functions
//
//********************************************************************************************

void ForEachModule(void (*ModuleFunc)(INDEX moduleindex, INDEX zoneindex) )
{
	INDEX currentzone = 0;
	INDEX currentmodule = LocalTrack->Width * LocalTrack->Height;

	while(currentzone < ZonesVisited)
	{
		U16 n;
		do{
			if(currentzone >= ZonesVisited)
			{
				return;
			}
			TRACKZONE* zone = &ZoneBuffer[ZoneSequence[currentzone++].ZoneID];
			S16 xpos = (zone->Centre.X / SMALL_CUBE_SIZE);
			S16 ypos = (zone->Centre.Z / SMALL_CUBE_SIZE);	//yes, thats right - we use the Z coord to calculate the y position
			n = (ypos * LocalTrack->Width) + xpos;
			//find the offset to the modules master position
			xpos -= LocalTrack->Modules[n].XOffset;
			ypos -= LocalTrack->Modules[n].YOffset;
			
			//calculate the index for this element
			n = (ypos * LocalTrack->Width) + xpos;
		}while(n == currentmodule);
		
		currentmodule = n;
		(*ModuleFunc)(currentmodule, currentzone - 1);
	}
}

//**********************************************
//**********************************************
INDEX SmallCubeIndex(REAL x, REAL y, REAL z)
{
	S32 xpos, ypos, zpos;
	
	xpos = (x - WorldCuboid.Xmin) - SMALL_CUBE_HALF;
	ypos = (y - WorldCuboid.Ymin) - SMALL_CUBE_HALF;
	zpos = (z - WorldCuboid.Zmin) - SMALL_CUBE_HALF;
	if(xpos != 0)
	{
		xpos += (xpos / xpos) * SMALL_CUBE_HALF;
	}
	if(ypos != 0)
	{
		ypos += (ypos / ypos) * SMALL_CUBE_HALF;
	}
	if(zpos != 0)
	{
		zpos += (zpos / zpos) * SMALL_CUBE_HALF;
	}	
	xpos /= SMALL_CUBE_SIZE;
	ypos /= SMALL_CUBE_SIZE;
	zpos /= SMALL_CUBE_SIZE;
	
	assert(xpos >= 0);
	assert(ypos >= 0);
	assert(zpos >= 0);

	return xpos + (ypos * CubesInWorldX) + (zpos * CubesInWorldX * CubesInWorldY);
}

//**********************************************
// CountCollisionPolys
// 
// Counts the total number of collision polys
// which exist in the world
//
//**********************************************
S16 CountCollisionPolys(void)
{
	assert(LocalTrack != NULL);
	assert(LocalTheme != NULL);

	U16 polys = 0;
	U32 m = LocalTrack->Width * LocalTrack->Height;

	while(m--)
	{
		TRACKUNIT*	unit = LocalTheme->Units[LocalTrack->Units[m].UnitID];
		MESH*		mesh = &LocalTheme->Meshes[unit->MeshID];
		U16 ps = mesh->PolySetCount;
		while(ps-- > HULL_INDEX)
		{
			polys += LocalTheme->PolySets[mesh->PolySetIndices[ps]].PolygonCount;
		}
	}

#if OUTPUT_WALLS
	U32 w = LocalTheme->UnitCount - LocalTheme->WallIndex;
	while(w--)
	{
		TRACKUNIT*	unit = LocalTheme->Units[w + LocalTheme->WallIndex];
		MESH*		mesh = &LocalTheme->Meshes[unit->MeshID];
		U16 ps = mesh->PolySetCount;
		while(ps-- > HULL_INDEX)
		{
			polys += LocalTheme->PolySets[mesh->PolySetIndices[ps]].PolygonCount;
		}
	}
#endif

	return polys;
}

//**********************************************
//
// MakeUnitTransformationMatrix
//
// Builds a translation matrix which represents
// the location/orientation of the specified
// unit instance
//
//**********************************************
void MakeUnitTransformationMatrix(const RevoltTrackUnitInstance* instance, REVOLTMATRIX* matrix)
{
	assert(LocalTheme != NULL);
	assert(instance != NULL);

	REVOLTMATRIX TransMatrix;
	REVOLTMATRIX RotMatrix;

	MAKE_ROTATE_Y_MATRIX(RotMatrix, (REAL)((PI * HALF) * instance->Direction));

	MAKE_TRANSLATE_MATRIX(TransMatrix, instance->XPos * SMALL_CUBE_SIZE, instance->Elevation * -ElevationStep, instance->YPos * SMALL_CUBE_SIZE);

	MATRIX_MULTIPLY(*matrix, TransMatrix, RotMatrix);

}

//**********************************************
//
// MakeModuleTransformationMatrix
//
// Builds a translation matrix which represents
// the location/orientation of the specified
// module instance and xpos/ypos
//
//**********************************************
void MakeModuleTransformationMatrix(const RevoltTrackModuleInstance* instance, S16 xpos, S16 ypos, REVOLTMATRIX* matrix)
{
	assert(LocalTheme != NULL);
	assert(instance != NULL);

	REVOLTMATRIX TransMatrix;
	REVOLTMATRIX RotMatrix;

	MAKE_ROTATE_Y_MATRIX(RotMatrix, (REAL)((PI * HALF) * instance->Direction));

	MAKE_TRANSLATE_MATRIX(TransMatrix, xpos * SMALL_CUBE_SIZE, instance->Elevation * -ElevationStep, ypos * SMALL_CUBE_SIZE);

	MATRIX_MULTIPLY(*matrix, TransMatrix, RotMatrix);

}

//**********************************************
// CountTrackZones
// 
// Counts the total number of track zones
// placed on the track.
//
//**********************************************
U16 CountTrackZones(void)
{
	assert (LocalTrack != NULL);
	assert (LocalTheme != NULL);

	U16 zonecount = 0;
	U16 n = LocalTrack->Width * LocalTrack->Height;
	while(n--)
	{
		INDEX moduleid = LocalTrack->Modules[n].ModuleID;
		if(moduleid != MAX_INDEX)
		{
			zonecount += LocalTheme->Modules[moduleid]->ZoneCount;
		}
	}
	return zonecount;
}

//**********************************************
//
// TransformRevoltVertex
//
//
//**********************************************
void TransformRevoltVertex(RevoltVertex* vert, REVOLTMATRIX& matrix)
{
	TRANSFORM_VECTOR( *vert, *vert, matrix);
	if(fabs(vert->X) < Real(0.25f))
	{
		vert->X = ZERO;
	}
	if(fabs(vert->Y) < Real(0.25f))
	{
		vert->Y = ZERO;
	}
	if(fabs(vert->Z) < Real(0.25f))
	{
		vert->Z = ZERO;
	}
}


//**********************************************
//
// TransformRevoltVertex
//
//
//**********************************************
void TransformRevoltVertex(RevoltVertex* destvert, const RevoltVertex* srcvert, REVOLTMATRIX& matrix)
{
	TRANSFORM_VECTOR( *destvert, *srcvert, matrix);
	if(fabs(destvert->X) < Real(0.25f))
	{
		destvert->X = ZERO;
	}
	if(fabs(destvert->Y) < Real(0.25f))
	{
		destvert->Y = ZERO;
	}
	if(fabs(destvert->Z) < Real(0.25f))
	{
		destvert->Z = ZERO;
	}
}

//**********************************************
//
// CreateZoneList
//
// Builds a list of zones, transformed to their
// correct positions on the track.
//
//**********************************************
void CreateZoneList(void)
{
	S16 xpos, ypos;
	REVOLTMATRIX modulematrix;
	ZonesInTrack = CountTrackZones();
	if(ZonesInTrack)
	{
		ZoneBuffer = new TRACKZONE[ZonesInTrack];
		ZoneSequence = new ZONESEQENTRY[ZonesInTrack];
		PosNodeBuffer = new POSNODE[ZonesInTrack + 1];
		TRACKZONE* destzone = ZoneBuffer;
		U16 n = LocalTrack->Width * LocalTrack->Height;
		while(n--)
		{
			RevoltTrackModuleInstance* instance = &LocalTrack->Modules[n];
			INDEX moduleid = instance->ModuleID;
			if(moduleid != MAX_INDEX)
			{
				xpos = n % LocalTrack->Width;
				ypos = n / LocalTrack->Width;
				TRACKMODULE* module = LocalTheme->Modules[moduleid];
				MakeModuleTransformationMatrix(instance, xpos, ypos, &modulematrix);
				U16 i = module->ZoneCount;
				while(i--)
				{
					const TRACKZONE* srczone = module->Zones[i];
					destzone->Centre.X = srczone->Centre.X;
					destzone->Centre.Y = srczone->Centre.Y;
					destzone->Centre.Z = srczone->Centre.Z;
					destzone->XSize = srczone->XSize;
					destzone->YSize = srczone->YSize;
					destzone->ZSize = srczone->ZSize;
					destzone->Links[0].Position.X = srczone->Links[0].Position.X;
					destzone->Links[0].Position.Y = srczone->Links[0].Position.Y;
					destzone->Links[0].Position.Z = srczone->Links[0].Position.Z;
					destzone->Links[1].Position.X = srczone->Links[1].Position.X;
					destzone->Links[1].Position.Y = srczone->Links[1].Position.Y;
					destzone->Links[1].Position.Z = srczone->Links[1].Position.Z;
					if(moduleid == TWM_PIPE_2 || moduleid == TWM_PIPEC_2 || moduleid == TWM_PIPE_20_2)
					{
						destzone->IsPipe = true;
					}
					else
					{
						destzone->IsPipe = false;
					}
					TransformRevoltVertex(&destzone->Centre, modulematrix);
					TransformRevoltVertex(&destzone->Links[0].Position, modulematrix);
					TransformRevoltVertex(&destzone->Links[1].Position, modulematrix);
					if((instance->Direction == EAST) || (instance->Direction == WEST))
					{
						REAL temp = destzone->XSize;
						destzone->XSize = destzone->ZSize;
						destzone->ZSize = temp;
					}
					destzone++;
				}
			}
		}
	}
}

//**********************************************
// CuboidContainsVector
// 
// Simply returns true/false depending on whether
// the vector is inside the bounds of the sphere
// or not.
//
//**********************************************
bool SphereContainsVector(const BIG_CUBE_HEADER_LOAD* sphere, const REVOLTVECTOR* vect)
{
	assert(sphere != NULL);
	assert(vect != NULL);

	REAL xdist = fabs(sphere->x - vect->X);	//get the distance from the center
	REAL ydist = fabs(sphere->y - vect->Y);	//of the sphere to the vector
	REAL zdist = fabs(sphere->z - vect->Z);	//for each of the 3 axes

	//get the square of the radius
	REAL limit = (sphere->Radius * sphere->Radius);
	//now calculate the square of the distance to the vector
	REAL distance = ((xdist * xdist) + (ydist * ydist) + (zdist * zdist));
							
	//then decide if it inside the sphere
	return (distance < limit);
}

//**********************************************
// CuboidContainsVector
// 
// Simply returns true/false depending on whether
// the vector is inside the bounds of the cuboid
// or not.
//
//**********************************************
bool CuboidContainsVector(const CUBE_HEADER_LOAD* cuboid, const REVOLTVECTOR* vect)
{
	assert(cuboid != NULL);
	assert(vect != NULL);

	CUBE_HEADER_LOAD testcuboid;	//create a working cuboid 
	
	// and expand it to give us a bit of tolerance on the floating point comparison
	testcuboid.Xmin = cuboid->Xmin - SIMILAR_REAL;
	testcuboid.Ymin = cuboid->Ymin - SIMILAR_REAL;
	testcuboid.Zmin = cuboid->Zmin - SIMILAR_REAL;
	testcuboid.Xmax = cuboid->Xmax + SIMILAR_REAL;
	testcuboid.Ymax = cuboid->Ymax + SIMILAR_REAL;
	testcuboid.Zmax = cuboid->Zmax + SIMILAR_REAL;

	if((vect->X >= testcuboid.Xmin) && (vect->X <= testcuboid.Xmax))
	{
		if((vect->Y >= testcuboid.Ymin) && (vect->Y <= testcuboid.Ymax))
		{
			if((vect->Z >= testcuboid.Zmin) && (vect->Z <= testcuboid.Zmax))
			{
				return true;
			}
		}
	}
	return false;
}

//**********************************************
// CuboidContainsPoly
// 
// Determines whether a poly falls within the
// bounds of the specified cuboid
// A polygons is deemed to be in the cuboid
// if the average of all of its vertices falls
// inside
//
//**********************************************
bool CuboidContainsPoly(const CUBE_HEADER_LOAD* cuboid, const BASICPOLY* poly, const REVOLTMATRIX* matrix)
{
	assert(LocalTheme != NULL);
	assert(cuboid != NULL);
	assert(poly != NULL);
	assert(matrix != NULL);

	REVOLTVECTOR shiftedresult;
	REVOLTVECTOR *vect;
	
	REVOLTVECTOR result = {ZERO, ZERO, ZERO};

	//find the average of the polygon vertices 
	U16 v = VertsInPoly(poly);
	while(v--)
	{
		vect = &LocalTheme->Verts[poly->Vertices[v]];
		result.X += vect->X;
		result.Y += vect->Y;
		result.Z += vect->Z;
	}

	v = VertsInPoly(poly);

	result.X /= v;
	result.Y /= v;
	result.Z /= v;

	//transform it into world coordinates
	TRANSFORM_VECTOR( shiftedresult, result, (REVOLTMATRIX&)*matrix);

	//and check that vector against the cuboid
	return CuboidContainsVector(cuboid, &shiftedresult);
}

//**********************************************
// PolysInCuboid
// 
// Counts the number of polygons from the 
// specified polyset which fall into the
// specified cuboid.
// Also works out how many vertices are involved
// Adds the results to the PolyNum & VertNum
// members of the cuboid so these should be zeroed
// before calling this function for the first time 
// for any given cuboid, subsequent calls will
// accumulate the results
//
//**********************************************
void CountPolysAndVertsInCuboid(const POLYSET* polyset, CUBE_HEADER_LOAD* cuboid, const REVOLTMATRIX* matrix, short* polycount, short* vertcount)
{
	assert(LocalTheme != NULL);
	assert(polyset != NULL);
	assert(cuboid != NULL);
	assert(matrix != NULL);
	assert(polycount != NULL);
	assert(vertcount != NULL);

	*polycount = 0;
	*vertcount = 0;

	U32 p = polyset->PolygonCount;

	while(p--)
	{
		BASICPOLY* poly = &LocalTheme->Polys[polyset->Indices[p] & ~GOURAUD_SHIFTED];
		if(CuboidContainsPoly(cuboid, poly, matrix))
		{
			(*polycount)++;
			(*vertcount) += VertsInPoly(poly);
		}
	}
}

//**********************************************
//
//**********************************************
void CalculateNormal(REVOLTVECTOR* normal, const WORLD_VERTEX_LOAD* p0, const WORLD_VERTEX_LOAD* p1, const WORLD_VERTEX_LOAD* p2)
{
	assert(p0 != NULL);
	assert(p1 != NULL);
	assert(p2 != NULL);
	assert(normal  != NULL);

	REVOLTVECTOR	vect0;
	REVOLTVECTOR	vect1;

	vect0.X = p0->x - p1->x;
	vect0.Y = p0->y - p1->y;
	vect0.Z = p0->z - p1->z;

	vect1.X = p2->x - p1->x;
	vect1.Y = p2->y - p1->y;
	vect1.Z = p2->z - p1->z;

	CROSS_PRODUCT(normal, &vect0, &vect1);
	NORMALIZE_VECTOR(normal);
}

//**********************************************
//
//**********************************************
void CalculateD3DNormal(REVOLTVECTOR* normal, const REVOLTVECTOR* p0, const REVOLTVECTOR* p1, const REVOLTVECTOR* p2)
{
	assert(p0 != NULL);
	assert(p1 != NULL);
	assert(p2 != NULL);
	assert(normal  != NULL);

	REVOLTVECTOR	vect0;
	REVOLTVECTOR	vect1;

	vect0.X = p0->X - p1->X;
	vect0.Y = p0->Y - p1->Y;
	vect0.Z = p0->Z - p1->Z;

	vect1.X = p2->X - p1->X;
	vect1.Y = p2->Y - p1->Y;
	vect1.Z = p2->Z - p1->Z;

	CROSS_PRODUCT(normal, &vect0, &vect1);
	NORMALIZE_VECTOR(normal);
}

//**********************************************
//
//**********************************************
void FindCuboidCentreAndRadius(CUBE_HEADER_LOAD* cuboid)
{
	assert(cuboid != NULL);

	cuboid->CentreX = (cuboid->Xmin + cuboid->Xmax) / Real(2.0f);
	cuboid->CentreY = (cuboid->Ymin + cuboid->Ymax) / Real(2.0f);
	cuboid->CentreZ = (cuboid->Zmin + cuboid->Zmax) / Real(2.0f);
	REAL xsize = cuboid->CentreX - cuboid->Xmin;
	REAL ysize = cuboid->CentreY - cuboid->Ymin;
	REAL zsize = cuboid->CentreZ - cuboid->Zmin;

	cuboid->Radius = sqrt((xsize * xsize) + (ysize * ysize) + (zsize * zsize));

	cuboid->Xmin = cuboid->CentreX - xsize;
	cuboid->Ymin = cuboid->CentreY - ysize;
	cuboid->Zmin = cuboid->CentreZ - zsize;
	cuboid->Xmax = cuboid->CentreX + xsize;
	cuboid->Ymax = cuboid->CentreY + ysize;
	cuboid->Zmax = cuboid->CentreZ + zsize;
}

//**********************************************
//
//**********************************************
bool AreCubesAdjacent(const SMALLCUBE* cube1, const SMALLCUBE* cube2)
{
	assert(cube1 != NULL);
	assert(cube2 != NULL);

	bool adjacent = false;
	REAL xdist = fabs(cube1->Header.CentreX - cube2->Header.CentreX);
	REAL zdist = fabs(cube1->Header.CentreZ - cube2->Header.CentreZ);
	xdist *= xdist;
	zdist *= zdist;
	REAL dist = xdist + zdist;
	if(dist > 1)	//if the distance is not small (i.e we are not checking against self or cube with similar Y coord)
	{
		adjacent = (ApproxEqual(dist, SMALL_CUBE_SIZE * SMALL_CUBE_SIZE) == TRUE);	//decide if the cube is adjacent or not
	}

	return adjacent;
}

//**********************************************
//
//**********************************************
void CopyPoly(WORLD_POLY_LOAD* dest, const WORLD_POLY_LOAD* src)
{
	assert(dest != NULL);
	assert(src != NULL);

	dest->Type = src->Type;
	dest->Tpage = src->Tpage;
	dest->vi0 = src->vi0;
	dest->vi1 = src->vi1;
	dest->vi2 = src->vi2;
	dest->vi3 = src->vi3;
	dest->c0 = src->c0;
	dest->c1 = src->c1;
	dest->c2 = src->c2;
	dest->c3 = src->c3;
	dest->u0 = src->u0;
	dest->v0 = src->v0;
	dest->u1 = src->u1;
	dest->v1 = src->v1;
	dest->u2 = src->u2;
	dest->v2 = src->v2;
	dest->u3 = src->u3;
	dest->v3 = src->v3;

}
//**********************************************
// RemoveCubePoly
//
// 'Removes' a polygon from a cube by copying the
// last polygon in the array over it and then
// decrementing the polygon count.
// This method is quick as it doesn't need to
// re-allocate memory or copy large numbers of
// array elements .
// NOTE - if the element to be deleted IS the
// last in the array then there is no need to
// do the copy
//
//**********************************************
void RemoveCubePoly(SMALLCUBE* cube, INDEX index)
{
	assert(cube != NULL);				  //assure that the cube pointer does point somewhere
	assert(index < cube->Header.PolyNum); //only allow indices which are within the size of the array

	if(index < (cube->Header.PolyNum - 1))	//if the poly to be deleted is not the last in the array
	{
		WORLD_POLY_LOAD* srcpoly = &cube->Polys[cube->Header.PolyNum - 1];	//get the address of the last in the array
		WORLD_POLY_LOAD* destpoly = &cube->Polys[index];					//and the address of the poly to be 'removed'
		CopyPoly(destpoly, srcpoly);										//then copy the last poly over the top of the 'removed' one
	}
	cube->Header.PolyNum--; //decrement the polygon count for the cube
}

//**********************************************
//
//**********************************************
bool NormalsOpposite(const WORLD_VERTEX_LOAD* vert1, const WORLD_VERTEX_LOAD* vert2)
{
	if(ApproxWithin(vert1->nx, -vert2->nx, NORMAL_TOLERANCE) == FALSE)
	{
		return false;
	}
	if(ApproxWithin(vert1->ny, -vert2->ny, NORMAL_TOLERANCE) == FALSE)
	{
		return false;
	}
	return (ApproxWithin(vert1->nz, -vert2->nz, NORMAL_TOLERANCE) == TRUE);
}

//**********************************************
//
//**********************************************
bool NormalsApprox(const WORLD_VERTEX_LOAD* vert1, const WORLD_VERTEX_LOAD* vert2)
{
	if(ApproxWithin(vert1->nx, vert2->nx, NORMAL_TOLERANCE) == FALSE)
	{
		return false;
	}
	if(ApproxWithin(vert1->ny, vert2->ny, NORMAL_TOLERANCE) == FALSE)
	{
		return false;
	}
	return (ApproxWithin(vert1->nz, vert2->nz, NORMAL_TOLERANCE) == TRUE);
}

//**********************************************
//
//**********************************************
bool VertsApprox(const WORLD_VERTEX_LOAD* vert1, const WORLD_VERTEX_LOAD* vert2)
{
	if(ApproxWithin(vert1->x, vert2->x, AXIS_TOLERANCE) == FALSE)
	{
		return false;
	}
	if(ApproxWithin(vert1->y, vert2->y, AXIS_TOLERANCE) == FALSE)
	{
		return false;
	}
	return (ApproxWithin(vert1->z, vert2->z, AXIS_TOLERANCE) == TRUE);
}

//**********************************************
//
//**********************************************
bool PlanesApproxOpposite(const PLANE* plane1, const PLANE* plane2)
{
	if(ApproxWithin(plane1->v[0], -plane2->v[0], PLANE_TOLERANCE) == FALSE)
	{
		return false;
	}
	if(ApproxWithin(plane1->v[1], -plane2->v[1], PLANE_TOLERANCE) == FALSE)
	{
		return false;
	}
	if(ApproxWithin(plane1->v[2], -plane2->v[2], PLANE_TOLERANCE) == FALSE)
	{
		return false;
	}
	return (ApproxWithin(plane1->v[3], -plane2->v[3], PLANE_TOLERANCE) == TRUE);
}

//**********************************************
//
//**********************************************
bool PlanesApprox(const PLANE* plane1, const PLANE* plane2)
{
	if(ApproxWithin(plane1->v[0], plane2->v[0], PLANE_TOLERANCE) == FALSE)
	{
		return false;
	}
	if(ApproxWithin(plane1->v[1], plane2->v[1], PLANE_TOLERANCE) == FALSE)
	{
		return false;
	}
	if(ApproxWithin(plane1->v[2], plane2->v[2], PLANE_TOLERANCE) == FALSE)
	{
		return false;
	}
	return (ApproxWithin(plane1->v[3], plane2->v[3], PLANE_TOLERANCE) == TRUE);
}

//**********************************************
//
//**********************************************
bool PolysAreFaceToFace(WORLD_POLY_LOAD* testpoly, WORLD_POLY_LOAD* currentpoly, SMALLCUBE* testcube, SMALLCUBE* currentcube)
{
	bool success = false;

	WORLD_VERTEX_LOAD* currentvert = &currentcube->Verts[currentpoly->vi0];
	if((currentpoly->Type & POLY_QUAD) == (testpoly->Type & POLY_QUAD))
	{
		WORLD_VERTEX_LOAD* testvert = &testcube->Verts[testpoly->vi0];
		if(NormalsOpposite(testvert, currentvert))
		{
			short* currentindices = &currentpoly->vi0;
			short* testindices = &testpoly->vi0;
			U16 vertcount = 3;
			if(currentpoly->Type & POLY_QUAD)
			{
				vertcount++;
			}
			S16 tv = vertcount;
			while(tv--)
			{
				if(VertsApprox(&testcube->Verts[testindices[tv]], &currentcube->Verts[currentindices[0]]))
				{
					S16 cv = 0;
					bool match = true;
					do{
						cv++;
						tv += vertcount-1;
						tv %= vertcount;
						if(VertsApprox(&testcube->Verts[testindices[tv]], &currentcube->Verts[currentindices[cv]]) == false)
						{	
							match = false;
						}
					}while((cv < (vertcount - 1)) && (match == true));
					success = match;
					break;
				}
			}
		}
	}
	return success;
}

void RemoveMatchingPolys(SMALLCUBE* testcube, SMALLCUBE* currentcube)
{
	INDEX currentindex = currentcube->Header.PolyNum;
	while(currentindex--)
	{
		WORLD_POLY_LOAD* currentpoly = &currentcube->Polys[currentindex];
		INDEX testindex = testcube->Header.PolyNum;
		while(testindex--)
		{
			WORLD_POLY_LOAD* testpoly = &testcube->Polys[testindex];
			if(PolysAreFaceToFace(testpoly, currentpoly, testcube, currentcube))
			{
				RemoveCubePoly(testcube, testindex);
				RemoveCubePoly(currentcube, currentindex);
				break;
			}
		}
	}
}

//**********************************************
// searches destcube for a vertex which matches
// the one specified by srccube and srcindex
// if the vertex doesn't exist, its is created.
// either way, its index is returned
//**********************************************
short GetVertIndex(SMALLCUBE* destcube, const SMALLCUBE* srccube, short srcindex)
{
	WORLD_VERTEX_LOAD* srcvert = &srccube->Verts[srcindex];
	WORLD_VERTEX_LOAD* destvert = &destcube->Verts[0];
	short index = 0;
	while(index < destcube->Header.VertNum)
	{
		if(VertsApprox(destvert, srcvert))
		{
			if(NormalsApprox(destvert, srcvert))
			{
				return index;
			}
		}
		destvert++;
		index++;
	}

	// If we got this far, the vertex isn't in the cube so we'll have to add it
	WORLD_VERTEX_LOAD* oldverts = destcube->Verts;
	destcube->Verts = new WORLD_VERTEX_LOAD[index + 1];
	memcpy(destcube->Verts, oldverts, sizeof(WORLD_VERTEX_LOAD) * index);
	destcube->Verts[index] = *srcvert;
	destcube->Header.VertNum++;
	delete[] oldverts;
	return index;
}

//**********************************************
//
//**********************************************
bool PolysAreWeldable(WELD_DATA* testweld, WELD_DATA* currentweld, SMALLCUBE* testcube, SMALLCUBE* currentcube, REAL yconstraint)
{
	WORLD_VERTEX_LOAD* currentvert = &currentcube->Verts[currentweld->poly->vi0];
	WORLD_VERTEX_LOAD* testvert = &testcube->Verts[testweld->poly->vi0];
	if(NormalsApprox(testvert, currentvert))
	{
		short* currentindices = &currentweld->poly->vi0;
		short* testindices = &testweld->poly->vi0;
		const U16 vertcount = 4;
		S16 cv = 0;
		while(cv < vertcount)
		{
			if(ApproxEqual(fabs(currentweld->edges[cv].ny), yconstraint))
			{
				S16 tv = vertcount;
				while(tv--)
				{
					if(VertsApprox(&testcube->Verts[testindices[tv]], &currentcube->Verts[currentindices[cv]]))
					{
						if(VertsApprox(&testcube->Verts[testindices[(tv + 3) % vertcount]], &currentcube->Verts[currentindices[(cv + 1) % vertcount]]))
						{
							if(NormalsApprox(&testweld->edges[tv], &currentweld->edges[(cv + 3) % vertcount]))
							{
								if(NormalsApprox(&testweld->edges[(tv + 2) % vertcount], &currentweld->edges[(cv + 1) % vertcount]))
								{
									currentindices[cv] = GetVertIndex(currentcube, testcube, testindices[(tv + 1) % vertcount]);
									currentindices[(cv + 1) % vertcount] = GetVertIndex(currentcube, testcube, testindices[(tv + 2) % vertcount]);
									long* currentcolor = &currentweld->poly->c0;
									long* testcolor = &testweld->poly->c0;
									currentcolor[cv] = testcolor[(tv + 1) % vertcount];
									currentcolor[(cv + 1) % vertcount] = testcolor[(tv + 2) % vertcount];
									return true;
								}
							}
						}
					}
				}
			}
			cv++;
		}
	}
	return false;
}

bool PolySuitsWelding(const WORLD_POLY_LOAD* poly, const SMALLCUBE* cube)
{
	bool suitswelding = false;

	if(poly->Type & QUAD)
	{
		if(poly->Tpage == TPAGE_NONE)
		{
			WORLD_VERTEX_LOAD* vert = &cube->Verts[poly->vi0];
			if(ApproxWithin(vert->ny, Real(ZERO), NORMAL_TOLERANCE) == TRUE)
			{
				suitswelding = true;
			}
		}
	}
	return suitswelding;
}

void MakeWeldData(WELD_DATA* weld, WORLD_POLY_LOAD* poly, SMALLCUBE* cube)
{
	REAL length;
	short* indices = &poly->vi0;
	for(int v = 0; v < 4; v++)
	{
		weld->edges[v].nx = cube->Verts[indices[v]].x - cube->Verts[indices[(v + 1) % 4]].x;
		weld->edges[v].ny = cube->Verts[indices[v]].y - cube->Verts[indices[(v + 1) % 4]].y;
		weld->edges[v].nz = cube->Verts[indices[v]].z - cube->Verts[indices[(v + 1) % 4]].z;
		length = (weld->edges[v].nx * weld->edges[v].nx);
		length += (weld->edges[v].ny * weld->edges[v].ny);
		length += (weld->edges[v].nz * weld->edges[v].nz);
		length = sqrt(length);
		weld->edges[v].nx /= length;
		weld->edges[v].ny /= length;
		weld->edges[v].nz /= length;
	}
	weld->poly = poly;
}

bool WeldPegPolys(SMALLCUBE* testcube, SMALLCUBE* currentcube, REAL yconstraint)
{
	WELD_DATA currentweld;
	WELD_DATA testweld;
	INDEX currentindex = currentcube->Header.PolyNum;
	while(currentindex--)
	{
		WORLD_POLY_LOAD* currentpoly = &currentcube->Polys[currentindex];
		if(PolySuitsWelding(currentpoly, currentcube))
		{
			MakeWeldData(&currentweld, currentpoly, currentcube);
			INDEX testindex = testcube->Header.PolyNum;
			if(currentcube == testcube)
			{
				testindex = currentindex;
			}
			while(testindex--)
			{
				WORLD_POLY_LOAD* testpoly = &testcube->Polys[testindex];
				if(PolySuitsWelding(testpoly, testcube))
				{
					MakeWeldData(&testweld, testpoly, testcube);
					if(PolysAreWeldable(&testweld, &currentweld, testcube, currentcube, yconstraint))
					{
						RemoveCubePoly(testcube, testindex);
						return true;
					}
				}
			}
		}
	}
	return false;
}

//**********************************************
//
//**********************************************
bool PolyContainsPlane(const NEWCOLLPOLY* poly, const PLANE* plane)
{
	U16 n = 3;
	if(poly->Type & QUAD)
	{
		n++;
	}
	while(n--)
	{
		if(PlanesApprox(plane, &poly->EdgePlane[n]))
		{
			return true;
		}
	}
	return false;
}

//**********************************************
//
//**********************************************
bool CollPolysMatch(const NEWCOLLPOLY* poly1, const NEWCOLLPOLY* poly2)
{
	bool match = false;
	if((poly1->Type & QUAD) == (poly2->Type & QUAD))
	{
		if(PlanesApproxOpposite(&poly1->Plane, &poly2->Plane))
		{
			U16 n = 3;
			if(poly1->Type & QUAD)
			{
				n++;
			}
			match = true;
			while(n-- && (match == true))
			{
				match = PolyContainsPlane(poly2, &poly1->EdgePlane[n]);
			}
		}
	}
	return match;
}

//**********************************************
//
//**********************************************
void MergeBBoxes(BBOX* dest, const BBOX* src)
{
	dest->XMin = min(dest->XMin, src->XMin);
	dest->YMin = min(dest->YMin, src->YMin);
	dest->ZMin = min(dest->ZMin, src->ZMin);
	dest->XMax = max(dest->XMax, src->XMax);
	dest->YMax = max(dest->YMax, src->YMax);
	dest->ZMax = max(dest->ZMax, src->ZMax);
}

//**********************************************
//
//**********************************************
bool CollPolysMerge(NEWCOLLPOLY* poly1, const NEWCOLLPOLY* poly2)
{
	bool match = false;
	if((poly1->Type & QUAD) && (poly2->Type & QUAD))
	{
		if(PlanesApprox(&poly1->Plane, &poly2->Plane))
		{
			U16 n1 = 4;
			while(n1-- && (match == false))
			{
				U16 n2 = 4;
				while(n2-- && (match == false))
				{
					if(PlanesApproxOpposite(&poly1->EdgePlane[n1], &poly2->EdgePlane[n2]))
					{
						if(PlanesApprox(&poly1->EdgePlane[(n1 + 1) % 4], &poly2->EdgePlane[(n2 + 3) % 4]))
						{
							if(PlanesApprox(&poly1->EdgePlane[(n1 + 3) % 4], &poly2->EdgePlane[(n2 + 1) % 4]))
							{
								match = true;
								poly1->EdgePlane[n1].v[0] = poly2->EdgePlane[(n2 + 2) % 4].v[0];
								poly1->EdgePlane[n1].v[1] = poly2->EdgePlane[(n2 + 2) % 4].v[1];
								poly1->EdgePlane[n1].v[2] = poly2->EdgePlane[(n2 + 2) % 4].v[2];
								poly1->EdgePlane[n1].v[3] = poly2->EdgePlane[(n2 + 2) % 4].v[3];
								//dont forget to update the bounding box to reflect the new size of the poly
								MergeBBoxes(&poly1->BBox, &poly2->BBox);
							}
						}
					}
				}
			}
		}
	}
	return match;
}

//**********************************************
//
//**********************************************
void CopyCollPoly(NEWCOLLPOLY* dest, const NEWCOLLPOLY* src)
{
	dest->BBox.XMax = src->BBox.XMax;
	dest->BBox.YMax = src->BBox.YMax;
	dest->BBox.ZMax = src->BBox.ZMax;
	dest->BBox.XMin = src->BBox.XMin;
	dest->BBox.YMin = src->BBox.YMin;
	dest->BBox.ZMin = src->BBox.ZMin;
	dest->EdgePlane[0] = src->EdgePlane[0];
	dest->EdgePlane[1] = src->EdgePlane[1];
	dest->EdgePlane[2] = src->EdgePlane[2];
	dest->EdgePlane[3] = src->EdgePlane[3];
	dest->Material = src->Material;
	dest->Plane = src->Plane;
	dest->Type = src->Type;
}

//**********************************************
//
//**********************************************
bool WeldAllPegPolys(void)
{
	bool weldhappened = false;
	bool weldedwithself;

	U16 currentcubeindex = SmallCubeCount;
	while(currentcubeindex--)
	{
		SMALLCUBE* currentcube = &SmallCubes[currentcubeindex];

		if(currentcube->Header.PolyNum != 0)
		{
			//first of all we need to weld polys which share a vertical edge
			do{
				weldedwithself = (WeldPegPolys(currentcube, currentcube, ONE) == true);
				if(weldedwithself)
				{
					weldhappened = true;
				}
			}while(weldedwithself == true);
			//now weld polys which share a horizontal edge
			do{
				weldedwithself = (WeldPegPolys(currentcube, currentcube, ZERO) == true);
				if(weldedwithself)
				{
					weldhappened = true;
				}
			}while(weldedwithself == true);

			//attempt to weld with all cubes before it in the list
			//(but only those which are adjacent
			U16 testcubeindex = currentcubeindex;
			while(testcubeindex--)
			{
				SMALLCUBE* testcube = &SmallCubes[testcubeindex];
				if(testcube->Header.PolyNum != 0)
				{
					if(ApproxEqual(currentcube->Header.CentreX, testcube->Header.CentreX) == TRUE)
					{
						if(ApproxEqual(currentcube->Header.CentreZ, testcube->Header.CentreZ) == TRUE)
						{
							if(WeldPegPolys(testcube, currentcube, ZERO) == true)
							{
								weldhappened = true;
							}
						}
					}
				}
			}
		}
	}
	return weldhappened;
}

//**********************************************
//
//**********************************************
void RemoveSurplusWorldPolys(void)
{
///	while(WeldAllPegPolys() == true)
///	{
///	}
	U16 currentcubeindex = SmallCubeCount;
	while(currentcubeindex--)
	{
		SMALLCUBE* currentcube = &SmallCubes[currentcubeindex];

		U16 testcubeindex = currentcubeindex;
		while(testcubeindex--)
		{
			SMALLCUBE* testcube = &SmallCubes[testcubeindex];
			bool adjacent = AreCubesAdjacent(testcube, currentcube);
			if(adjacent == true)
			{
				RemoveMatchingPolys(testcube, currentcube);
			}
		}
	}
}

//**********************************************
//
//**********************************************
void RemoveSurplusCollisionPolys(void)
{
	//flag all of the coliision polys as being used
	for(S16 cp = 0; cp < (CollisionData.NumPolys); cp++)
	{
		CollisionData.PolyStatus[cp] = 1;
	}
	
	// remove all collision polys which occupy the same space
	// but are pointing in oppsite directions
	NEWCOLLPOLY* currentpoly = CollisionData.Polys;
	for(cp = 0; cp < (CollisionData.NumPolys - 1); cp++)
	{
		//if the poly is being used
		if(CollisionData.PolyStatus[cp] == 1)
		{
			//check each of the polys after it in the list
			NEWCOLLPOLY* testpoly = currentpoly;
			for(S16 tp = cp + 1; tp < CollisionData.NumPolys; tp++)
			{
				testpoly++;
				//if that poly is also being used
				if(CollisionData.PolyStatus[tp] == 1)
				{
					//check to see if the pair of polys can be flagged as unused
					if(CollPolysMatch(currentpoly, testpoly) == true)
					{
						CollisionData.PolyStatus[cp] = 0;
						CollisionData.PolyStatus[tp] = 0;
						break;
					}
				}
			}
		}
		currentpoly++;
	}

#if 1
	// merge any polys which are adjacent to each other and 
	// form a quad when merged
	bool merged;
	do
	{
		merged = false;
		currentpoly = CollisionData.Polys;
		for(cp = 0; cp < (CollisionData.NumPolys - 1); cp++)
		{
			//if the poly is being used
			if(CollisionData.PolyStatus[cp] == 1)
			{
				//check each of the polys after it in the list
				NEWCOLLPOLY* testpoly = currentpoly;
				for(S16 tp = cp + 1; tp < CollisionData.NumPolys; tp++)
				{
					testpoly++;
					//if that poly is also being used
					if(CollisionData.PolyStatus[tp] == 1)
					{
						//check to see if the pair of polys can be merged
						if(CollPolysMerge(currentpoly, testpoly) == true)
						{
							CollisionData.PolyStatus[tp] = 0;
							merged = true;
						}
					}
				}
			}
			currentpoly++;
		}
	}while(merged == true);
#endif

	//remove the unused polys from the list
	S32 lastentry = CollisionData.NumPolys - 1;
	while(CollisionData.PolyStatus[lastentry] == 0)
	{
		lastentry--;
	}
	cp = lastentry;
	while(cp--)
	{
		if(CollisionData.PolyStatus[cp] == 0)
		{
			CopyCollPoly(&CollisionData.Polys[cp], &CollisionData.Polys[lastentry]);
			lastentry--;
		}
	}
	CollisionData.NumUsedPolys = lastentry + 1;
}

//**********************************************
//
//**********************************************
void RemoveSurplusPolys(void)
{
	RemoveSurplusWorldPolys();
	RemoveSurplusCollisionPolys();
	FillCollisionCells();
}

//**********************************************
//
//**********************************************
void FillSmallCube(SMALLCUBE* cube, const TRACKUNIT* unit, const REVOLTMATRIX* matrix, U16 Elevation)
{
	assert(LocalTheme != NULL);
	assert(cube != NULL);
	assert(unit != NULL);
	assert(matrix != NULL);

	MESH* mesh = &LocalTheme->Meshes[unit->MeshID];

	CUBE_HEADER_LOAD adjustedcuboid;				//create a cube for growing into the final size
	CopyCuboid(&adjustedcuboid, &cube->Header);		//initialize it to the size of the actual cube
		
	U16 vertindex = cube->NextVert;
	U16 polyindex = cube->NextPoly;

	WORLD_POLY_LOAD*	currentpoly = &cube->Polys[polyindex];
	WORLD_VERTEX_LOAD*	currentvert = &cube->Verts[vertindex];
	
	REVOLTVECTOR normal;

	for(U16 ps = 0; ps <= PAN_INDEX; ps++)
	{
		POLYSET* polyset = &LocalTheme->PolySets[mesh->PolySetIndices[ps]];
		U32 p = polyset->PolygonCount;
		while(p--)
		{
			BASICPOLY* poly = &LocalTheme->Polys[polyset->Indices[p] & ~GOURAUD_SHIFTED];

			if(CuboidContainsPoly(&cube->Header, poly, matrix))
			{
				bool gouraudshifted = ((polyset->Indices[p] & GOURAUD_SHIFTED) != 0);
	
				currentpoly->Type = poly->IsTriangle ? 0 : POLY_QUAD;

				currentpoly->vi0 = vertindex + 2;
				currentpoly->vi1 = vertindex + 1;
				currentpoly->vi2 = vertindex;
				currentpoly->vi3 = vertindex + 3;

				U16 vertcount = VertsInPoly(poly);
				for(U16 v = 0; v < vertcount; v++)
				{
					REVOLTVECTOR vect;
					TRANSFORM_VECTOR( vect, LocalTheme->Verts[poly->Vertices[v]], (REVOLTMATRIX&)*matrix);

					currentvert->x = vect.X;
					currentvert->y = vect.Y;
					currentvert->z = vect.Z;
					currentvert++;
				}
				currentvert -= vertcount;	//wind back to first vertex

				CalculateNormal(&normal, &currentvert[0], &currentvert[1], &currentvert[2]);
				for(v = 0; v < vertcount; v++)
				{
					currentvert->nx = normal.X;
					currentvert->ny = normal.Y;
					currentvert->nz = normal.Z;
					currentvert++;
				}
				currentvert -= vertcount;	//wind back to first vertex

				if(ps == PAN_INDEX)
				{
					currentpoly->Tpage = (unit->UVPolys[p].TPageID & ~UV_REVERSED) - 1;
					BASICPOLY* uvpoly = &LocalTheme->UVPolys[unit->UVPolys[p].PolyID];
					U32 uvmodvalue = VertsInPoly(uvpoly);
					U32 uvindex = unit->UVPolys[p].Rotation;
					U32 uvstep = (unit->UVPolys[p].TPageID & UV_REVERSED) ? (uvmodvalue - 1) : 1;

					currentpoly->u2  = LocalTheme->UVCoords[uvpoly->Vertices[uvindex]].U;
					currentpoly->v2  = LocalTheme->UVCoords[uvpoly->Vertices[uvindex]].V;
					uvindex += uvstep;
					uvindex %= uvmodvalue;

					currentpoly->u1  = LocalTheme->UVCoords[uvpoly->Vertices[uvindex]].U;
					currentpoly->v1  = LocalTheme->UVCoords[uvpoly->Vertices[uvindex]].V;
					uvindex += uvstep;
					uvindex %= uvmodvalue;

					currentpoly->u0  = LocalTheme->UVCoords[uvpoly->Vertices[uvindex]].U;
					currentpoly->v0  = LocalTheme->UVCoords[uvpoly->Vertices[uvindex]].V;
					uvindex += uvstep;
					uvindex %= uvmodvalue;

					currentpoly->u3  = LocalTheme->UVCoords[uvpoly->Vertices[uvindex]].U;
					currentpoly->v3  = LocalTheme->UVCoords[uvpoly->Vertices[uvindex]].V;
					uvindex += uvstep;
					uvindex %= uvmodvalue;

					BASICPOLY* rgbpoly = &LocalTheme->RGBPolys[unit->RGBs[p]];
					currentpoly->c0 = 0xff000000 | rgbpoly->Vertices[2];
					currentpoly->c1 = 0xff000000 | rgbpoly->Vertices[1];
					currentpoly->c2 = 0xff000000 | rgbpoly->Vertices[0];
					currentpoly->c3 = 0xff000000 | rgbpoly->Vertices[3];
				}
				else
				{
					currentpoly->Tpage = TPAGE_NONE;
					currentpoly->u0	= currentpoly->u1 = currentpoly->u2	= currentpoly->u3 = ZERO;
					currentpoly->v0	= currentpoly->v1 = currentpoly->v2	= currentpoly->v3 = ZERO;
					const REAL maximumy = MAX_ELEVATION * ElevationStep;
					long red, green, blue;
					long color;
					red	  = (long)(((fabs(currentvert[2].y) / maximumy) * 90) + 10);
					green = (long)(((fabs(currentvert[2].y) / maximumy) * 180) + 40);
					blue  = (long)(((fabs(currentvert[2].y) / maximumy) * 180) + 50);
					color = 0xff000000 | blue | (green << 8) | (red << 16);
					currentpoly->c0 = color;
					red	  = (long)(((fabs(currentvert[1].y) / maximumy) * 90) + 10);
					green = (long)(((fabs(currentvert[1].y) / maximumy) * 180) + 40);
					blue  = (long)(((fabs(currentvert[1].y) / maximumy) * 180) + 50);
					color = 0xff000000 | blue | (green << 8) | (red << 16);
					currentpoly->c1 = color;
					red	  = (long)(((fabs(currentvert[0].y) / maximumy) * 90) + 10);
					green = (long)(((fabs(currentvert[0].y) / maximumy) * 180) + 40);
					blue  = (long)(((fabs(currentvert[0].y) / maximumy) * 180) + 50);
					color = 0xff000000 | blue | (green << 8) | (red << 16);
					currentpoly->c2 = color;
					red	  = (long)(((fabs(currentvert[3].y) / maximumy) * 90) + 10);
					green = (long)(((fabs(currentvert[3].y) / maximumy) * 180) + 40);
					blue  = (long)(((fabs(currentvert[3].y) / maximumy) * 180) + 50);
					color = 0xff000000 | blue | (green << 8) | (red << 16);
					currentpoly->c3 = color;
				}
	
				if((gouraudshifted == false) && (vertcount == 4))
				{
					short verttemp = currentpoly->vi0;
					currentpoly->vi0 = currentpoly->vi1;
					currentpoly->vi1 = currentpoly->vi2;
					currentpoly->vi2 = currentpoly->vi3;
					currentpoly->vi3 = verttemp;

					long colortemp = currentpoly->c0;
					currentpoly->c0 = currentpoly->c1;
					currentpoly->c1 = currentpoly->c2;
					currentpoly->c2 = currentpoly->c3;
					currentpoly->c3 = colortemp;

					float uvtemp = currentpoly->u0;
					currentpoly->u0 = currentpoly->u1;
					currentpoly->u1 = currentpoly->u2;
					currentpoly->u2 = currentpoly->u3;
					currentpoly->u3 = uvtemp;

					uvtemp = currentpoly->v0;
					currentpoly->v0 = currentpoly->v1;
					currentpoly->v1 = currentpoly->v2;
					currentpoly->v2 = currentpoly->v3;
					currentpoly->v3 = uvtemp;
				}

				currentvert += vertcount;	//wind back to first vertex
				vertindex += vertcount;

				currentpoly++;
				polyindex++;
			}
		}
	}
	if(Elevation != 0)
	{
		for(U16 n = 0; n < 16; n += 8)
		{
			U16 edgemask = 0x0008;
			for(U16 edge = 0; edge < 4; edge++)
			{
				if(RootEdges & edgemask)
				{
					for(U16 v = 0; v < 2; v++)
					{
						REVOLTVECTOR vect;

						TRANSFORM_VECTOR( vect, UnitRootVects[n + (edge * 2) + v], (REVOLTMATRIX&)*matrix);

						currentvert->x = vect.X;
						currentvert->y = vect.Y;
						currentvert->z = vect.Z;
						currentvert++;
					}
				}
				edgemask >>= 1;
			}
		}

		currentvert -= (EdgeCount[RootEdges] * 4);	//wind back to first vertex

		for(U16 p = 0; p < EdgeCount[RootEdges]; p++)
		{
			CalculateNormal(&normal, &currentvert[(EdgeCount[RootEdges] * 2)], &currentvert[1], &currentvert[0]);
			for(U16 v = 0; v < 2; v++)
			{
				currentvert[0].nx = normal.X;
				currentvert[0].ny = normal.Y;
				currentvert[0].nz = normal.Z;
				currentvert[(EdgeCount[RootEdges] * 2)].nx = normal.X;
				currentvert[(EdgeCount[RootEdges] * 2)].ny = normal.Y;
				currentvert[(EdgeCount[RootEdges] * 2)].nz = normal.Z;
				currentvert++;
			}
		}

		for(n = 0; n < EdgeCount[RootEdges]; n++)
		{
			currentpoly->Type = POLY_QUAD;
			currentpoly->vi0 = vertindex + (EdgeCount[RootEdges] * 2) + 1;
			currentpoly->vi1 = vertindex + (EdgeCount[RootEdges] * 2);
			currentpoly->vi2 = vertindex + 0;
			currentpoly->vi3 = vertindex + 1;
			currentpoly->Tpage = TPAGE_NONE;
			currentpoly->u0	= currentpoly->u1 = currentpoly->u2	= currentpoly->u3 = ZERO;
			currentpoly->v0	= currentpoly->v1 = currentpoly->v2	= currentpoly->v3 = ZERO;

			const REAL maximumy = MAX_ELEVATION * ElevationStep;
			long red, green, blue;
			long color;
			red	  = (long)(((fabs(currentvert->y) / maximumy) * 90) + 10);
			green = (long)(((fabs(currentvert->y) / maximumy) * 180) + 40);
			blue  = (long)(((fabs(currentvert->y) / maximumy) * 180) + 50);
			color = 0xff000000 | blue | (green << 8) | (red << 16);
			currentpoly->c0 = color;
			currentpoly->c1 = color;
			color = 0xff000000 | 50 | (40 << 8) | (10 << 16);
///			color = 0xffff0000;
			currentpoly->c2 = color;
			currentpoly->c3 = color;

			currentpoly++;
			polyindex++;

			currentvert +=2;
			vertindex += 2;
		}
		vertindex += (EdgeCount[RootEdges] * 2);	//step past rest of peg ring vertices
	}
	cube->NextVert = vertindex;
	cube->NextPoly = polyindex;
	assert(cube->NextPoly <= cube->Header.PolyNum);
	assert(cube->NextVert <= cube->Header.VertNum);
	CopyCuboid(&cube->Header, &adjustedcuboid);
	FindCuboidCentreAndRadius(&cube->Header);
}

//**********************************************
//
//**********************************************

void CalculateUnitGeometryStorage(const CUBE_HEADER_LOAD* srccuboid, const TRACKUNIT* unit, REVOLTMATRIX* matrix, U16 Elevation)
{
	MESH* mesh = &LocalTheme->Meshes[unit->MeshID];

	CUBE_HEADER_LOAD cuboid, cube;
	CopyCuboid(&cuboid, srccuboid);			//take a copy of the units cuboid
	ExpandCuboid(&cuboid, SMALL_CUBE_SIZE);		

	cube.Zmin = cuboid.Zmin;
	while(cube.Zmin < cuboid.Zmax)
	{
		cube.Zmax = cube.Zmin + SMALL_CUBE_SIZE;
		cube.Ymin = cuboid.Ymin;
		while(cube.Ymin < cuboid.Ymax)
		{
			cube.Ymax = cube.Ymin + SMALL_CUBE_SIZE;
			cube.Xmin = cuboid.Xmin;
			while(cube.Xmin < cuboid.Xmax)
			{
				cube.Xmax = cube.Xmin + SMALL_CUBE_SIZE;
				FindCuboidCentreAndRadius(&cube);

				INDEX cubeindex = SmallCubeIndex(cube.CentreX, cube.CentreY, cube.CentreZ);
				SMALLCUBE *currentcube = &SmallCubes[cubeindex];

				short pegpolycount, panpolycount, pegvertcount, panvertcount;
				
				POLYSET* polyset;
				polyset = &LocalTheme->PolySets[mesh->PolySetIndices[PEG_INDEX]];
				CountPolysAndVertsInCuboid(polyset, &currentcube->Header, matrix, &pegpolycount, &pegvertcount);
				polyset = &LocalTheme->PolySets[mesh->PolySetIndices[PAN_INDEX]];
				CountPolysAndVertsInCuboid(polyset, &currentcube->Header, matrix, &panpolycount, &panvertcount);
				
				if(Elevation != 0)
				{
					U16 rootedges = EdgeCount[unit->RootEdges % 16];
					if(Elevation != 0)			//if the unit is off the ground then add the following storage space
					{
						pegpolycount += rootedges;	
						pegvertcount += rootedges * 4;	
					}	
				}

				if((pegpolycount > 0) || (panpolycount > 0))
				{
					currentcube->Header.PolyNum += (pegpolycount + panpolycount);
					currentcube->Header.VertNum += (pegvertcount + panvertcount);
				}
				cube.Xmin += SMALL_CUBE_SIZE;
			}
			cube.Ymin += SMALL_CUBE_SIZE;
		}
		cube.Zmin += SMALL_CUBE_SIZE;
	}
}

void UtilizeUnitGeometryStorage(const CUBE_HEADER_LOAD* srccuboid, const TRACKUNIT* unit, REVOLTMATRIX* matrix, U16 Elevation)
{
	MESH* mesh = &LocalTheme->Meshes[unit->MeshID];

	CUBE_HEADER_LOAD cuboid, cube;
	CopyCuboid(&cuboid, srccuboid);			//take a copy of the units cuboid
	ExpandCuboid(&cuboid, SMALL_CUBE_SIZE);		

	cube.Zmin = cuboid.Zmin;
	while(cube.Zmin < cuboid.Zmax)
	{
		cube.Zmax = cube.Zmin + SMALL_CUBE_SIZE;
		cube.Ymin = cuboid.Ymin;
		while(cube.Ymin < cuboid.Ymax)
		{
			cube.Ymax = cube.Ymin + SMALL_CUBE_SIZE;
			cube.Xmin = cuboid.Xmin;
			while(cube.Xmin < cuboid.Xmax)
			{
				cube.Xmax = cube.Xmin + SMALL_CUBE_SIZE;
				FindCuboidCentreAndRadius(&cube);

				INDEX cubeindex = SmallCubeIndex(cube.CentreX, cube.CentreY, cube.CentreZ);
				SMALLCUBE *currentcube = &SmallCubes[cubeindex];

				FillSmallCube(currentcube, unit, matrix, Elevation);

				cube.Xmin += SMALL_CUBE_SIZE;
			}
			cube.Ymin += SMALL_CUBE_SIZE;
		}
		cube.Zmin += SMALL_CUBE_SIZE;
	}
}

void ForEachUnit(void (*UnitFunc)(const CUBE_HEADER_LOAD* srccuboid, const TRACKUNIT* unit, REVOLTMATRIX* matrix, U16 Elevation) )
{
	assert(LocalTrack != NULL);
	assert(LocalTheme != NULL);

	//translation vector to move cuboid to the position of an instance on the track
	REVOLTVECTOR translation = {ZERO, ZERO, ZERO};

	REVOLTMATRIX	objectmatrix;
	CUBE_HEADER_LOAD cuboid;	//cuboid representing the bounds of the current unit

	U32 n = 0;
	U32 m = LocalTrack->Width * LocalTrack->Height;
	while(m--)
	{
		MakeUnitTransformationMatrix(&LocalTrack->Units[m], &objectmatrix); //work out transformation matrix for this position on the track

		CopyCuboid(&cuboid, &UnitCuboids[LocalTrack->Units[m].UnitID]);	//take a working copy 

		translation.X = LocalTrack->Units[m].XPos * SMALL_CUBE_SIZE;	//work out the translation
		translation.Y = LocalTrack->Units[m].Elevation * -ElevationStep; //corresponding to the 
		translation.Z = LocalTrack->Units[m].YPos * SMALL_CUBE_SIZE;	//units elevation

		TranslateCuboid(&translation, &cuboid); //apply this translation to the cuboid
		if(translation.Y != ZERO)				//if it is off the ground
		{
			cuboid.Ymax = ZERO;		//extend it back to the ground
		}
		for(U16 v = 0; v < 8; v++)	
		{
			UnitRootVects[v].Y = -translation.Y;	//move the vertices at the bottom of the root polys down
		}											//to compensate for the elevation that is about to happen
		
		TRACKUNIT* unit = LocalTheme->Units[LocalTrack->Units[m].UnitID];
		RootEdges = unit->RootEdges;
		UnitFunc(&cuboid, unit, &objectmatrix, LocalTrack->Units[m].Elevation);
	}
#if OUTPUT_WALLS
	U16 w = LocalTheme->UnitCount - LocalTheme->WallIndex;
	while(w--)
	{
		TRACKUNIT* unit = LocalTheme->Units[w + LocalTheme->WallIndex];
		CopyCuboid(&cuboid, &UnitCuboids[w + LocalTheme->WallIndex]);		
		TransformCuboid(&cuboid, &WallMatrix[w]);
		UnitFunc(&cuboid, unit, &WallMatrix[w], 0);
	}
#endif
}

void FillSmallCubes(void)
{
	ForEachUnit(CalculateUnitGeometryStorage);

	SMALLCUBE* smallcube = SmallCubes;
	for(U16 s = 0; s < SmallCubeCount; s++)
	{
		smallcube->Polys = new WORLD_POLY_LOAD[smallcube->Header.PolyNum];
		smallcube->Verts = new WORLD_VERTEX_LOAD[smallcube->Header.VertNum];
		smallcube++;
	}
	
	ForEachUnit(UtilizeUnitGeometryStorage);

	REVOLTVECTOR testvector;
	smallcube = SmallCubes;
	for(s = 0; s < SmallCubeCount; s++)
	{
		WORLD_VERTEX_LOAD* vert = smallcube->Verts;
		U16 vertcount = smallcube->Header.VertNum;
		while(vertcount--)
		{
   			testvector.X = vert->x;
			testvector.Y = vert->y;
			testvector.Z = vert->z;
			AddVectorToCuboid(&testvector, &smallcube->Header);
			vert++;
		}
		FindCuboidCentreAndRadius(&smallcube->Header);
		smallcube++;
	}
}

//**********************************************
// 
// CalculateWorldCuboid
//
// Calculates the bounding cuboid of all of the
// small cubes and stores the result in
// the WorldCuboid structure
// 
//**********************************************
void CalculateWorldCuboid(void)
{
	assert(LocalTrack != NULL);
	assert(LocalTheme != NULL);

	REVOLTMATRIX		objectmatrix;
	REVOLTVECTOR		vect;
	CUBE_HEADER_LOAD	cuboid;	//cuboid representing the bounds of the current unit

	NullifyCuboid(&WorldCuboid);
	
	CreateUnitCuboids();
	CalculateWallMatrices(LocalTrack);

	U32 m = LocalTrack->Width * LocalTrack->Height;
	while(m--)
	{
		MakeUnitTransformationMatrix(&LocalTrack->Units[m], &objectmatrix); //work out transformation matrix for this position on the track

		CopyCuboid(&cuboid, &UnitCuboids[LocalTrack->Units[m].UnitID]);	//take a working copy 

		TransformCuboid(&cuboid, &objectmatrix);
		
		if(LocalTrack->Units[m].Elevation != ZERO)				//if it is off the ground
		{
			cuboid.Ymax = ZERO;		//extend it back to the ground
		}
		vect.X = cuboid.Xmin;		
		vect.Y = cuboid.Ymin;		
		vect.Z = cuboid.Zmin;
		AddVectorToCuboid(&vect, &WorldCuboid);
		vect.X = cuboid.Xmax;		
		vect.Y = cuboid.Ymax;		
		vect.Z = cuboid.Zmax;
		AddVectorToCuboid(&vect, &WorldCuboid);
	}
#if OUTPUT_WALLS
	U16 w = LocalTheme->UnitCount - LocalTheme->WallIndex;
	while(w--)
	{
		CopyCuboid(&cuboid, &UnitCuboids[w + LocalTheme->WallIndex]);		
		TransformCuboid(&cuboid, &WallMatrix[w]);
		vect.X = cuboid.Xmin;		
		vect.Y = cuboid.Ymin;		
		vect.Z = cuboid.Zmin;
		AddVectorToCuboid(&vect, &WorldCuboid);
		vect.X = cuboid.Xmax;		
		vect.Y = cuboid.Ymax;		
		vect.Z = cuboid.Zmax;
		AddVectorToCuboid(&vect, &WorldCuboid);
	}
#endif
	//expand the world cube so that it encompasses whole small cubes 
	ExpandCuboid(&WorldCuboid, SMALL_CUBE_SIZE);
	CubesInWorldX = (WorldCuboid.Xmax - WorldCuboid.Xmin) / SMALL_CUBE_SIZE;
	CubesInWorldY = (WorldCuboid.Ymax - WorldCuboid.Ymin) / SMALL_CUBE_SIZE;
	CubesInWorldZ = (WorldCuboid.Zmax - WorldCuboid.Zmin) / SMALL_CUBE_SIZE;
}

//********************************************************************************************
//
//	Now we have the buffer allocation / de-allocation routines
//
//********************************************************************************************

//**********************************************
//
//
//
//**********************************************
void LocateStartZone(void)
{
	CurrentZone = MAX_INDEX; //default to no zone found
	U16 n = ZonesInTrack;
	while(n--)
	{
		TRACKZONE* zone = &ZoneBuffer[n];
		REAL xdist = fabs(StartPos.X - zone->Centre.X);
		REAL zdist = fabs(StartPos.Z - zone->Centre.Z);
		if((xdist < zone->XSize) && (zdist < zone->ZSize))
		{
			CurrentZone = n;
			break;
		}
	}
}

//**********************************************
//
//
//
//**********************************************
void FindNextZone(INDEX currentzone, U16 currentlink, INDEX* nextzone, U16* nextlink)
{
	*nextzone = MAX_INDEX;	//default to no zone found
	U16 n = ZonesInTrack;
	const TRACKZONE* thiszone = &ZoneBuffer[currentzone];
	TRACKZONE* testzone;
	while(n--)
	{
		if(n != currentzone)	//don't check against self
		{
			testzone = &ZoneBuffer[n];
			U16 link = 2;
			while(link--)
			{
				if(fabs(thiszone->Links[currentlink].Position.X - testzone->Links[link].Position.X) < Real(0.25f))	//x-coords must match
				{
					if(fabs(thiszone->Links[currentlink].Position.Z - testzone->Links[link].Position.Z) < Real(0.25f))	//z-coords must match
					{
						if(thiszone->IsPipe && testzone->IsPipe) 
						{
							if(fabs(thiszone->Links[currentlink].Position.Y - testzone->Links[link].Position.Y) < Real(0.25f))	//y-coords must match in pipes
							{
								*nextzone = n;
								*nextlink = link;
							}
						}
						else
						{
							if(thiszone->Links[currentlink].Position.Y <= testzone->Links[link].Position.Y)	//y-coords can be greater than current one
							{
								*nextzone = n;
								*nextlink = link;
							}
						}
					}
				}
			}
		}
	}
}

//**********************************************
//
//
//
//**********************************************
bool CreateZoneInformation(void)
{
	CreateZoneList();
	LocateStartZone();

	INDEX	nextzone, startzone;
	U16		exitlink, nextlink;

	ZonesVisited = 0;
	exitlink = 1;	//we are coming out of link b of the start grid
	nextzone = MAX_INDEX;	//default to illegal value
	startzone = CurrentZone;

	//special node generation for the first node in the list - this needs to be slightly back from the exit link of the start grid
	
	// get vector between entry and exit links of start grid
	PosNodeBuffer[0].Position.X = ZoneBuffer[startzone].Links[0].Position.X - ZoneBuffer[CurrentZone].Links[1].Position.X;
	PosNodeBuffer[0].Position.Y = ZoneBuffer[startzone].Links[0].Position.Y - ZoneBuffer[CurrentZone].Links[1].Position.Y;
	PosNodeBuffer[0].Position.Z = ZoneBuffer[startzone].Links[0].Position.Z - ZoneBuffer[CurrentZone].Links[1].Position.Z;

	//scale it down by 100
	PosNodeBuffer[0].Position.X /= Real(100.0f);
	PosNodeBuffer[0].Position.Y /= Real(100.0f);
	PosNodeBuffer[0].Position.Z /= Real(100.0f);
	
	//add to position of exit link
	PosNodeBuffer[0].Position.X += ZoneBuffer[startzone].Links[1].Position.X;
	PosNodeBuffer[0].Position.Y += ZoneBuffer[startzone].Links[1].Position.Y;
	PosNodeBuffer[0].Position.Z += ZoneBuffer[startzone].Links[1].Position.Z;

	bool justjumped = false;
	REAL yslope;
	do
	{
		ZoneSequence[ZonesVisited].ZoneID = CurrentZone;
		ZoneSequence[ZonesVisited].Forwards = (exitlink == 1);
		if(justjumped == true)
		{
			justjumped = false;
			PosNodeBuffer[ZonesVisited].Position = ZoneBuffer[CurrentZone].Links[1 - exitlink].Position;
		}
		
		yslope = ZoneBuffer[CurrentZone].Links[1-exitlink].Position.Y - ZoneBuffer[CurrentZone].Links[exitlink].Position.Y;
		if(yslope > (10 * GameScale))
		{
			justjumped = true;
		}
		PosNodeBuffer[ZonesVisited + 1].Position = ZoneBuffer[CurrentZone].Links[exitlink].Position;

		ZonesVisited++;
		FindNextZone(CurrentZone, exitlink, &nextzone, &nextlink);
		if((nextzone != MAX_INDEX))
		{
			CurrentZone = nextzone;
			exitlink = (1 - nextlink);
		}
		else
		{
			//couldn't find a next zone so we terminate the loop
			break;
		}

	}while((ZonesVisited < ZonesInTrack) && (nextzone != ZoneSequence[0].ZoneID));
	//loop terminates when we have used all of the zones
	//or we reach the start zone again

	if(justjumped == true)
	{
		justjumped = false;
		PosNodeBuffer[ZonesVisited].Position = ZoneBuffer[startzone].Links[1 - exitlink].Position;
	}

	PosnNodeCount = ZonesVisited + 1;
	return (nextzone == ZoneSequence[0].ZoneID);
}

//**********************************************
//
//
//
//**********************************************
void DestroyZoneInformation(void)
{
	delete[] ZoneSequence;
	ZoneSequence = NULL;
	delete[] ZoneBuffer;
	ZoneBuffer = NULL;
	delete[] PosNodeBuffer;
	PosNodeBuffer = NULL;
}

//**********************************************
//
//
//
//**********************************************
int GetBridgeFlags(TRACKDESC* track, INDEX moduleindex)
{
	INDEX zpos = moduleindex / track->Width;
	INDEX xpos = moduleindex % track->Width;
	INDEX zoneindex = ZonesVisited;
	bool  reversed[2] = {false, false};
	int elevation[2] = {-1, -1};
	while(zoneindex--)
	{
		TRACKZONE* zone = &ZoneBuffer[ZoneSequence[zoneindex].ZoneID];
		if((zone->Centre.X / SMALL_CUBE_SIZE) == xpos)
		{
			if((zone->Centre.Z / SMALL_CUBE_SIZE) == zpos)
			{
				if(elevation[0] == -1)
				{
					elevation[0] = fabs(zone->Centre.Y / SMALL_CUBE_SIZE);
					reversed[0] = !ZoneSequence[zoneindex].Forwards;
				}
				else
				{
					elevation[1] = fabs(zone->Centre.Y / SMALL_CUBE_SIZE);
					reversed[1] = !ZoneSequence[zoneindex].Forwards;
				}
			}
		}
	}
	int flags = 0;
	if(elevation[0] < elevation[1])
	{
		if(reversed[0])
		{
			flags |= LOWER_DECK_REVERSED;
		}
		if(reversed[1])
		{
			flags |= UPPER_DECK_REVERSED;
		}
	}
	else
	{
		if(reversed[1])
		{
			flags |= LOWER_DECK_REVERSED;
		}
		if(reversed[0])
		{
			flags |= UPPER_DECK_REVERSED;
		}
	}
	return flags;
}

//**********************************************
//
//
//
//**********************************************
void CorrectBridgeDirections(TRACKDESC* track)
{
	INDEX moduleindex = (track->Width * track->Height);
	while(moduleindex--)
	{
		INDEX moduleid = track->Modules[moduleindex].ModuleID;
		if((moduleid >= TWM_BRIDGE_10_2_N) && (moduleid <= TWM_BRIDGE_80_2_N))
		{
			moduleid -= (TWM_BRIDGE_10_2_N - TWM_BRIDGE_10_2);
			int flags = GetBridgeFlags(track, moduleindex);
			if(flags & LOWER_DECK_REVERSED)
			{
				track->Modules[moduleindex].Direction = ReverseDirection[track->Modules[moduleindex].Direction];
			}
			if((flags == LOWER_DECK_REVERSED) || (flags == UPPER_DECK_REVERSED))
			{
				moduleid += (TWM_BRIDGE_10_2_LH - TWM_BRIDGE_10_2);
			}
			track->Modules[moduleindex].ModuleID = moduleid;
		}
	}
}

//**********************************************
//
//
//
//**********************************************
void CorrectCarpetDirections(TRACKDESC* track)
{
	INDEX moduleindex = (track->Width * track->Height);
	while(moduleindex--)
	{
		INDEX moduleid = track->Modules[moduleindex].ModuleID;
		if((moduleid >= TWM_DIP_R) && (moduleid <= TWM_HUMP_XT_2))
		{
			track->Modules[moduleindex].Direction = ReverseDirection[track->Modules[moduleindex].Direction];
		}
	}
	CorrectBridgeDirections(track);
}

//**********************************************
//
//
//
//**********************************************
void MakeModulesDirectional(TRACKDESC* track)
{
	INDEX index = ZonesVisited;
	while(index--)
	{
		TRACKZONE* zone = &ZoneBuffer[ZoneSequence[index].ZoneID];
		S16 xpos = zone->Centre.X / SMALL_CUBE_SIZE;
		S16 ypos = zone->Centre.Z / SMALL_CUBE_SIZE;
		U16 n = (ypos * track->Width) + xpos;
		//find the offset to the modules master position
		xpos -= track->Modules[n].XOffset;
		ypos -= track->Modules[n].YOffset;
		
		//calculate the index for this element
		n = (ypos * track->Width) + xpos;
		INDEX moduleid = track->Modules[n].ModuleID;
		INDEX newmoduleid;
		if(ZoneSequence[index].Forwards == true)
		{
			newmoduleid = LocalTheme->Lookup->Changes[moduleid].Forward;
		}
		else
		{
			newmoduleid = LocalTheme->Lookup->Changes[moduleid].Reverse;
			if( ((newmoduleid >= TWM_DIP_R) && (newmoduleid <= TWM_HUMP_XT_2)) || (newmoduleid == TWM_RAMP_00_2) )
			{
				track->Modules[n].Direction = ReverseDirection[track->Modules[n].Direction];
			}
		}
		track->Modules[n].ModuleID = newmoduleid;
	}
	CorrectBridgeDirections(track);
}

//**********************************************
//
//
//
//**********************************************
void CreateUnitCuboids(void)
{
	assert(LocalTheme != NULL);

	U32 n = LocalTheme->UnitCount;

	UnitCuboids = new CUBE_HEADER_LOAD[n];

	TRACKUNIT* unit;
	while(n--)
	{
		unit = LocalTheme->Units[n];			//get a unit
		CuboidOfUnit(unit, &UnitCuboids[n]);	//and works out the bounding cuboid of all its polys
	}
}

//**********************************************
//
//
//
//**********************************************
void DestroyUnitCuboids(void)
{
	delete[] UnitCuboids;
	UnitCuboids = NULL;
}

//**********************************************
//
//
//
//**********************************************
void CreateSmallCubes(void)
{
	SmallCubeCount = CubesInWorldX * CubesInWorldY * CubesInWorldZ;
	SmallCubes = new SMALLCUBE[SmallCubeCount];
	S32 xpos, ypos, zpos;
	zpos = WorldCuboid.Zmin + SMALL_CUBE_HALF;
	U16 i = 0;
	for(U16 z = 0; z < CubesInWorldZ; z++)
	{
		ypos = WorldCuboid.Ymin + SMALL_CUBE_HALF;
		for(U16 y = 0; y < CubesInWorldY; y++)
		{
			xpos = WorldCuboid.Xmin + SMALL_CUBE_HALF;
			for(U16 x = 0; x < CubesInWorldX; x++)
			{
				SmallCubes[i].Header.CentreX = xpos;
				SmallCubes[i].Header.CentreY = ypos;
				SmallCubes[i].Header.CentreZ = zpos;
				SmallCubes[i].Header.Radius = SMALL_CUBE_RADIUS; 
				SmallCubes[i].Header.Xmin = xpos - SMALL_CUBE_HALF;
				SmallCubes[i].Header.Ymin = ypos - SMALL_CUBE_HALF;
				SmallCubes[i].Header.Zmin = zpos - SMALL_CUBE_HALF;
				SmallCubes[i].Header.Xmax = xpos + SMALL_CUBE_HALF;
				SmallCubes[i].Header.Ymax = ypos + SMALL_CUBE_HALF;
				SmallCubes[i].Header.Zmax = zpos + SMALL_CUBE_HALF;
				SmallCubes[i].Polys = NULL;
				SmallCubes[i].Header.PolyNum = 0;
				SmallCubes[i].NextPoly = 0;
				SmallCubes[i].Verts = NULL;
				SmallCubes[i].Header.VertNum = 0;
				SmallCubes[i].NextVert = 0;
				xpos += SMALL_CUBE_SIZE;
				i++;
			}
			ypos += SMALL_CUBE_SIZE;
		}
		zpos += SMALL_CUBE_SIZE;
	}
	FillSmallCubes();
}

//**********************************************
//
//
//
//**********************************************
void DestroySmallCubes(void)
{
	if(SmallCubes != NULL)
	{
		for(U16 c = 0; c < SmallCubeCount; c++)
		{
			delete[] SmallCubes[c].Polys;
			delete[] SmallCubes[c].Verts;
		}
		delete[] SmallCubes;
		SmallCubes = NULL;
		SmallCubeCount = 0;
	}
}

//**********************************************
//
//
//
//**********************************************
void CorrectBigCubeRadius(BIGCUBE* bigcube)
{
	for(U16 s = 0; s< bigcube->Header.CubeNum; s++)
	{
		SMALLCUBE* smallcube = &SmallCubes[bigcube->CubeIndices[s]];

		REAL xdist = fabs(bigcube->Header.x - smallcube->Header.Xmin);	//get the distance from the center
		REAL ydist = fabs(bigcube->Header.y - smallcube->Header.Ymin);	//of the sphere to the min corner
		REAL zdist = fabs(bigcube->Header.z - smallcube->Header.Zmin);	//for each of the 3 axes

		//get the square of the radius
		REAL limit = (bigcube->Header.Radius * bigcube->Header.Radius);
		//now calculate the square of the distance to the vector
		REAL distance = ((xdist * xdist) + (ydist * ydist) + (zdist * zdist));
								
		if(distance > bigcube->Header.Radius)
		{
			bigcube->Header.Radius = sqrt(distance);
		}

		xdist = fabs(bigcube->Header.x - smallcube->Header.Xmax);	//get the distance from the center
		ydist = fabs(bigcube->Header.y - smallcube->Header.Ymax);	//of the sphere to the max corner
		zdist = fabs(bigcube->Header.z - smallcube->Header.Zmax);	//for each of the 3 axes

		//get the square of the radius
		limit = (bigcube->Header.Radius * bigcube->Header.Radius);
		//now calculate the square of the distance to the vector
		distance = ((xdist * xdist) + (ydist * ydist) + (zdist * zdist));
								
		if(distance > bigcube->Header.Radius)
		{
			bigcube->Header.Radius = sqrt(distance);
		}
	}
}
//**********************************************
//
//
//
//**********************************************
void CreateBigCubes(void)
{
	U16				 xcubes, ycubes, zcubes;
	REAL			 x, y, z;

	BigCubeCount = 0;		//start off with no big cubes

	x = WorldCuboid.Xmin;	//start off at left
	y = WorldCuboid.Ymin;	//and top
	z = WorldCuboid.Zmin;	//and front of world cuboid

	xcubes = ycubes = zcubes = 0;	//zero out number of cubes along each axis

	while(x <= WorldCuboid.Xmax)	//if we are not at the right hand edge of the cube
	{
		xcubes ++;					//increment # of cubes on x axis
		x += BIG_CUBE_SIZE;			//move right by one cube
	}
	while(y <= WorldCuboid.Ymax)	//if we are not at the bottom edge of the cube
	{
		ycubes ++;					//increment # of cubes on y axis
		y += BIG_CUBE_SIZE;			//move down by one cube
	}
	while(z <= WorldCuboid.Zmax)	//if we are not at the back edge of the cube
	{
		zcubes ++;					//increment # of cubes on z axis
		z += BIG_CUBE_SIZE;			//move forward by one cube
	}
	BigCubeCount = xcubes * ycubes * zcubes;	//now calculate the total number of cubes
	BigCubes = new BIGCUBE[BigCubeCount];		//assign memory for that many cubes
	LinkTable = new INDEX[SmallCubeCount];		//assign memory for the link table
												//use a small cube number as an index into this
												//table and you can then read off which big cube
												//that small cube falls into
	for(U16 n = 0; n < SmallCubeCount; n++)		//initialize the table to impossible values
	{
		LinkTable[n] = MAX_INDEX;
	}

	x = WorldCuboid.Xmin;						
	y = WorldCuboid.Ymin;
	z = WorldCuboid.Zmin;

	BIGCUBE* bigcube = BigCubes;
	for(U16 b = 0; b < BigCubeCount; b++)
	{
		bigcube->Header.CubeNum = 0;
		bigcube->Header.x = x + (BIG_CUBE_SIZE / Real(2.0f));
		bigcube->Header.y = y + (BIG_CUBE_SIZE / Real(2.0f));
		bigcube->Header.z = z + (BIG_CUBE_SIZE / Real(2.0f));
		bigcube->Header.Radius = BIG_CUBE_RADIUS;
				
		SMALLCUBE* smallcube = SmallCubes;
		REVOLTVECTOR testvector;
		for(U16 s = 0; s < SmallCubeCount; s++)
		{
			testvector.X = smallcube->Header.CentreX;
			testvector.Y = smallcube->Header.CentreY;
			testvector.Z = smallcube->Header.CentreZ;
			if(SphereContainsVector(&bigcube->Header, &testvector) == true)
			{
				if(LinkTable[s] == MAX_INDEX)
				{
					LinkTable[s] = b;
					bigcube->Header.CubeNum++;
				}
			}
			smallcube++;
		}
		bigcube++;
		x += BIG_CUBE_SIZE;
		if(x >= WorldCuboid.Xmax)
		{
			x = WorldCuboid.Xmin;
			y += BIG_CUBE_SIZE;
			if(y >= WorldCuboid.Ymax)
			{
				y = WorldCuboid.Ymin;
				z += BIG_CUBE_SIZE;
			}
		}
	}
	bigcube = BigCubes;
	for(b = 0; b < BigCubeCount; b++)
	{
		bigcube->CubeIndices = new U32[bigcube->Header.CubeNum];
		U16 n = 0;
		for(U16 s = 0; s < SmallCubeCount; s++)
		{
			if(LinkTable[s] == b)
			{
				bigcube->CubeIndices[n++] = s;
			}
		}
		CorrectBigCubeRadius(bigcube);
		bigcube++;
	}
}

//**********************************************
//
//
//
//**********************************************
void DestroyBigCubes(void)
{
	delete[] LinkTable;
	LinkTable = NULL;
	if(BigCubes != NULL)
	{
		while(BigCubeCount--)
		{
			delete[] BigCubes[BigCubeCount].CubeIndices;
		}
		delete[] BigCubes;
		BigCubes = NULL;
	}
}

//**********************************************
//
//
//
//**********************************************
void FillPlane(PLANE* plane, const REVOLTVECTOR* vect0, const REVOLTVECTOR* vect1, const REVOLTVECTOR* vect2)
{
	REVOLTVECTOR normal;
	//first calculate the normal to the 3 vectors supplied
	CalculateD3DNormal(&normal, vect0, vect1, vect2);	
	
	//now work out the shortest distance from the poly to the world origin
	REAL d = -((vect0->X * normal.X) + (vect0->Y * normal.Y) + (vect0->Z * normal.Z));
	
	//stuff the answers into the plane structure
	plane->v[0] = normal.X;
	plane->v[1] = normal.Y;
	plane->v[2] = normal.Z;
	plane->v[3] = d;
}

//**********************************************
//
//
//
//**********************************************
bool NormalIsHorizontal(REVOLTVECTOR& normal)
{
	bool horizontal = false;
	if(ApproxEqual(fabs(normal.X), ONE) == TRUE)
	{
		if(ApproxEqual(normal.Z, ZERO) == TRUE)
		{
			if(ApproxEqual(normal.Y, ZERO) == TRUE)
			{
				horizontal = true;
			}
		}
	}
	else
	{
		if(ApproxEqual(fabs(normal.Z), ONE) == TRUE)
		{
			if(ApproxEqual(normal.X, ZERO) == TRUE)
			{
				if(ApproxEqual(normal.Y, ZERO) == TRUE)
				{
					horizontal = true;
				}
			}
		}
	}
	return horizontal;
}

//**********************************************
//
//
//
//**********************************************
void FillCollisionPoly(NEWCOLLPOLY* collpoly, const BASICPOLY* poly, const REVOLTMATRIX* matrix)
{
	if(poly->IsTriangle == TRUE)
	{
		collpoly->Type = 0;
	}
	else
	{
		collpoly->Type = QUAD;
	}
	REVOLTVECTOR vect[4];
	REVOLTVECTOR tempvect;
	REVOLTVECTOR normal;
	U16 vertcount = VertsInPoly(poly);
	for(U16 v = 0; v < vertcount; v++)
	{
		tempvect = LocalTheme->Verts[poly->Vertices[v]];
		if(tempvect.Y > SMALL_CUBE_SIZE)
		{
			tempvect.Y +=SMALL_CUBE_SIZE;
		}
		TRANSFORM_VECTOR( vect[(vertcount - 1)-v], tempvect, (REVOLTMATRIX&)*matrix);
	}

	FillPlane(&collpoly->Plane, &vect[0], &vect[1], &vect[2]);
	
	//now take a copy of the normal from the main plane
	normal.X = collpoly->Plane.v[0];
	normal.Y = collpoly->Plane.v[1];
	normal.Z = collpoly->Plane.v[2];

	//if poly is vertical then cap its distance below ground to a uniform dimension
	if(NormalIsHorizontal(normal))
	{
		for(v = 0; v < vertcount; v++)
		{
			vect[v].Y = min(vect[v].Y, SMALL_CUBE_SIZE);
		}
	}

	//now use it to form an edge of each of the edgeplanes
	tempvect.X = vect[0].X + normal.X;
	tempvect.Y = vect[0].Y + normal.Y;
	tempvect.Z = vect[0].Z + normal.Z;
	FillPlane(&collpoly->EdgePlane[0], &tempvect, &vect[0], &vect[1]);
	tempvect.X = vect[1].X + normal.X;
	tempvect.Y = vect[1].Y + normal.Y;
	tempvect.Z = vect[1].Z + normal.Z;
	FillPlane(&collpoly->EdgePlane[1], &tempvect, &vect[1], &vect[2]);
	tempvect.X = vect[2].X + normal.X;
	tempvect.Y = vect[2].Y + normal.Y;
	tempvect.Z = vect[2].Z + normal.Z;
	if(poly->IsTriangle == TRUE)
	{
		FillPlane(&collpoly->EdgePlane[2], &tempvect, &vect[2], &vect[0]);
	}
	else
	{
		FillPlane(&collpoly->EdgePlane[2], &tempvect, &vect[2], &vect[3]);
		tempvect.X = vect[3].X + normal.X;
		tempvect.Y = vect[3].Y + normal.Y;
		tempvect.Z = vect[3].Z + normal.Z;
		FillPlane(&collpoly->EdgePlane[3], &tempvect, &vect[3], &vect[0]);
	}

	CUBE_HEADER_LOAD cuboid;
	NullifyCuboid(&cuboid);
	AddVectorToCuboid(&vect[0], &cuboid);
	AddVectorToCuboid(&vect[1], &cuboid);
	AddVectorToCuboid(&vect[2], &cuboid);
	if(poly->IsTriangle == FALSE)
	{
		AddVectorToCuboid(&vect[3], &cuboid);
	}
	collpoly->BBox.XMin = cuboid.Xmin;
	collpoly->BBox.YMin = cuboid.Ymin;
	collpoly->BBox.ZMin = cuboid.Zmin;
	collpoly->BBox.XMax = cuboid.Xmax;
	collpoly->BBox.YMax = cuboid.Ymax;
	collpoly->BBox.ZMax = cuboid.Zmax;
}

//**********************************************
//
//
//
//**********************************************
bool CuboidOverlapsBBox(const CUBE_HEADER_LOAD* cuboid, const BBOX* bbox)
{
	if (cuboid->Xmin > bbox->XMax || cuboid->Xmax < bbox->XMin || cuboid->Zmin > bbox->ZMax || cuboid->Zmax < bbox->ZMin)
	{
		return false;
	}
	return true;
}

//**********************************************
//
//
//
//**********************************************
bool CreateCollisionPolys(void)
{
	CollisionData.Header.XStart = WorldCuboid.Xmin;
	CollisionData.Header.ZStart = WorldCuboid.Zmin;
	CollisionData.Header.XNum = CubesInWorldX;
	CollisionData.Header.ZNum = CubesInWorldZ;
	CollisionData.Header.GridSize = SMALL_CUBE_SIZE;

	CollisionData.NumPolys = CollisionData.NumUsedPolys = CountCollisionPolys();
	CollisionData.Polys = new NEWCOLLPOLY[CollisionData.NumPolys];
	CollisionData.PolyStatus = new S16[CollisionData.NumPolys];

	CollisionCellCount = CubesInWorldX * CubesInWorldZ;
	CollisionData.Cells = new CELLDATA[CollisionCellCount];
	
	U16 cellcount = CollisionCellCount;
	while(cellcount--)
	{
		CollisionData.Cells[cellcount].NumEntries = 0;
		CollisionData.Cells[cellcount].CollPolyIndices = NULL;
	}

	if((CollisionData.Polys == NULL) || (CollisionData.PolyStatus == NULL) || (CollisionData.Cells ==NULL))
	{
		return false;
	}
	
	NEWCOLLPOLY* collpoly  = CollisionData.Polys;
	REVOLTMATRIX	 objectmatrix;

	U16 unitindex = LocalTrack->Width * LocalTrack->Height;
	while(unitindex--)
	{
		MakeUnitTransformationMatrix(&LocalTrack->Units[unitindex], &objectmatrix);

		TRACKUNIT*	unit = LocalTheme->Units[LocalTrack->Units[unitindex].UnitID];
		MESH*		mesh = &LocalTheme->Meshes[unit->MeshID];
		U16			ps	 = mesh->PolySetCount;

		ps	 = mesh->PolySetCount;
		while(ps-- > HULL_INDEX)
		{
			POLYSET* polyset = &LocalTheme->PolySets[mesh->PolySetIndices[ps]];
			U16 p = polyset->PolygonCount;
			while(p--)
			{
				BASICPOLY* poly = &LocalTheme->Polys[polyset->Indices[p] & ~GOURAUD_SHIFTED];
				collpoly->Material = unit->Surfaces[p];
				FillCollisionPoly(collpoly, poly, &objectmatrix);
				collpoly++;
			}
		}
	}
#if OUTPUT_WALLS
	unitindex = LocalTheme->WallIndex;
	do{
		TRACKUNIT*	unit = LocalTheme->Units[unitindex];
		MESH*		mesh = &LocalTheme->Meshes[unit->MeshID];
		U16			ps	 = mesh->PolySetCount;

		ps	 = mesh->PolySetCount;
		while(ps-- > HULL_INDEX)
		{
			POLYSET* polyset = &LocalTheme->PolySets[mesh->PolySetIndices[ps]];
			U16 p = polyset->PolygonCount;
			while(p--)
			{
				BASICPOLY* poly = &LocalTheme->Polys[polyset->Indices[p] & ~GOURAUD_SHIFTED];
				collpoly->Material = unit->Surfaces[p];
				FillCollisionPoly(collpoly, poly, &WallMatrix[unitindex - LocalTheme->WallIndex]);
				collpoly++;
			}
		}
		unitindex++;
	}while(unitindex < LocalTheme->UnitCount);
#endif

	return true;
}

void FillCollisionCells(void)
{
	CELLDATA*	 cell;
	CUBE_HEADER_LOAD cuboid;

	cuboid.Ymin = WorldCuboid.Ymin;
	cuboid.Ymax = WorldCuboid.Ymax;
	cuboid.Zmin = WorldCuboid.Zmin - COLLISION_BLEED;
	cuboid.Zmax = WorldCuboid.Zmin + SMALL_CUBE_SIZE + COLLISION_BLEED;

	cell	  = CollisionData.Cells;
	for(U16 z = 0; z < CubesInWorldZ; z++)
	{
		cuboid.Xmin = WorldCuboid.Xmin - COLLISION_BLEED;
		cuboid.Xmax = WorldCuboid.Xmin + SMALL_CUBE_SIZE + COLLISION_BLEED;
		for(U16 x = 0; x < CubesInWorldX; x++)
		{
			cell->NumEntries = 0;

			NEWCOLLPOLY* collpoly  = CollisionData.Polys;	//start at the first collision poly
			S16 collpolyindex = 0;							//initialize loop counter
			while(collpolyindex < CollisionData.NumUsedPolys)	//while there are polys left to check
			{
				if(CuboidOverlapsBBox(&cuboid, &collpoly->BBox))	//if the polys bounding box overlaps the cells box
				{
					cell->NumEntries++;						//increment the count of overlapping polys
				}
				collpoly++;			//point to next poly
				collpolyindex++;	//increment the loop count
			}

			cell->CollPolyIndices = new long[cell->NumEntries];

			collpoly = CollisionData.Polys;					//start at the first collision poly
			collpolyindex = 0;								//initialize loop counter

			long* indices = cell->CollPolyIndices;
			while(collpolyindex < CollisionData.NumUsedPolys)	//while there are polys left to check
			{
				if(CuboidOverlapsBBox(&cuboid, &collpoly->BBox))	//if the polys bounding box overlaps the cells box
				{
					*indices = collpolyindex;				//put the index into the table
					indices++;								//and skip to the next point in the table
				}
				collpoly++;			//point to the next poly
				collpolyindex++;	//increment the loop count
			}

			cell++;
			cuboid.Xmin += SMALL_CUBE_SIZE;
			cuboid.Xmax += SMALL_CUBE_SIZE;
		}
		cuboid.Zmin += SMALL_CUBE_SIZE;
		cuboid.Zmax += SMALL_CUBE_SIZE;
	}
}

//**********************************************
//
//
//
//**********************************************
void DestroyCollisionPolys(void)
{
	if(CollisionData.Cells != NULL)
	{
		U16 cellcount = CollisionCellCount;
		while(cellcount--)
		{
			delete[] CollisionData.Cells[cellcount].CollPolyIndices;
			CollisionData.Cells[cellcount].CollPolyIndices = NULL;
		}
		delete[] CollisionData.Cells;
		CollisionData.Cells = NULL;
	}
	delete[] CollisionData.Polys;
	CollisionData.Polys = NULL;
	delete[] CollisionData.PolyStatus;
	CollisionData.PolyStatus = NULL;

}

//**********************************************
//
//
// This must be called before writing PAN or
// FAN files as they both refer to LapDistance
// in their operations.
//
//**********************************************
REAL CalculatePosNodeDistances(void)
{
	LapDistance = Real(ZERO);

	for(INDEX n = 0; n < PosnNodeCount; n++)
	{
		POSNODE* thisnode = &PosNodeBuffer[n];
		POSNODE* nextnode = &PosNodeBuffer[(n + 1) % PosnNodeCount];
		REAL xdiff, ydiff, zdiff;
		xdiff = fabs(thisnode->Position.X - nextnode->Position.X);
		ydiff = fabs(thisnode->Position.Y - nextnode->Position.Y);
		zdiff = fabs(thisnode->Position.Z - nextnode->Position.Z);
		thisnode->Distance = LapDistance;
		LapDistance += sqrt((xdiff * xdiff) + (ydiff * ydiff) + (zdiff * zdiff));
	}
	for(n = 1; n < PosnNodeCount; n++)
	{
		POSNODE* thisnode = &PosNodeBuffer[n];
		thisnode->Distance = (LapDistance - thisnode->Distance);
	}
	return LapDistance;
}

//**********************************************
//
//
//
//**********************************************
U16 CountStartGrids(void)
{
	assert(LocalTrack != NULL);
	assert(LocalTheme != NULL);

	StartPos.X	= ZERO;
	StartPos.Y	= ZERO;
	StartPos.Z	= ZERO;
	StartDir	= ZERO;
	U16 count = 0;
	U32 m = LocalTrack->Width * LocalTrack->Height;
	while(m--)
	{
		if(LocalTrack->Units[m].UnitID == UNIT_STARTGRID) 
		{
			count++;
			StartPos.X	= LocalTrack->Units[m].XPos * SMALL_CUBE_SIZE;
			StartPos.Y	= LocalTrack->Units[m].Elevation * -ElevationStep;
			StartPos.Z	= LocalTrack->Units[m].YPos * SMALL_CUBE_SIZE;
			StartDir	= LocalTrack->Units[m].Direction * Real(0.25f);
		}
	}
	return count;
}

//**********************************************
//
//
//
//**********************************************
bool ValidateTrack(void)
{
	bool trackisvalid = false;
	U16 gridcount = CountStartGrids();
	if(gridcount == 1)
	{
		trackisvalid = CreateZoneInformation();
		if(trackisvalid == true)
		{
			CalculatePosNodeDistances(); //this must be done before writing PAN or FAN files as they both use it.
			trackisvalid = CreateAINodes();
		}
		if(trackisvalid == false)
		{
			TRACKZONE* lastzone = &ZoneBuffer[ZoneSequence[ZonesVisited - 1].ZoneID];
			if(lastzone->IsPipe)
			{
				FlagError(TEXT_WARNING_TRACK_INVALID1);
			}
			else
			{
				FlagError(TEXT_WARNING_TRACK_INVALID2);
			}
			LocalCursor->X = (lastzone->Centre.X / SMALL_CUBE_SIZE);
			LocalCursor->Y = (lastzone->Centre.Z / SMALL_CUBE_SIZE);
			SetCurrentModule(MODULE_STARTGRID);	//change to 1 by 1 module in case new cursor position is on the edge of the track
		}
	}
	else
	{
		if(gridcount == 0)
		{
			FlagError(TEXT_WARNING_NO_START);
		}
		else
		{
			FlagError(TEXT_WARNING_MULTIPLE_STARTS);
		}
	}
	if(trackisvalid == false)
	{
		PopScreenState();
	}
	return trackisvalid;
}

//**********************************************
//
//
//
//**********************************************
bool NodesAreClose(const RevoltVertex* node1, const RevoltVertex* node2, REAL tolerance)
{
	if(fabs(node1->X - node2->X) < tolerance)
	{
		if(fabs(node1->Z - node2->Z) < tolerance)
		{
			return true;
		}
	}
	return false;
}

//**********************************************
//
//
//
//**********************************************
void ExtractNodesFromModule(INDEX moduleindex, INDEX zoneindex)
{
	RevoltTrackModuleInstance* instance = &LocalTrack->Modules[moduleindex];
	REVOLTMATRIX modulematrix;
	MakeModuleTransformationMatrix(instance, moduleindex % LocalTrack->Width, moduleindex / LocalTrack->Width, &modulematrix);
	RevoltVertex tempvector;
	INDEX moduleid = instance->ModuleID;
	if(moduleid != MAX_INDEX)
	{
		TRACKMODULE* module = LocalTheme->Modules[moduleid];
		U16 g = 0;
		U16 leftnode = AI_GREEN_NODE;
		U16 rightnode = AI_RED_NODE;
		REAL racingbase = ZERO;
		S16 nstep = 1;
		U16 nlimit;
		AINODEINFO* destnode = &AINodes[AINodesPlaced - 1];

		if(AINodesPlaced > 0)
		{
			do{
				U16 nodecount = module->NodeCount[g];
				if(nodecount > 0)
				{
					TransformRevoltVertex(&tempvector, &module->Nodes[g][0]->Ends[AI_GREEN_NODE], modulematrix);
					if(NodesAreClose(&destnode->Ends[AI_GREEN_NODE], &tempvector, Real(20.0f)))
					{
						break;
					}
					TransformRevoltVertex(&tempvector, &module->Nodes[g][nodecount - 1]->Ends[AI_RED_NODE], modulematrix);
					if(NodesAreClose(&destnode->Ends[AI_GREEN_NODE], &tempvector, Real(20.0f)))
					{
						leftnode = AI_RED_NODE;
						rightnode = AI_GREEN_NODE;
						nstep = -1;
						racingbase = 1.0f;
						break;
					}
				}
				g++;
			}while(g < 2);
		}		
		if(g < 2)
		{
			U16 nodecount = module->NodeCount[g];
			if(nodecount > 0)
			{
				U16 n = 0;
				if(leftnode == AI_GREEN_NODE)
				{
					nlimit = nodecount;
				}
				else
				{
					nlimit = MAX_U16;
					n = nodecount - 1;
				}
				destnode = &AINodes[AINodesPlaced];
				if(AINodesPlaced > 0)
				{
					destnode--;		//step back to the node that we need to merge with

					destnode->RacingLine = (destnode->RacingLine + fabs(racingbase - module->Nodes[g][n]->RacingLine)) / 2;
					destnode->Priority = module->Nodes[g][n]->Priority;

					TransformRevoltVertex(&tempvector, &module->Nodes[g][n]->Ends[leftnode], modulematrix);
					if(tempvector.Y < destnode->Ends[AI_GREEN_NODE].Y)
					{
						AINodesValid = false;	//indicate that we founf an invalid AI node
						ZonesVisited = zoneindex;	//and show where we got to (this also terminates the loop withing 'ForEachModule')
					}
					destnode->Ends[AI_GREEN_NODE].X = tempvector.X;
					destnode->Ends[AI_GREEN_NODE].Y = min(tempvector.Y, destnode->Ends[AI_GREEN_NODE].Y);
					destnode->Ends[AI_GREEN_NODE].Z = tempvector.Z;
					if(destnode->Ends[AI_GREEN_NODE].Y < -(19 * GameScale))
					{
						destnode->Ends[AI_GREEN_NODE].Y = tempvector.Y;
					}

					TransformRevoltVertex(&tempvector, &module->Nodes[g][n]->Ends[rightnode], modulematrix);
					if(tempvector.Y < destnode->Ends[AI_RED_NODE].Y)
					{
						AINodesValid = false;	//indicate that we founf an invalid AI node
						ZonesVisited = zoneindex;	//and show where we got to (this also terminates the loop withing 'ForEachModule')
					}
					destnode->Ends[AI_RED_NODE].X = tempvector.X;
					destnode->Ends[AI_RED_NODE].Y = min(tempvector.Y, destnode->Ends[AI_RED_NODE].Y);
					destnode->Ends[AI_RED_NODE].Z = tempvector.Z;
					if(destnode->Ends[AI_RED_NODE].Y < -(19 * GameScale))
					{
						destnode->Ends[AI_RED_NODE].Y = tempvector.Y;
					}
					destnode++;
					n += nstep;
				}
				while(n != nlimit)
				{
					TransformRevoltVertex(&destnode->Ends[AI_GREEN_NODE], &module->Nodes[g][n]->Ends[leftnode], modulematrix);
					TransformRevoltVertex(&destnode->Ends[AI_RED_NODE], &module->Nodes[g][n]->Ends[rightnode], modulematrix);
					destnode->RacingLine = fabs(racingbase - module->Nodes[g][n]->RacingLine);
					destnode->Priority = module->Nodes[g][n]->Priority;
					destnode++;
					n += nstep;
					AINodesPlaced++;
				}
			}
		}
	}
}

//**********************************************
//
//
//
//**********************************************
void CountAINodes(INDEX moduleindex, INDEX /*zoneindex*/)
{
	RevoltTrackModuleInstance* instance = &LocalTrack->Modules[moduleindex];
	INDEX moduleid = instance->ModuleID;
	if(moduleid != MAX_INDEX)
	{
		U16 nodecount = LocalTheme->Modules[moduleid]->NodeCount[0];
		if(nodecount)
		{
			AINodeCount += (nodecount - 1);
		}
		nodecount = LocalTheme->Modules[moduleid]->NodeCount[1];
		if(nodecount)
		{
			AINodeCount += (nodecount - 1);
		}
	}
}

//**********************************************
//
//
//
//**********************************************
bool CreateAINodes(void)
{
	AINodeCount = 0;
	AINodesValid = false;
	ForEachModule(CountAINodes);
	if(AINodeCount > 0)
	{
		AINodes = new AINODEINFO[AINodeCount + 1];		//allocate enough room for the nodes, plus one more for the wrap
		AINodesPlaced = 0;
		AINodesValid = true;	//assume nodes are valid - following call will make the real decision
		ForEachModule(ExtractNodesFromModule);

		AINODEINFO* destnode = &AINodes[AINodesPlaced - 1];	//get the last node
		//and set the y-coords of the first node to the highest of the 2
		AINodes[0].Ends[AI_GREEN_NODE].Y = min(AINodes[0].Ends[AI_GREEN_NODE].Y, destnode->Ends[AI_GREEN_NODE].Y);
		AINodes[0].Ends[AI_RED_NODE].Y = min(AINodes[0].Ends[AI_RED_NODE].Y, destnode->Ends[AI_RED_NODE].Y);
		AINodes[0].RacingLine = (AINodes[0].RacingLine + destnode->RacingLine) / 2;
		AINodesPlaced--;	//we can now discount the last node placed as it is not actually used
		U16 n = 0;
		char temppriority = AINodes[0].Priority;
		while(n < (AINodesPlaced - 1))
		{
			AINodes[n].Priority = AINodes[n + 1].Priority;
			n++;
		}
		AINodes[AINodesPlaced - 1].Priority = temppriority;
	}
	return AINodesValid;
}

//**********************************************
//
//
//
//**********************************************
void CountLights(INDEX moduleindex, INDEX /*zoneindex*/)
{
	RevoltTrackModuleInstance* instance = &LocalTrack->Modules[moduleindex];
	INDEX moduleid = instance->ModuleID;
	if(moduleid != MAX_INDEX)
	{
		LightCount += LocalTheme->Modules[moduleid]->LightCount;
	}
}

//**********************************************
//
//
//
//**********************************************
void PlaceLights(INDEX moduleindex, INDEX /*zoneindex*/)
{
	RevoltTrackModuleInstance* instance = &LocalTrack->Modules[moduleindex];
	INDEX moduleid = instance->ModuleID;
	if(moduleid != MAX_INDEX)
	{
		REVOLTMATRIX modulematrix;
		MakeModuleTransformationMatrix(instance, moduleindex % LocalTrack->Width, moduleindex / LocalTrack->Width, &modulematrix);
		TRACKMODULE* module = LocalTheme->Modules[moduleid];

		U16 lightcount = module->LightCount;
		while(lightcount--)
		{
			FILELIGHT* light = &Lights[LightsPlaced];
			REVOLTVECTOR tempvec;
			TRANSFORM_VECTOR(tempvec, module->Lights[lightcount]->Position, modulematrix);
			
			light->x = tempvec.X;
			light->y = tempvec.Y;
			light->z = tempvec.Z;
			light->Reach = module->Lights[lightcount]->Reach;
			
			//dont forget to work out
			RotMatrixZYX(&light->DirMatrix, 0.25f, 0, 0);
			
			light->Cone = module->Lights[lightcount]->Cone;
			light->r = module->Lights[lightcount]->Red;
			light->g = module->Lights[lightcount]->Green;
			light->b = module->Lights[lightcount]->Blue;

			light->Type = module->Lights[lightcount]->Type;
			
			LightsPlaced++;
		}
	}
}

//**********************************************
//
//
//
//**********************************************
void CreateLights(void)
{
	LightCount = 0;
	ForEachModule(CountLights);
	if(AINodeCount > 0)
	{
		Lights = new FILELIGHT[LightCount];
		LightsPlaced = 0;
		ForEachModule(PlaceLights);
	}
}

//**********************************************
//
//
//
//**********************************************
void DestroyLights(void)
{
	delete[] Lights;
}

//**********************************************
//
//
//
//**********************************************
void RemovePipeExitWall(INDEX moduleindex, INDEX zoneindex)
{
	INDEX moduleid = LocalTrack->Modules[moduleindex].ModuleID;
	
	INDEX i = sizeof(PipeWelds) / sizeof(PIPEWELD);
	while(i--)
	{
		if(PipeWelds[i].ModuleID == moduleid)
		{
			if(ZoneSequence[zoneindex].Forwards == true)
			{
				LocalTrack->Modules[moduleindex].ModuleID = PipeWelds[i].NoExit;
				return;
			}
			else
			{
				LocalTrack->Modules[moduleindex].ModuleID = PipeWelds[i].NoEntry;
				return;
			}
		}
	}
}

//**********************************************
//
//
//
//**********************************************
void RemovePipeEntryWall(INDEX moduleindex, INDEX zoneindex)
{
	INDEX moduleid = LocalTrack->Modules[moduleindex].ModuleID;
	
	INDEX i = sizeof(PipeWelds) / sizeof(PIPEWELD);
	while(i--)
	{
		if(PipeWelds[i].ModuleID == moduleid)
		{
			if(ZoneSequence[zoneindex].Forwards == true)
			{
				LocalTrack->Modules[moduleindex].ModuleID = PipeWelds[i].NoEntry;
				return;
			}
			else
			{
				LocalTrack->Modules[moduleindex].ModuleID = PipeWelds[i].NoExit;
				return;
			}
		}
	}
}


//**********************************************
//
//
//
//**********************************************
void WeldAPipe(INDEX moduleindex, INDEX zoneindex)
{
	bool inapipe = false;
	INDEX moduleid = LocalTrack->Modules[moduleindex].ModuleID;
	if(moduleid == TWM_PIPE_2 || moduleid == TWM_PIPEC_2 || moduleid == TWM_PIPE_20_2)
	{
		inapipe = true;
		if(PreviouslyInAPipe)
		{
			RemovePipeExitWall(PreviousModuleIndex, PreviousZoneIndex);
			RemovePipeEntryWall(moduleindex, zoneindex);
		}
		PreviousModuleIndex = moduleindex;
		PreviousZoneIndex = zoneindex;
	}
	PreviouslyInAPipe = inapipe;
}

//**********************************************
//
//
//
//**********************************************
void WeldPipes(void)
{
	PreviousModuleIndex = MAX_INDEX;
	PreviousZoneIndex	= MAX_INDEX;
	PreviouslyInAPipe	= false;

	ForEachModule(WeldAPipe);
}

//**********************************************
//
//
//
//**********************************************
void DestroyAINodes(void)
{
	delete[] AINodes;
	AINodes = NULL;
}

//**********************************************
//
//
//
//**********************************************
void CleanUp(void)
{
	DestroyZoneInformation();
	DestroyCollisionPolys();
	DestroyBigCubes();
	DestroySmallCubes();
	DestroyUnitCuboids();
	DestroyAINodes();
	if(LocalTrack != NULL)
	{
		DestroyTrack(LocalTrack);
		delete LocalTrack;
		LocalTrack = NULL;
	}
	LocalTheme = NULL;
	LocalCursor = NULL;
}

//**********************************************
// UseRealSpaceUnits(TRACKDESC* LocalTrack)
//
// Replace all of the space units in the track
// with the ones which look right for the game
//
//**********************************************
void UseRealSpaceUnits(TRACKDESC* LocalTrack)
{
	U32 m = LocalTrack->Width * LocalTrack->Height;

	while(m--)
	{
		if(LocalTrack->Units[m].UnitID == UNIT_SPACER)
		{
			LocalTrack->Units[m].UnitID = UNIT_REAL_SPACER;
		}
	}
}

//**********************************************
//
//
//
//**********************************************
void CompileTrack(const TRACKDESC* track, const TRACKTHEME* theme, CURSORDESC* cursor)
{
	static char levelname[MAX_PATH];

	static COMPILE_STATES nextstate[] = {
									TCS_UNIT_CUBES,
									TCS_SMALL_CUBES,
									TCS_BIG_CUBES,
									TCS_MAKING_COLLISONS,
									TCS_OPTIMIZING,
									TCS_WRITING_WORLD,
									TCS_WRITING_INF,
									TCS_WRITING_BITMAPS,
									TCS_WRITING_NCP,
									TCS_WRITING_TAZ,
									TCS_CLEANUP,
									TCS_RINGING_CLOCK,
									TCS_FINISHED,
									TCS_VALIDATING
								};

	
	for(U16 i = 0; i < 16; i++)
	{
		UnitRootVects[i] =  InitUnitRootVects[i];
	}

	if(CompileState == TCS_VALIDATING)
	{
		LocalTrack = new TRACKDESC;
		LocalTheme = theme;
		LocalCursor = cursor;

		if(LocalTrack != NULL)
		{
			char name[MAX_DESCRIPTION_LENGTH];
			NextFreeDescription(name);
			CreateTrack(LocalTrack, DEFAULT_TRACK_WIDTH, DEFAULT_TRACK_HEIGHT, name);
			CloneTrack(LocalTrack, track);
			MakeTrackFromModules(LocalTrack);
			strcpy(levelname, "");	//zero out the levelname
			if(DescribedLevelExists(LocalTrack->Name))
			{
				GetDescribedLevelName(LocalTrack->Name, levelname);
			}
			else
			{
				NextFreeLevel(levelname);
			}

			if(ValidateTrack() == false)
			{
				CleanUp();
				return;
			}
			StartLoopingSound(SND_TICK_TOCK);
			MakeModulesDirectional(LocalTrack);
			WeldPipes();
			MakeTrackFromModules(LocalTrack);
			UseRealSpaceUnits(LocalTrack);	//change to the game version of the space units - NOTE must be called AFTER MakeTrackFromModules
		}
		else
		{
			StopSound(SND_TICK_TOCK);
			FlagError(TEXT_WARNING_COMPILE_OUT_OF_MEMORY);
			PopScreenState();
			CleanUp();
			return;
		}
	}

	if(CompileState == TCS_UNIT_CUBES)
	{
		CalculateWorldCuboid();
	}

	if(CompileState == TCS_SMALL_CUBES)
	{
		CreateSmallCubes();	
	}

	if(CompileState == TCS_BIG_CUBES)
	{
		CreateBigCubes();
	}

	if(CompileState == TCS_MAKING_COLLISONS)
	{
		if(CreateCollisionPolys() == false)
		{
			StopSound(SND_TICK_TOCK);
			FlagError(TEXT_WARNING_COMPILE_OUT_OF_MEMORY);
			PopScreenState();
			CleanUp();
			CompileState = TCS_VALIDATING;
			return;
		}
	}

	if(CompileState == TCS_OPTIMIZING)
	{
		RemoveSurplusPolys();
	}

	if(strlen(levelname) > 0)
	{
		char levelpath[MAX_PATH];
		sprintf(levelpath, "%s\\%s", LevelFolder, levelname);
		CreateDirectory(levelpath, NULL);

		char levelroot[MAX_PATH];
		sprintf(levelroot, "%s\\%s\\%s", LevelFolder, levelname, levelname);
		
		char filepath[MAX_PATH];

		if(CompileState == TCS_WRITING_WORLD)
		{
			sprintf(filepath, "%s.w", levelroot);
			WriteWorldFile(filepath);
		}

		if(CompileState == TCS_WRITING_INF)
		{
			sprintf(filepath, "%s.inf", levelroot);
			WriteInfFile(filepath, LocalTrack->Name, StartPos, StartDir);
		}

		if(CompileState == TCS_WRITING_BITMAPS)
		{
			CopyBitmaps(levelroot);
		}

		if(CompileState == TCS_WRITING_NCP)
		{
			sprintf(filepath, "%s.ncp", levelroot);
			WriteNCPFile(filepath);
		}

		if(CompileState == TCS_WRITING_TAZ)
		{
			sprintf(filepath, "%s.taz", levelroot);
			WriteTAZFile(filepath);
			sprintf(filepath, "%s.pan", levelroot);
			WritePANFile(filepath);
			sprintf(filepath, "%s.fob", levelroot);
			WriteFOBFile(filepath, LocalTrack, LocalTheme);
			sprintf(filepath, "%s.fan", levelroot);
			WriteFANFile(filepath);
			CreateLights();
			sprintf(filepath, "%s.lit", levelroot);
			WriteLITFile(filepath, StartPos);
			DestroyLights();
		}
	}
	else
	{
		CompileState = TCS_CLEANUP;
	}
		
	if(CompileState == TCS_CLEANUP)
	{
		CleanUp();
	}

	switch(CompileState)
	{
		case TCS_RINGING_CLOCK:
			ClockFrame-=2;
			if(ClockFrame == 0)
			{
				CompileState = nextstate[CompileState];
				ClockFrame = CLOCK_STATIC_DURATION;
			}
		break;

		case TCS_FINISHED:
			ClockFrame-=2;
			if(ClockFrame == 0)
			{
				PopScreenState();
				CompileState = nextstate[CompileState];
			}
		break;			
		
		default:
			CompileState = nextstate[CompileState];
			if(CompileState == TCS_RINGING_CLOCK)
			{
				StopSound(SND_TICK_TOCK);
				StartSound(SND_CONGRATS);
				ClockFrame = CLOCK_RING_DURATION;
			}
		break;
	}
}

//**********************************************
//
//
//
//**********************************************

