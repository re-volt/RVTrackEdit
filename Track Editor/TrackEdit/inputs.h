#ifndef _INPUTS_H
#define _INPUTS_H

//The order of the entries in this enum is important as they
//are used as an index in the ViewpointAdjustedKey() function
enum CURSOR_DIR
{
	CURSOR_UP,
	CURSOR_LEFT,
	CURSOR_DOWN,
	CURSOR_RIGHT
};

void	HandleKeyDown(void);
void	MoveCursor(CURSORDESC* cursor, CURSOR_DIR direction);
#endif