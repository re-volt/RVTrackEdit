//-------------------------------------------------------------------------------------------------
// Re-Volt Track Unit Extraction Program.
//
//-------------------------------------------------------------------------------------------------
#include "stdafx.h"
#include "HabExtract.h"		// Structures are in here
#include <MappedVector.h>
#include <Primitives.h>
#include <Constants.h>
#include <ScriptObject.h>
#include <OutputWaveFront.h>
#include <OutputUnitFile.h>
#include <OutputPSXModels.h>
#include <UnitInfo.h>
#include <fstream>
#include <lbmsave.h>

using namespace std;

enum {
	LIGHT_OMNI,
	LIGHT_OMNINORMAL,
	LIGHT_SPOT,
	LIGHT_SPOTNORMAL,
};

#define PI 3.141592654f
const float SLICE_HEIGHT = 125.0f;

//-------------------------------------------------------------------------------------------------
// Global variables
//-------------------------------------------------------------------------------------------------
RevoltTheme*	Theme;
U32				VerticesScanned;
U32				PolygonsScanned;
VALID_TARGETS	TargetFlags	= 0;	//default to no valid targets - ensures that the user
									//has defined _PC, _N64, _PSX etc and code in 'theuserprogram'
									//has filled in a valid set of bits
EndianStream::Endian WhichEnd = EndianStream::ENDIAN_LOCAL;
bool			FixedPoint = false;

//-------------------------------------------------------------------------------------------------
// Prototypes for data processing routines
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// GetObjectPosition(RevoltVertex* position, const rv_Model* object)
// 
//-------------------------------------------------------------------------------------------------
void GetObjectPosition(RevoltVertex* position, const rv_Model* object)
{
	ASSERT(object != NULL);
	const JC_Anchor& anchor = object->Anchor();
	const JC_Vector& pin = anchor.PinVector();

	position->X = pin.X() * GameScale;
	position->Y = -pin.Y() * GameScale;
	position->Z = pin.Z() * GameScale;
}

REAL SnapValue(REAL value, REAL gridsize)
{
	if(value < ZERO)
	{
		value -= (gridsize / 2);
		value = ((int)(value / gridsize)) * gridsize;
	}
	else
	{
		value += (gridsize / 2);
		value = ((int)(value / gridsize)) * gridsize;
	}

	return value;
}

void SnapVertex(RevoltVertex* vertex, REAL gridsize)
{
	vertex->X = SnapValue(vertex->X, gridsize);
	vertex->Y = SnapValue(vertex->Y, gridsize);
	vertex->Z = SnapValue(vertex->Z, gridsize);
}

void SlicePoly(RevoltPolySet* polyset, RevoltPolygon* poly, U16 index, RevoltTrackUnit* unit, U8 surface)
{
	RevoltVertex verts[4];
	for(U16 n = 0; n < 4; n++)
	{
		verts[n] = *(Theme->Vertex(poly->at(index)));
		index++;
		index %= 4;
	}
	REAL targety = verts[2].Y;
	verts[2].Y = verts[3].Y = (verts[0].Y + SLICE_HEIGHT);
	RevoltPolygon newpoly;
	do
	{
		newpoly.clear();
		newpoly.push_back(Theme->InsertVertex(verts[0]));
		newpoly.push_back(Theme->InsertVertex(verts[1]));
		newpoly.push_back(Theme->InsertVertex(verts[2]));
		newpoly.push_back(Theme->InsertVertex(verts[3]));
		newpoly.SmallestFirst();
		polyset->push_back(Theme->InsertPolygon(newpoly));
		unit->SurfaceSet.push_back(surface);
		verts[0].Y += SLICE_HEIGHT;
		verts[1].Y += SLICE_HEIGHT;
		verts[2].Y = min(verts[2].Y + SLICE_HEIGHT, targety);
		verts[3].Y = min(verts[3].Y + SLICE_HEIGHT, targety);
	}while(verts[0].Y < targety);
}

bool PolyWasntSliced(RevoltPolySet* polyset, RevoltPolygon* poly, RevoltTrackUnit* unit, U8 surface)
{
	bool wasntsliced = true;
	if(poly->size() == 4)
	{
		RevoltVertex verts[8];
		U16 n = 4;
		while(n--)
		{
			verts[n] = verts[n+4] = *(Theme->Vertex(poly->at(n)));
		}
		n = 0;
		do
		{
			if((verts[n].Y == verts[n+1].Y) && (verts[n+2].Y == verts[n+3].Y))
			{
				if(fabs(verts[n].Y - verts[n+3].Y) > SLICE_HEIGHT)
				{
					if((verts[n].X == verts[n+3].X) && (verts[n+1].X == verts[n+2].X))
					{
						if((verts[n].Z == verts[n+3].Z) && (verts[n+1].Z == verts[n+2].Z))
						{
							if(verts[n].Y < verts[n+3].Y)
							{
								SlicePoly(polyset, poly, n, unit, surface);
							}
							else
							{
								SlicePoly(polyset, poly, (n + 2) % 4, unit, surface);
							}
							wasntsliced = false;
						}
					}
				}
			}
		}while((wasntsliced == true) && (++n < 4));
	}
	return wasntsliced;
}

//-------------------------------------------------------------------------------------------------
// ExtractComponent(rv_Model* model, RevoltPolySet* polyset)
//
//
//-------------------------------------------------------------------------------------------------
bool ExtractComponent(rv_Model* model, RevoltPolySet* polyset, RevoltTrackUnit* unit)
{
	ASSERT(polyset != NULL);
	
	bool success = false;
	
	PS_Database& db = ((CHabExtractApp*)AfxGetApp())->Database();

	U32 surfacerecnum = db.FindRecord("Properties\\Material", JC_Record::TypePropertyName);
	JC_RecordRefList& childrefs = model->ChildList();
	POSITION childpos = childrefs.GetHeadPosition();
	PS_RecordInfo recinfo;
	while(childpos && !success)
	{
		JC_RecordRef* childref = (JC_RecordRef*)childrefs.GetAt(childpos);

		U32 recordnumber = childref->RecordNum();
		if(recordnumber != PS_Database::NullRecord)
		{
			db.FirstRecord(recinfo);
			db.GetInfo(recordnumber,recinfo);
			if(recinfo.Name() == polyset->Name().c_str())
			{
				JC_Template* childmodel = GET_TEMPLATE(&db, recordnumber);
				JC_VertexList* vertices = childmodel->VertexList();
				JC_FaceList*   faces = childmodel->FaceList();

				RevoltVertex rv;
				RevoltVertex origin;
				RevoltUVCoord uv;
				RevoltUVPolygon uvpoly;
				RevoltRGBPolygon rgbpoly;
				RevoltPolygon poly;
				PS_FloatUVArray uvarray;
				CString errorinfo;
				CString modelinfo;
				U32 rotation;
				U32 polyshift;
				U32 uvshift;
				U8	r, g, b;

				GetObjectPosition(&origin, childmodel);

				POSITION facepos = faces->GetHeadPosition();
				while(facepos)
				{
					poly.clear();
					rgbpoly.clear();
					JC_Face* face = (JC_Face*)faces->GetAt(facepos);
					const JC_PointList* points = face->PointList();
					POSITION pointpos = points->GetTailPosition();
					while(pointpos)
					{
						JC_Point* point = (JC_Point*)points->GetAt(pointpos);
						rv.X = (point->Vertex()->X() * GameScale) + origin.X;
						rv.Y = -(point->Vertex()->Y() * GameScale) + origin.Y;
						rv.Z = (point->Vertex()->Z() * GameScale) + origin.Z;
						point->Colour().Get(r, g, b);
						rgbpoly.push_back((r << 16) | (g << 8) | b);

						SnapVertex(&rv, (REAL(0.010f) * GameScale));	//round to nearest 10 mm point in world grid

						U32 vertindex = Theme->InsertVertex(rv);
						poly.push_back(vertindex);
						points->GetPrev(pointpos);
						VerticesScanned++;
					}
					polyshift = poly.SmallestFirst();
					RevoltRGBPolygon::iterator start;	
					RevoltRGBPolygon::iterator finish;
					RevoltRGBPolygon::iterator middle;
					start  = rgbpoly.begin();
					finish = rgbpoly.end();
					middle = start + polyshift;
					rotate(start, middle, finish) ;	//shift the list so that the smallest comes first

					if(poly.size() < 3)
					{
						errorinfo.Format("*********** %s:- less than 3 verts ***********\n", childmodel->Path()); 
						api.Log(errorinfo);
					}

					U8 surface = 0; //default to 'NORMAL' surface
					JC_PropertyList* propertylist = face->PropertyList();
					POSITION propertypos = propertylist->GetHeadPosition();
					BOOL found = FALSE;
					while (propertypos && (found == FALSE))
					{
						JC_Property * property = (JC_Property*)propertylist->GetNext(propertypos);
						if (property)
						{
							JC_Data * value = property->CopyValue();
							if (value)
							{
								if (value->Token() == JC_Data::TokEnumRef)
								{
									JC_EnumRef * enum_ref = (JC_EnumRef*)value;
									if(property->GetNameRecordNumber() == surfacerecnum)
									{
										surface = (U8)*enum_ref;
										found = true;
									}
								}
								delete value;
							}
						}
					}
					bool usepoly = true;
					if(polyset->Name().c_str() == string("hul_00"))
					{
						if(!FixedPoint)
						{
							usepoly = PolyWasntSliced(polyset, &poly, unit, surface);
						}
					}
					
					if(usepoly == true)
					{
						U16 polyindex = Theme->InsertPolygon(poly);
						if(polyshift & 0x0001)
						{
							polyindex |= GOURAUD_SHIFTED;
						}
						polyset->push_back(polyindex);
						unit->SurfaceSet.push_back(surface);
					}

					PolygonsScanned ++;

					if(polyset->Name().c_str() == string("pan"))
					{
						uvpoly.clear();
						uvarray.RemoveAll();
						uvshift = 0;

						const PS_Texture* texture = face->FrontTexture();
						U32 texturerecord = PS_Database::NullRecord;
						if(texture != NULL)
						{
							texturerecord = texture->GetBitmapID();
							texture->GetFloatUVArray(uvarray);
							U32 c = uvarray.GetSize();
							while(c--)
							{
								uv.U = uvarray[c].u;
								uv.V = uvarray[c].v;
								U32 uvindex = Theme->InsertUVCoord(uv);
								uvpoly.push_back(uvindex);
							}
							uvshift = uvpoly.SmallestFirst();
							texture->Release();
							if(poly.size() != uvpoly.size())
							{
								errorinfo.Format("*********** poly/texture mismatch ***********\n"); 
								api.Log(errorinfo);
							}
						}
						else
						{
							for(U32 c = 0; c < poly.size(); c++)
							{
								uvpoly.push_back(0);
							}
						}
						U32 uvpolyindex = Theme->InsertUVPolygon(uvpoly);

						BA_VertexOrient& orient = face->FrontOrient();
						U32 ffv	= (poly.size() -1) - orient.FirstFaceVertex;
						U32 ftv	= (poly.size() -1) - orient.FirstTextureVertex;
						if(orient.Reversed == FALSE)
						{
							rotation = poly.size() - ffv;
							rotation += ftv;
							rotation += polyshift;
							rotation += poly.size() - uvshift;
							rotation %= poly.size();
						}
						else
						{
							rotation = ffv;
							rotation += ftv;
							rotation += poly.size() - polyshift;
							rotation += poly.size() - uvshift;
							rotation %= poly.size();
						}

						UVPolyInstance* pi = new UVPolyInstance;
						pi->TPageID = Theme->InsertTPageRecnum(texturerecord);
						pi->PolyID = uvpolyindex;
						pi->Rotation = (U8)rotation;
						pi->Reversed = (orient.Reversed != FALSE);
						unit->UVPolySet.Insert(pi);
						unit->RGBPolySet.push_back(Theme->InsertRGBPolygon(rgbpoly));
					}
					faces->GetNext(facepos);
				}
				modelinfo.Format("%s:- %d verts, %d faces\n", childmodel->Path(), vertices->GetCount(), faces->GetCount()); 
				success = true;
				childmodel->Release();
			}
		}
		childrefs.GetNext(childpos);
	}
	return success;	
}

//-------------------------------------------------------------------------------------------------
// ExtractPickup(rv_Model* model, RevoltTrackUnit* unit)
//
//
//-------------------------------------------------------------------------------------------------
bool ExtractPickup(rv_Model* model, RevoltTrackUnit* unit)
{
	ASSERT(model != NULL);
	ASSERT(unit != NULL);
	
	bool success = false;
	unit->PickupPos.X = 0.0f;
	unit->PickupPos.Y = 0.0f;
	unit->PickupPos.Z = 0.0f;

	PS_Database& db = ((CHabExtractApp*)AfxGetApp())->Database();

	JC_RecordRefList& childrefs = model->ChildList();
	POSITION childpos = childrefs.GetHeadPosition();
	PS_RecordInfo recinfo;
	while(childpos && !success)
	{
		JC_RecordRef* childref = (JC_RecordRef*)childrefs.GetAt(childpos);

		U32 recordnumber = childref->RecordNum();
		if(recordnumber != PS_Database::NullRecord)
		{
			db.FirstRecord(recinfo);
			db.GetInfo(recordnumber,recinfo);
			if(recinfo.Name() == "pickup0")
			{
				JC_Object* pickup = GET_OBJ(&db, recordnumber);

				GetObjectPosition(&unit->PickupPos, pickup);
				success = true;
				pickup->Release();
			}
		}
		childrefs.GetNext(childpos);
	}
	return success;
}

//-------------------------------------------------------------------------------------------------
// ExtractUnit(U32 recordnumber, RevoltMesh* mesh, RevoltTrackUnit* unit)
//
// Extracts the information from the specified model and stores it in 'mesh', filling 'unit' with
// UV polygon information (for the 'pan' component only)
//-------------------------------------------------------------------------------------------------
void ExtractUnit(U32 recordnumber, RevoltMesh* mesh, RevoltTrackUnit* unit)
{
	ASSERT(mesh != NULL);
	
	PS_Database& db = ((CHabExtractApp*)AfxGetApp())->Database();

	static string pegname("peg");
	static string panname("pan");
	static string hullname("hul_00");

	if(recordnumber != PS_Database::NullRecord)
	{
		JC_Template* model = GET_TEMPLATE(&db, recordnumber);
		
		JC_PropertyList* properties = model->CreateFullPropertyList();
		JC_Property*	 root_prop = properties->FindProperty("Root Edges");
		
		if(FixedPoint)
		{
			unit->RootEdges = 0x05; //default value - all root edges visible
		}
		else
		{
			unit->RootEdges = 0x0f; //default value - all root edges visible
		}
		
		if(root_prop != NULL)
		{
			JC_Data* data =	root_prop->CopyValue();
			unit->RootEdges = atoi((LPCSTR)data->String());
			delete data;
		}
		delete properties;

		RevoltPolySet* pegset = new RevoltPolySet(pegname);
		ExtractComponent(model, pegset, unit);
		mesh->push_back(Theme->InsertPolySet(*pegset));
		delete pegset;

		RevoltPolySet* panset = new RevoltPolySet(panname);
		ExtractComponent(model, panset, unit);
		mesh->push_back(Theme->InsertPolySet(*panset));
		delete panset;

		RevoltPolySet* hullset = new RevoltPolySet(hullname);
		ExtractComponent(model, hullset, unit);
		mesh->push_back(Theme->InsertPolySet(*hullset));
		delete hullset;

		ExtractPickup(model, unit);
		model->Release();
	}
}
//-------------------------------------------------------------------------------------------------
// ExtractUnit(const CString& recordspec, RevoltMesh* mesh)
//
// Extracts the information from the specified model and stores it in 'mesh'
//-------------------------------------------------------------------------------------------------
void ExtractUnit(const CString& recordspec, RevoltMesh* mesh, RevoltTrackUnit* unit)
{
	ASSERT(mesh != NULL);
	
	PS_Database& db = ((CHabExtractApp*)AfxGetApp())->Database();

	U32 recordnumber = db.FindRecord(recordspec, JC_Record::TypeTemplate);
	if(recordnumber != PS_Database::NullRecord)
	{
		ExtractUnit(recordnumber, mesh, unit);
	}
	else
	{
		api.Log("Missing Model:- " + recordspec + "\n"); 
	}
}

//-------------------------------------------------------------------------------------------------
// IsObjectAUnit(const JC_Object* object)
//
// Determines whether the object represents a unit or not
// This is done by checking the first character of the name to ensure that it is an
// alphabetical character - non-alphabetical characters indicate a special use object
// 
//-------------------------------------------------------------------------------------------------
bool IsObjectAUnit(const JC_Object* object)
{
	ASSERT(object != NULL);
	CString name = object->Name();
	return (isalpha(name[0]) != 0);
}

//-------------------------------------------------------------------------------------------------
// 
//-------------------------------------------------------------------------------------------------
void GetZoneSizes(TRACKZONE* zone, JC_Object* object)
{
	ASSERT(object != NULL);

	float minx, miny, minz;
	float maxx, maxy, maxz;

	minx = miny = minz = FLT_MAX;
	maxx = maxy = maxz = -FLT_MAX;
	
	JC_VertexArray vertices;
	JC_Vertex*	vert;
	const JC_Vector& scale = object->Scale();
	
	//	find the bounding cuboid then put the sizes into the zone
	object->GetVertices(vertices);
	U32 n = vertices.GetSize();
	while(n--)
	{
		vert = vertices[n];
		minx = min(minx, vert->X() * scale.X() * GameScale);
		miny = min(miny, -vert->Y() * scale.Y() * GameScale);
		minz = min(minz, vert->Z() * scale.Z() * GameScale);
		maxx = max(maxx, vert->X() * scale.X() * GameScale);
		maxy = max(maxy, -vert->Y() * scale.Y() * GameScale);
		maxz = max(maxz, vert->Z() * scale.Z() * GameScale);
		delete vert;
	}
	//shift the cuboid into position
	minx += zone->Centre.X;
	maxx += zone->Centre.X;
	miny += zone->Centre.Y;
	maxy += zone->Centre.Y;
	minz += zone->Centre.Z;
	maxz += zone->Centre.Z;

	//calculate the dimensions (from centre of cuboid)
	zone->XSize = ((maxx - minx) / 2);
	zone->YSize = ((maxy - miny) / 2);
	zone->ZSize = ((maxz - minz) / 2);
	//finally adjust the zones position so that it is in the centre of the cuboid
	zone->Centre.X = (minx + zone->XSize);
	zone->Centre.Y = (miny + zone->YSize);
	zone->Centre.Z = (minz + zone->ZSize);
}

//-------------------------------------------------------------------------------------------------
// 
//-------------------------------------------------------------------------------------------------
JC_Object* FindChildObject(const JC_Template* model, CString& name)
{
	ASSERT(model != NULL);
	
	JC_Object* result = NULL;
	PS_Database& db = ((CHabExtractApp*)AfxGetApp())->Database();
	const JC_RecordRefList& childrefs = model->ChildList();
	POSITION childpos = childrefs.GetHeadPosition();
	PS_RecordInfo recinfo;
	while(childpos && (result == NULL))
	{
		JC_RecordRef* childref = (JC_RecordRef*)childrefs.GetAt(childpos);

		U32 recordnumber = childref->RecordNum();
		if(recordnumber != PS_Database::NullRecord)
		{
			db.GetInfo(recordnumber, recinfo);
			if(recinfo.Name() == name)
			{
				result = GET_OBJ(&db, recordnumber);
			}
		}
		childrefs.GetNext(childpos);
	}
	return result;
}

//-------------------------------------------------------------------------------------------------
// ExtractZoneInfo(RevoltModule* module, const JC_Template* model)
//
// Searches the model for all zone information (and associated links) and stores the results
// into module
//-------------------------------------------------------------------------------------------------
void ExtractZoneInfo(RevoltModule* module, const JC_Template* model)
{
	bool zonefound;
	U32 n = 1;
	CString name;
	TRACKZONE* zone = NULL;
	do
	{
		zonefound = false;
		name.Format("$z%d", n);
		JC_Object* zoneobject = FindChildObject(model, name);
		if(zoneobject)
		{
			name.Format("$l%da", n);
			JC_Object* linkobjecta = FindChildObject(model, name);
			if(linkobjecta)
			{
				name.Format("$l%db", n);
				JC_Object* linkobjectb = FindChildObject(model, name);
				if(linkobjectb)
				{
					zone = new TRACKZONE;
					GetObjectPosition(&zone->Centre, zoneobject);
					GetZoneSizes(zone, zoneobject);
					GetObjectPosition(&zone->Links[0].Position, linkobjecta);
					GetObjectPosition(&zone->Links[1].Position, linkobjectb);
					linkobjectb->Release();
					module->Zones.Insert(zone);
					zonefound = true;
				}
				linkobjecta->Release();
			}
			zoneobject->Release();
		}
		n++;
	}while(zonefound == true);

}

void CrossProduct(RevoltVertex* result, const RevoltVertex* v1, const RevoltVertex* v2)
{
	result->X = v1->Y * v2->Z - v1->Z * v2->Y;
    result->Y = v1->Z * v2->X - v1->X * v2->Z;
    result->Z = v1->X * v2->Y - v1->Y * v2->X;
}
	
REAL PlaneDistance(const RevoltVertex* normal, const RevoltVertex* planevec)
{
	return -((planevec->X * normal->X) + (planevec->Y * normal->Y) + (planevec->Z * normal->Z));
}

REAL DistanceFromPlane(const RevoltVertex* normal, const RevoltVertex* planevec, const RevoltVertex* testvec)
{
	REAL planedistance = PlaneDistance(normal, planevec);
	REAL pointdistance = PlaneDistance(normal, testvec);
	return planedistance - pointdistance;
}

REAL LengthOfVector(const RevoltVertex* vec)
{
	return fabs(sqrt((vec->X * vec->X) + (vec->Y * vec->Y) + (vec->Z * vec->Z)));
}

//-------------------------------------------------------------------------------------------------
// CalculateRacingLine(AINODEINFO* node, const RevoltVertex* racingpos)
//
//
//-------------------------------------------------------------------------------------------------
void CalculateRacingLine(AINODEINFO* node, const RevoltVertex* racingpos)
{
	RevoltVertex redvec, greenvec;

	greenvec.X = fabs(node->Ends[AI_GREEN_NODE].X - racingpos->X);
	greenvec.Y = fabs(node->Ends[AI_GREEN_NODE].Y - racingpos->Y);
	greenvec.Z = fabs(node->Ends[AI_GREEN_NODE].Z - racingpos->Z);

	redvec.X = fabs(node->Ends[AI_RED_NODE].X - racingpos->X);
	redvec.Y = fabs(node->Ends[AI_RED_NODE].Y - racingpos->Y);
	redvec.Z = fabs(node->Ends[AI_RED_NODE].Z - racingpos->Z);

	REAL greenlength = LengthOfVector(&greenvec);
	REAL redlength = LengthOfVector(&redvec);

	node->RacingLine = redlength / (redlength + greenlength);
}

//-------------------------------------------------------------------------------------------------
// IntegerizeVector(RevoltVertex* racingpos)
//
//
//-------------------------------------------------------------------------------------------------
void IntegerizeVector(RevoltVertex* vec)
{
	if(vec->X < 0)
	{
		vec->X = (REAL)((int)(vec->X - HALF));
	}
	else
	{
		vec->X = (REAL)((int)(vec->X + HALF));
	}

	if(vec->Y < 0)
	{
		vec->Y = (REAL)((int)(vec->Y - HALF));
	}
	else
	{
		vec->Y = (REAL)((int)(vec->Y + HALF));
	}

	if(vec->Z < 0)
	{
		vec->Z = (REAL)((int)(vec->Z - HALF));
	}
	else
	{
		vec->Z = (REAL)((int)(vec->Z + HALF));
	}
}

//-------------------------------------------------------------------------------------------------
// ExtractAINodes(RevoltModule* module, const JC_Template* model)
//
// Searches the model for all AI node information and stores the results into module
//-------------------------------------------------------------------------------------------------
void ExtractAINodes(RevoltModule* module, const JC_Template* model)
{
	bool nodefound;
	CString name;
	AINODEINFO* node = NULL;
	for(U16 g = 0; g < MAX_MODULE_ROUTES; g++)
	{
		U32 n = 1;
		do
		{
			nodefound = false;
			name.Format("$greennode%d%c", n, g + 'a');
			JC_Object* greenobject = FindChildObject(model, name);
			if(greenobject)
			{
				name.Format("$rednode%d%c", n, g + 'a');
				JC_Object* redobject = FindChildObject(model, name);
				if(redobject)
				{
					name.Format("$racingnode%d%c", n, g + 'a');
					JC_Object* racingobject = FindChildObject(model, name);
					if(racingobject)
					{
						node = new AINODEINFO;
						RevoltVertex racingpos;
						GetObjectPosition(&node->Ends[AI_GREEN_NODE], greenobject);
						IntegerizeVector(&node->Ends[AI_GREEN_NODE]);
						node->Ends[AI_GREEN_NODE].Y = SnapValue(node->Ends[AI_GREEN_NODE].Y, GameScale /2);

						GetObjectPosition(&node->Ends[AI_RED_NODE], redobject);
						IntegerizeVector(&node->Ends[AI_RED_NODE]);
						node->Ends[AI_RED_NODE].Y = SnapValue(node->Ends[AI_RED_NODE].Y, GameScale /2);

						GetObjectPosition(&racingpos, racingobject);
						CalculateRacingLine(node, &racingpos);
						racingobject->Release();
						module->Nodes[g].Insert(node);	//give ownership of the node info to the module
						node = NULL;	//NULL out the pointer so that we don't accidentally use it again
						nodefound = true;
					}
					redobject->Release();
				}
				greenobject->Release();
			}
			n++;
		}while(nodefound == true);
	}
}

//-------------------------------------------------------------------------------------------------
// ExtractLights(RevoltModule* module, const JC_Template* model)
//
// Searches the model for all lights and stores the results into module
//-------------------------------------------------------------------------------------------------
void ExtractLights(RevoltModule* module, const JC_Template* model)
{
	CString name;
	JC_Object* lightobject = NULL;
	JC_Vector Up;
	JC_Vector Forward;
	U32 n = 1;
	do
	{
		name.Format("$light%d", n);
		lightobject = FindChildObject(model, name);
		if(lightobject)
		{
			LIGHTINFO* light = new LIGHTINFO;

			GetObjectPosition(&light->Position, lightobject);
			IntegerizeVector(&light->Position);
			light->Reach = lightobject->Scale().Z() * GameScale;
			Up = lightobject->Anchor().VectorUp();
			Forward = lightobject->Anchor().VectorFwd();
			light->Up.X = Up.X;
			light->Up.Y = Up.Y;
			light->Up.Z = Up.Z;
			light->Forward.X = Forward.X;
			light->Forward.Y = Forward.Y;
			light->Forward.Z = Forward.Z;
			light->Cone = atan(((lightobject->Scale().X() + lightobject->Scale().Y()) / 2) / lightobject->Scale().Z()) * 2;
			light->Cone /= (PI / 180);
			light->Red = 255;
			light->Green = 255;
			light->Blue = 255;
			light->Type = LIGHT_SPOT;
			
			module->Lights.Insert(light);	//give ownership of the node info to the module
			lightobject->Release();
			n++;
		}
	}while(lightobject != NULL);
}

//-------------------------------------------------------------------------------------------------
// ExtractModule(const CString& recordspec, RevoltTheme* theme)
//
// Extracts the information from the specified module, stores it in a locally created module
// then attaches that module to 'theme'
//-------------------------------------------------------------------------------------------------
void ExtractModule(const CString& recordspec, RevoltTheme* theme)
{
	ASSERT(theme != NULL);
	
	PS_Database& db = ((CHabExtractApp*)AfxGetApp())->Database();

	U32 recordnumber = db.FindRecord(recordspec, JC_Record::TypeTemplate);
	if(recordnumber != PS_Database::NullRecord)
	{
		JC_Template* model = GET_TEMPLATE(&db, recordnumber);
		
		string description((LPCSTR)model->Name());
		JC_PropertyList* properties = model->CreateFullPropertyList();
		JC_Property*	 desc_prop = properties->FindProperty("Description");
		if(desc_prop != NULL)
		{
			JC_Data* data =	desc_prop->CopyValue();
			description = (LPCSTR)data->String();
			delete data;
		}
		delete properties;

		RevoltModule* module = new RevoltModule(description);

 		theme->InsertModule(module);

		CString response;
		response.Format("Processing module %s\n", recordspec);
		api.Log(response);

		response.Format("Name - %s\n", description.c_str());
		api.Log(response);

		api.Indent(4);

		JC_RecordRefList& childrefs = model->ChildList();
		POSITION childpos = childrefs.GetHeadPosition();
		PS_RecordInfo recinfo;
		while(childpos)
		{
			JC_RecordRef* childref = (JC_RecordRef*)childrefs.GetAt(childpos);

			U32 recordnumber = childref->RecordNum();
			if(recordnumber != PS_Database::NullRecord)
			{
				JC_Object*	   childobject = GET_OBJ(&db, recordnumber);

				if(IsObjectAUnit(childobject))
				{
					JC_Vector pos = childobject->Anchor().AnchorPos() + childobject->Anchor().PinVector();

					RevoltTrackUnit* unit = new RevoltTrackUnit;	//create a new track unit
					RevoltMesh* mesh = new RevoltMesh((LPCSTR)childobject->Name()); //create a new mesh

					unit->MeshID = theme->InsertMesh(mesh); //add mesh to theme and store its index in the unit
					
					U32 unitID = theme->InsertUnit(unit);

					RevoltTrackUnitInstance* instance = new RevoltTrackUnitInstance;
					instance->UnitID = unitID;
					instance->Direction = NORTH;
					instance->XPos = (S16)( pos.X() / 4);
					instance->YPos = (S16)( pos.Z() / 4);
					instance->Elevation = 0;
					module->push_back(instance);
					
					const JC_RecordRef& templateref = childobject->Template();

					ExtractUnit(templateref.RecordNum(), mesh, unit);
				}

				childobject->Release();
			}
			childrefs.GetNext(childpos);
		}
		//if there are no units in the module then we need to add a dummy one (to keep unit indices the same)
		if(module->size() == 0)
		{
			RevoltTrackUnit* unit = new RevoltTrackUnit;	//create a new track unit
			RevoltMesh* mesh = new RevoltMesh("dummy bridge"); //create a new mesh
			mesh->push_back(0);
			mesh->push_back(0);
			mesh->push_back(0);

			unit->MeshID = theme->InsertMesh(mesh); //add mesh to theme and store its index in the unit
			
			U32 unitID = theme->InsertUnit(unit);
			RevoltTrackUnitInstance* instance = new RevoltTrackUnitInstance;
			instance->UnitID = UNIT_SPACER;
			instance->Direction = NORTH;
			instance->XPos = 0;
			instance->YPos = 0;
			instance->Elevation = 0;
			module->push_back(instance);
		}
		ExtractZoneInfo(module, model);
		ExtractAINodes(module, model);
		ExtractLights(module, model);

		if(module->Zones.size() == 0)
		{
			response.Format("********* No zones for this module\n");
			api.Log(response);
		}		
		api.Indent(-4);
		model->Release();
	}
	else
	{
		api.Log("Missing Model:- " + recordspec + "\n"); 
	}
}

//-------------------------------------------------------------------------------------------------
// ExtractWall(const CString& recordspec, RevoltTheme* theme)
//
//-------------------------------------------------------------------------------------------------
void ExtractWall(const CString root, RevoltTheme* theme)
{
	ASSERT(theme != NULL);
	
	PS_Database& db = ((CHabExtractApp*)AfxGetApp())->Database();

	static string pegname("peg");
	static string panname("pan");
	static string hullname("hul_00");

	CString recordspec;
#ifdef NEW_WALLS
	recordspec.Format("shell\\shell_02\\%s\\%s", root, root);
#else
	recordspec.Format("shell\\shell_01\\%s\\%s", root, root);
#endif

	U32 recordnumber = db.FindRecord(recordspec, JC_Record::TypeTemplate);
	if(recordnumber != PS_Database::NullRecord)
	{
		JC_Template* model = GET_TEMPLATE(&db, recordnumber);
		RevoltTrackUnit* unit = new RevoltTrackUnit;	//create a new track unit
		RevoltMesh* mesh = new RevoltMesh((LPCSTR)root); //create a new mesh

		unit->MeshID = theme->InsertMesh(mesh); //add mesh to theme and store its index in the unit
		
		U32 unitID = theme->InsertUnit(unit);

		RevoltPolySet* pegset = new RevoltPolySet(pegname);
		ExtractComponent(model, pegset, unit);
		mesh->push_back(Theme->InsertPolySet(*pegset));
		delete pegset;

		RevoltPolySet* panset = new RevoltPolySet(panname);
		ExtractComponent(model, panset, unit);
		mesh->push_back(Theme->InsertPolySet(*panset));
		delete panset;

		RevoltPolySet* hullset = new RevoltPolySet(hullname);
		ExtractComponent(model, hullset, unit);
		mesh->push_back(Theme->InsertPolySet(*hullset));
		delete hullset;

		model->Release();
	}
}

//-------------------------------------------------------------------------------------------------
// ExtractWalls(const CString& recordspec, RevoltTheme* theme)
//
//-------------------------------------------------------------------------------------------------
void ExtractWalls(RevoltTheme* theme)
{
	ASSERT(theme != NULL);
	
	CString response("Processing walls");
	api.Log(response);

	api.Indent(4);

	theme->WallIndex = theme->UnitCount();
	ExtractWall("wall_00", theme);
	ExtractWall("wall_01", theme);
	ExtractWall("wall_02", theme);
	ExtractWall("wall_03", theme);
	ExtractWall("wall_04", theme);
#ifdef NEW_WALLS
	ExtractWall("wall_05", theme);
#endif

	api.Indent(-4);
}

//-------------------------------------------------------------------------------------------------
// HandleModuleScript(const CString& root)
//
// Looks in the specified location ('root') for a PropertyName called 'ModuleScript' and
// if found, uses the script as the source of the names of the track units to extract.
// The code uses the name ('UnitName') obtained from the script to build a path of the
// form ('root'/Units/'UnitName') and then passes this on to ExtractUnit() to process
// each individual unit
//-------------------------------------------------------------------------------------------------
void HandleModuleScript(const CString& root, RevoltTheme* theme)
{
	CString scriptname = root + "\\ModuleScript";
///	CString scriptname = root + "\\Short Script";
	
	PS_Database& db = ((CHabExtractApp*)AfxGetApp())->Database();

	U32 recordnumber = db.FindRecord(scriptname, JC_Record::TypePropertyName);
	if(recordnumber != PS_Database::NullRecord)
	{
		CString response;
		response.Format("Found Script in record #%d\n", recordnumber);
		api.Log(response);

		JC_PropertyName* script = GET_PROP_NAME(&db, recordnumber);
		
		if(script->HasDefault())
		{
			api.Indent(4);
			JC_Data* data =	script->CopyDefault();
			CString text = data->String();
			delete data;
			ScriptObject scriptlines(text);

			Theme->InsertTPageRecnum(PS_Database::NullRecord);	//ensure that the TPage cache has a NULL texture in it
																//this makes the remaining TPageID values '1-based'
			CString modulepath;	
			U32 pos = 0;
			while( pos < scriptlines.size())
			{
				modulepath = root + "\\Modules\\" + scriptlines[pos] + "\\mod_" + scriptlines[pos];
				string modname = "mod_";
				modname += (LPCSTR)scriptlines[pos];

				ExtractModule(modulepath, theme);
				
				pos++;
				api.Update();
			}
			ExtractWalls(theme);
			IMAGE image;
			CString filename;
			for(U32 b = 1; b < theme->TPageRecnumCount(); b++)
			{
				JC_Bitmap* bitmap = GET_BITMAP(&db, theme->TPageRecnum(b));
				if(bitmap)
				{
					BITMAPINFO* bmi = bitmap->CreateBitmapInfo();
					image.CopyFrom(bmi);
					delete bmi;
					
					filename.Format("tpage_%02d.bmp", b);
					response.Format("Saving %s\n", filename);
					api.Log(response);
					image.SaveBMP((char*)(LPCSTR)filename);
					bitmap->Release();
				}
			}
		}
		else
		{
			api.Log("Script has no default");
		}
		script->Release();
	}
	else
	{
		api.Log("Unable to locate Script record:- " + scriptname);
	}
}

//-------------------------------------------------------------------------------------------------
// HandleUnitScript(const CString& root)
//
// Looks in the specified location ('root') for a PropertyName called 'UnitScript' and
// if found, uses the script as the source of the names of the track units to extract.
// The code uses the name ('UnitName') obtained from the script to build a path of the
// form ('root'/Units/'UnitName') and then passes this on to ExtractUnit() to process
// each individual unit
//-------------------------------------------------------------------------------------------------
void HandleUnitScript(const CString& root, RevoltTheme* theme)
{
	CString scriptname = root + "\\UnitScript";
	
	PS_Database& db = ((CHabExtractApp*)AfxGetApp())->Database();

	U32 recordnumber = db.FindRecord(scriptname, JC_Record::TypePropertyName);
	if(recordnumber != PS_Database::NullRecord)
	{
		CString response;
		response.Format("Found Script in record #%d\n", recordnumber);
		api.Log(response);

		JC_PropertyName* script = GET_PROP_NAME(&db, recordnumber);
		if(script->HasDefault())
		{
			api.Indent(4);
			JC_Data* data =	script->CopyDefault();
			CString text = data->String();
			ScriptObject scriptlines(text);

			CString unitpath;	
			U32 pos = 0;
			while( pos < scriptlines.size())
			{
				unitpath = root + "\\Units\\" + scriptlines[pos] + "\\" + scriptlines[pos];
				RevoltMesh* mesh = new RevoltMesh((LPCSTR)scriptlines[pos]);
				theme->InsertMesh(mesh);
				ExtractUnit(unitpath, mesh, NULL);
				
				pos++;
				api.Update();
			}
			delete data;
		}
		else
		{
			api.Log("Script has no default");
		}
		script->Release();
	}
	else
	{
		api.Log("Unable to locate Script record:- " + scriptname);
	}
}

//-------------------------------------------------------------------------------------------------
// HandleThemeScript(const CString& root)
//
// Looks in the specified location ('root') for a PropertyName called 'ThemeScript' and
// if found, uses the script as the source of the names of the track units to extract.
// The code uses the name ('ThemeName') obtained from the script to build a path of the
// form ('root'/'ThemeName') and then passes this on to HandleModuleScript() to process
// each individual module
//-------------------------------------------------------------------------------------------------
void HandleThemeScript(const CString& root)
{
	CString scriptname = root + "\\ThemeScript";
	
	PS_Database& db = ((CHabExtractApp*)AfxGetApp())->Database();

	U32 recordnumber = db.FindRecord(scriptname, JC_Record::TypePropertyName);
	if(recordnumber != PS_Database::NullRecord)
	{
		CString response;
		response.Format("Found Script in record #%d\n", recordnumber);
		api.Log(response);

		JC_PropertyName* script = GET_PROP_NAME(&db, recordnumber);
		
		if(script->HasDefault())
		{
			api.Indent(4);
			JC_Data* data =	script->CopyDefault();
			CString text = data->String();
			ScriptObject scriptlines(text);

			CString themepath;	
			U32 pos = 0;
			while( pos < scriptlines.size())
			{
				string themename = (LPCSTR)scriptlines[pos];
				themepath.Format("%s\\%s", root, (LPCSTR)scriptlines[pos]);
	
				string filename;
				if( FixedPoint )
					filename = "trackunit.ptu";
				else
					filename = "trackunit.rtu";
				
				Theme = new RevoltTheme(themename);

				HandleModuleScript(themepath, Theme);
	
				CString message;

				U16 meshcount = Theme->MeshCount();
				
				message.Format("%d meshes were created\n", meshcount);
				api.Log(message);

				U16 meshindex = meshcount;
				U16 polysetcount = 0;
				while(meshindex--)
				{
					polysetcount += Theme->Mesh(meshindex)->size();
				}
				
				message.Format("%d modules were created\n", Theme->ModuleCount());
				api.Log(message);

				message.Format("%d units were created\n", Theme->UnitCount());
				api.Log(message);

				message.Format("%d polysets were created from a possible %d\n", Theme->PolySetCount(), polysetcount);
				api.Log(message);

				message.Format("%d vertices were pooled from a total of %d\n", Theme->VertexCount(), VerticesScanned);
				api.Log(message);
				
				message.Format("%d polygons were pooled from a total of %d\n", Theme->PolygonCount(), PolygonsScanned);
				api.Log(message);
				
				message.Format("%d UV coords were pooled \n", Theme->UVCoordCount());
				api.Log(message);

				message.Format("%d UV polygons were pooled from a total of %d\n", Theme->UVPolyCount(), PolygonsScanned);
				api.Log(message);

				message.Format("%d TPage records were pooled \n", Theme->TPageRecnumCount());
				api.Log(message);

				message.Format("%d Gouraud Polys were pooled \n", Theme->RGBPolyCount());
				api.Log(message);

				
				if(WhichEnd == EndianStream::ENDIAN_FOREIGN){
					UnitFileOutputN64 converter;
					converter.OutputUnitFile(filename, Theme, WhichEnd);
				}else{
					UnitFileOutput converter;
					converter.OutputUnitFile(filename, Theme, WhichEnd);
				}
				
				if(FixedPoint == true)
				{
					message.Format("\nOutputting PSX files\n");
					api.Log(message);
					PSXModelOutput converter;
					converter.OutputModels("psxmodels", Theme, WhichEnd);
				}

				delete Theme;

				pos++;
				api.Update();
			}
			delete data;
		}
		else
		{
			api.Log("Script has no default");
		}
		script->Release();
	}
	else
	{
		api.Log("Unable to locate Script record:- " + scriptname);
	}
}
//-------------------------------------------------------------------------------------------------
void theUserProgram(void)	// This is the part you are free to change as you see fit
{
	api.SetAutoRun(TRUE);

#ifdef _PC
	TargetFlags = FOR_PC;
#endif
#ifdef _N64
	TargetFlags = FOR_N64;
#endif
#ifdef _PSX
	TargetFlags = FOR_PSX;
#endif

	CString filename;

	if (__argc == 1)
	{
		api.Error("No command line args");
		return;
	}

	for (int i = 1 ; i < __argc ; i++)
	{
		if (*__argv[i] != '-')
		{
			api.Error("Can't parse command line");
			return;
		}

		if (strncmp(__argv[i], "-f", 2) == 0)
		{
			filename = __argv[i];
			filename = filename.Mid(2);
		}

		if (strcmp(__argv[i], "-e") == 0)
		{
			WhichEnd = EndianStream::ENDIAN_FOREIGN;
		}

		if (strcmp(__argv[i], "-p") == 0)
		{
			FixedPoint = true;
		}
	}

	if (api.Begin("Track Unit extraction", filename))
	{

		VerticesScanned = 0;
		{
			HandleThemeScript("");

		}
		api.SetAutoRun(FALSE);
		api.End("End of extraction");						// Finished
	}
}

U8 FloatToPSXUV(REAL value)
{
	return (U8)min(255, 256 * value);
}

