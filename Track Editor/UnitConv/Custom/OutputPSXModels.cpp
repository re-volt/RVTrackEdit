//-------------------------------------------------------------------------------------------------
// Re-Volt Track Unit Extraction Program.
//
//-------------------------------------------------------------------------------------------------
#include "stdafx.h"

#include <assert.h>
#include "OutputPSXModels.h"
#include "..\trackedit\editorconstants.h"

#define TYPE_QUAD			1
#define TYPE_DSIDED			2
#define TYPE_SEMITRANS		4
#define TYPE_TEXTURED		8
#define TYPE_GOURAUD		16
#define TYPE_PSXMODEL1		32
#define TYPE_PSXMODEL2		64
#define TYPE_MIRROR			128
#define TYPE_SEMITRANS_ONE	256
#define TYPE_TEXANIM		512
#define TYPE_NOENV			1024
#define TYPE_ENV			2048

enum {PEG_INDEX, PAN_INDEX, HULL_INDEX};

typedef struct
{
	U16 Type;
	U16 pad;
	U16 vert[4];
	U8 r[4];
	U8 g[4];
	U8 b[4];
	REAL u[4];
	REAL v[4];
}PSXPOLY;

U8 FloatToPSXUV(REAL value);
EndianOutputStream& operator << (EndianOutputStream& stream, REAL value);

class IndexCompare
{
	public:
		bool operator()(const U16* left, const U16* right) const
		{
			return (*left < *right);
		}
};

typedef MappedVector<U16, IndexCompare> IndexRenumber;

void DeleteDirContents(const char* dirname);
void DeleteDirectory(const char* dirname);

void DeleteDirContents(const char* dirname)
{
	assert(dirname != NULL);

	char searchname[MAX_PATH];
	sprintf(searchname, "%s\\*.*", dirname);

	WIN32_FIND_DATA find_data;
	HANDLE h = FindFirstFile(searchname, &find_data);
	if(h != INVALID_HANDLE_VALUE)
	{
		char targetname[MAX_PATH];
		do
		{
			sprintf(targetname, "%s\\%s", dirname, find_data.cFileName);

			if(find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if(find_data.cFileName[0] != '.')
				{
					DeleteDirectory(targetname);
				}
			}
			else
			{
				DeleteFile(targetname);
			}
		}while(FindNextFile(h, &find_data) == TRUE);
	}
}


void DeleteDirectory(const char* dirname)
{
	assert(dirname != NULL);

	DeleteDirContents(dirname);
	RemoveDirectory(dirname);
}

//**********************************************
//
//**********************************************
void PSXModelOutput::CalculateD3DNormal(REVOLTVECTOR* normal, const REVOLTVECTOR* p0, const REVOLTVECTOR* p1, const REVOLTVECTOR* p2)
{
	assert(p0 != NULL);
	assert(p1 != NULL);
	assert(p2 != NULL);
	assert(normal  != NULL);

	REVOLTVECTOR	vect0;
	REVOLTVECTOR	vect1;

	vect0.X = p0->X - p1->X;
	vect0.Y = p0->Y - p1->Y;
	vect0.Z = p0->Z - p1->Z;

	vect1.X = p2->X - p1->X;
	vect1.Y = p2->Y - p1->Y;
	vect1.Z = p2->Z - p1->Z;

	CROSS_PRODUCT(normal, &vect0, &vect1);
	NORMALIZE_VECTOR(normal);
}

//**********************************************
//
// NullifyCuboid
//
// Initializes cuboid to an 'inside-out, maximized'
// cuboid. This is not a normal configuration
// as all of the min value are greater than all
// of the max values and is mainly used to
// indicate an illegal cuboid.
//
//**********************************************
void PSXModelOutput::NullifyCuboid(CUBE_HEADER_LOAD* cuboid)
{
	assert(cuboid != NULL);

	cuboid->Xmin = FLT_MAX;
	cuboid->Ymin = FLT_MAX;
	cuboid->Zmin = FLT_MAX;

	cuboid->Xmax = -FLT_MAX;
	cuboid->Ymax = -FLT_MAX;
	cuboid->Zmax = -FLT_MAX;
}

//**********************************************
//
// AddVectorToCuboid
//
// If the vector specified is outside of the
// cuboid, the cuboid is expanded to a size which 
// includes it
//
//**********************************************
void PSXModelOutput::AddVectorToCuboid(const VEC* vect,  CUBE_HEADER_LOAD* cuboid)
{
	assert(vect != NULL);
	assert(cuboid != NULL);

	cuboid->Xmin = min(cuboid->Xmin, vect->v[0]);
	cuboid->Ymin = min(cuboid->Ymin, vect->v[1]);
	cuboid->Zmin = min(cuboid->Zmin, vect->v[2]);

	cuboid->Xmax = max(cuboid->Xmax, vect->v[0]);
	cuboid->Ymax = max(cuboid->Ymax, vect->v[1]);
	cuboid->Zmax = max(cuboid->Zmax, vect->v[2]);
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void PSXModelOutput::FillPlane(PSXCOLLPOLY* collpoly)
{
	REVOLTVECTOR normal;
	//first calculate the normal to the 3 vectors supplied
	CalculateD3DNormal(&normal, (RevoltVertex*)&collpoly->Vertex[0], (RevoltVertex*)&collpoly->Vertex[1], (RevoltVertex*)&collpoly->Vertex[2]);	
	
	//now work out the shortest distance from the poly to the world origin
	REAL d = -((collpoly->Vertex[0].v[0] * normal.X) + (collpoly->Vertex[0].v[1] * normal.Y) + (collpoly->Vertex[0].v[2] * normal.Z));
	
	//stuff the answers into the plane structure
	collpoly->Plane.v[0] = normal.X;
	collpoly->Plane.v[1] = normal.Y;
	collpoly->Plane.v[2] = normal.Z;
	collpoly->Plane.v[3] = d;
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void PSXModelOutput::FillCollisionPoly(PSXCOLLPOLY* collpoly, const RevoltPolygon* poly)
{
	U16 vertcount;
	if(poly->size() == 3)
	{
		collpoly->Type = 0;
		vertcount = 3;
	}
	else
	{
		collpoly->Type = QUAD;
		vertcount = 4;
	}
	for(U16 v = 0; v < vertcount; v++)
	{
		const RevoltVertex* vect = LocalTheme->Vertex(poly->at(v));
		collpoly->Vertex[(vertcount - 1)-v].v[0] = vect->X;
		collpoly->Vertex[(vertcount - 1)-v].v[1] = vect->Y;
		collpoly->Vertex[(vertcount - 1)-v].v[2] = vect->Z;

		if(collpoly->Vertex[(vertcount - 1)-v].v[1]  > SMALL_CUBE_SIZE)
		{
			collpoly->Vertex[(vertcount - 1)-v].v[1]  +=SMALL_CUBE_SIZE;
		}
	}

	FillPlane(collpoly);
	
	//now take a copy of the normal from the main plane

	CUBE_HEADER_LOAD cuboid;
	NullifyCuboid(&cuboid);
	AddVectorToCuboid(&collpoly->Vertex[0], &cuboid);
	AddVectorToCuboid(&collpoly->Vertex[1], &cuboid);
	AddVectorToCuboid(&collpoly->Vertex[2], &cuboid);
	if(vertcount == 4)
	{
		AddVectorToCuboid(&collpoly->Vertex[3], &cuboid);
	}
	collpoly->BBox.XMin = cuboid.Xmin;
	collpoly->BBox.YMin = cuboid.Ymin;
	collpoly->BBox.ZMin = cuboid.Zmin;
	collpoly->BBox.XMax = cuboid.Xmax;
	collpoly->BBox.YMax = cuboid.Ymax;
	collpoly->BBox.ZMax = cuboid.Zmax;
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void PSXModelOutput::OutputFloatAsShort(EndianOutputStream* stream, REAL value)
{
	ASSERT(stream != NULL);

	S32	number = (S32)( value * 65536 );
	number >>= 16;
	stream->PutS16(number);
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void PSXModelOutput::OutputModule(const RevoltModule* module, EndianOutputStream* stream)
{
	ASSERT(stream != NULL);
	ASSERT(module != NULL);

	U16 zonecount = module->Zones.size();
	*stream << zonecount;
	for(U16 i = 0; i < zonecount; i++)
	{
		const TRACKZONE* zone = module->Zones.at(i);

		OutputFloatAsShort(stream, (REAL)zone->Centre.X);
		OutputFloatAsShort(stream, (REAL)zone->Centre.Y);
		OutputFloatAsShort(stream, (REAL)zone->Centre.Z);
		OutputFloatAsShort(stream, (REAL)zone->XSize);
		OutputFloatAsShort(stream, (REAL)zone->YSize);
		OutputFloatAsShort(stream, (REAL)zone->ZSize);
		OutputFloatAsShort(stream, (REAL)zone->Links[0].Position.X);
		OutputFloatAsShort(stream, (REAL)zone->Links[0].Position.Y);
		OutputFloatAsShort(stream, (REAL)zone->Links[0].Position.Z);
		OutputFloatAsShort(stream, (REAL)zone->Links[1].Position.X);
		OutputFloatAsShort(stream, (REAL)zone->Links[1].Position.Y);
		OutputFloatAsShort(stream, (REAL)zone->Links[1].Position.Z);
	}
	for(U16 g = 0; g < MAX_MODULE_ROUTES; g++)
	{
		U16 ainodecount = module->Nodes[g].size();
		*stream << ainodecount;
		for(i = 0; i < ainodecount; i++)
		{
			const AINODEINFO* node = module->Nodes[g].at(i);

			OutputFloatAsShort(stream, (REAL)node->Ends[AI_GREEN_NODE].X);
			OutputFloatAsShort(stream, (REAL)node->Ends[AI_GREEN_NODE].Y);
			OutputFloatAsShort(stream, (REAL)node->Ends[AI_GREEN_NODE].Z);
			OutputFloatAsShort(stream, (REAL)node->Ends[AI_RED_NODE].X);
			OutputFloatAsShort(stream, (REAL)node->Ends[AI_RED_NODE].Y);
			OutputFloatAsShort(stream, (REAL)node->Ends[AI_RED_NODE].Z);
			*stream << (U16)(node->RacingLine * 65535);
		}
	}
#if 0
#pragma message	("drop the lights - triage has decide this")
	U16 lightcount = module->Lights.size();
	*stream << lightcount;
	for(i = 0; i < lightcount; i++)
	{
		const LIGHTINFO* light = module->Lights.at(i);

		OutputFloatAsShort(stream, (REAL)light->Position.X);
		OutputFloatAsShort(stream, (REAL)light->Position.Y);
		OutputFloatAsShort(stream, (REAL)light->Position.Z);
		OutputFloatAsShort(stream, (REAL)light->Reach);
		*stream << (REAL)light->Up.X;
		*stream << (REAL)light->Up.Y;
		*stream << (REAL)light->Up.Z;
		*stream << (REAL)light->Forward.X;
		*stream << (REAL)light->Forward.Y;
		*stream << (REAL)light->Forward.Z;
		*stream << (REAL)light->Cone;
		stream->PutU8(light->Red);
		stream->PutU8(light->Green);
		stream->PutU8(light->Blue);
		stream->PutU8(light->Type);
	}
#endif
}

//-------------------------------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------------------------------
void PSXModelOutput::OutputModels(const string& basefolder, RevoltTheme* theme, EndianStream::Endian whichend)
{
	ASSERT(theme != NULL);
	U32 filepos;
	LocalTheme = theme;
	
	CreateDirectory(basefolder.c_str(), NULL);
	DeleteDirContents(basefolder.c_str());

	CString filename;
	CString modelfolder;
	CString collsfolder;
	CString modulefolder;
	CString groupfolder;

	IndexRenumber renumberer;
	
	modelfolder.Format("%s\\models", basefolder.c_str());
	CreateDirectory((LPCTSTR)modelfolder, NULL);
	collsfolder.Format("%s\\colls", basefolder.c_str());
	CreateDirectory((LPCTSTR)collsfolder, NULL);
	modulefolder.Format("%s\\modules", basefolder.c_str());
	CreateDirectory((LPCTSTR)modulefolder, NULL);

	filename.Format("%s\\skirts.dat", modulefolder);
	EndianOutputStream* Skirts = new EndianOutputStream(filename, whichend,whichend == EndianStream::ENDIAN_FOREIGN);

	U16 unitindex = LocalTheme->UnitCount();

	*Skirts << unitindex;
		
	U16* SkirtData = new U16[unitindex];
		
	while(unitindex--)
	{
		groupfolder.Format("%s\\%02d0%02d9", (LPCTSTR)modelfolder, unitindex / 10, unitindex / 10);
		CreateDirectory((LPCTSTR)groupfolder, NULL);
		filename.Format("%s\\%03d.pem", (LPCTSTR)groupfolder, unitindex);

		Output = new EndianOutputStream(filename, whichend,whichend == EndianStream::ENDIAN_FOREIGN);

		if(Output != NULL)
		{
			if(Output->is_open())
			{
				renumberer.clear();
				
				const RevoltTrackUnit*	unit = LocalTheme->Unit(unitindex);
				const RevoltMesh*		mesh = LocalTheme->Mesh(unit->MeshID);

				SkirtData[unitindex] = unit->RootEdges;
				
				U16 polycount = 0;
				for(U16 i = 0; i <= PAN_INDEX; i++)
				{
					const RevoltPolySet* polyset = LocalTheme->PolySet(mesh->at(i));
					polycount += polyset->size();
				}
				*Output << polycount;
				
				U32 vertfilepos = Output->tellp(); //make a note of where we are in file so that we can come back later
				*Output << polycount;				//actually a dummy value until we know the number of verts (unhelpful file format)

				for(i = 0; i <= PAN_INDEX; i++)
				{
					const RevoltPolySet* polyset = LocalTheme->PolySet(mesh->at(i));
					const RevoltRGBPolygon* rgbpoly;
					const RevoltUVPolygon* uvpoly;
					PSXPOLY psxpoly;

					U16 faceindex = polyset->size();
					while(faceindex--)
					{
						U16 uvnum = 0;
						U16 uvstep = 1;
						U16 colornum = 0;
						const RevoltPolygon* poly = LocalTheme->Polygon(polyset->at(faceindex) & ~GOURAUD_SHIFTED);
						if(i == PAN_INDEX)
						{
							rgbpoly = LocalTheme->RGBPolygon(unit->RGBPolySet.at(faceindex));
							uvpoly = LocalTheme->UVPolygon(unit->UVPolySet.at(faceindex)->PolyID);
							uvnum = unit->UVPolySet.at(faceindex)->Rotation;
							uvstep = 1;
							if(unit->UVPolySet.at(faceindex)->Reversed == true)
							{
								uvstep = poly->size() - 1;
							}
						}

						U16 type = TYPE_GOURAUD;
						if(poly->size() == 4)
						{
							type |= TYPE_QUAD;
						}
						if(i == PAN_INDEX)
						{
							type |= TYPE_TEXTURED;
							U16 tpage = unit->UVPolySet.at(faceindex)->TPageID;
							if(tpage == 2)
							{
								type |= TYPE_PSXMODEL2;
							}
						}

						*Output << type;

						U16 vertnum = 0;
						while(vertnum < poly->size())
						{
							psxpoly.vert[vertnum] = (U16)renumberer.Insert(poly->at(vertnum));
							if(i == PAN_INDEX)
							{
								U32 color = rgbpoly->at(colornum);
								psxpoly.r[vertnum] = color >> 16;
								psxpoly.g[vertnum] = color >> 8;
								psxpoly.b[vertnum] = color;
								colornum++;
								colornum %= rgbpoly->size();
								psxpoly.u[vertnum] = LocalTheme->UVCoord(uvpoly->at(uvnum))->U;
								psxpoly.v[vertnum] = LocalTheme->UVCoord(uvpoly->at(uvnum))->V;
								uvnum += uvstep;
								uvnum %= uvpoly->size();
							}
							vertnum++;
						}
						U16 a,b,c,d;
						if( ((polyset->at(faceindex) & GOURAUD_SHIFTED) != 0) && (poly->size() == 4) )
						{
							a = 1;
							b = 2;
							c = 3;
							d = 0;
						}
						else
						{
							a = 0;
							b = 1;
							c = 2;
							d = 3;
						}
						*Output << psxpoly.vert[c] << psxpoly.vert[b] << psxpoly.vert[a] << psxpoly.vert[d];
						Output->PutU8(psxpoly.r[c]);
						Output->PutU8(psxpoly.g[c]);
						Output->PutU8(psxpoly.b[c]);
						Output->PutU8(psxpoly.r[b]);
						Output->PutU8(psxpoly.g[b]);
						Output->PutU8(psxpoly.b[b]);
						Output->PutU8(psxpoly.r[a]);
						Output->PutU8(psxpoly.g[a]);
						Output->PutU8(psxpoly.b[a]);
						Output->PutU8(psxpoly.r[d]);
						Output->PutU8(psxpoly.g[d]);
						Output->PutU8(psxpoly.b[d]);
						Output->PutU8(FloatToPSXUV(psxpoly.u[c]));
						Output->PutU8(FloatToPSXUV(psxpoly.v[c]));
						Output->PutU8(FloatToPSXUV(psxpoly.u[b]));
						Output->PutU8(FloatToPSXUV(psxpoly.v[b]));
						Output->PutU8(FloatToPSXUV(psxpoly.u[a]));
						Output->PutU8(FloatToPSXUV(psxpoly.v[a]));
						Output->PutU8(FloatToPSXUV(psxpoly.u[d]));
						Output->PutU8(FloatToPSXUV(psxpoly.v[d]));
						filepos = Output->tellp();
					}
				}
				for(i = 0; i < renumberer.size(); i++)
				{
					const RevoltVertex* vert = LocalTheme->Vertex(*(renumberer[i]));
					OutputFloatAsShort(Output, vert->X);
					OutputFloatAsShort(Output, vert->Y);
					OutputFloatAsShort(Output, vert->Z);
					filepos = Output->tellp();
				}
				U32 endfilepos = Output->tellp();
				Output->seekp(vertfilepos, (std::ios_base::seekdir)ios::beg);
				Output->PutU16(renumberer.size());
				Output->seekp(endfilepos, (std::ios_base::seekdir)ios::beg);

				groupfolder.Format("%s\\%02d0%02d9", (LPCTSTR)collsfolder, unitindex / 10, unitindex / 10);
				CreateDirectory((LPCTSTR)groupfolder, NULL);
				filename.Format("%s\\%03d.col", (LPCTSTR)groupfolder, unitindex);
				CollOutput = new EndianOutputStream(filename, whichend,whichend == EndianStream::ENDIAN_FOREIGN);

				if(CollOutput != NULL)
				{
					if(CollOutput->is_open())
					{
						const RevoltPolySet* polyset = LocalTheme->PolySet(mesh->at(HULL_INDEX));
						PSXCOLLPOLY collpoly;

						U16 faceindex = polyset->size();
						CollOutput->PutU16(faceindex);
						while(faceindex--)
						{
							const RevoltPolygon* poly = LocalTheme->Polygon(polyset->at(faceindex) & ~GOURAUD_SHIFTED);
							FillCollisionPoly(&collpoly, poly);
							collpoly.Material = unit->SurfaceSet.at(faceindex);
							CollOutput->PutU16(collpoly.Type);
							CollOutput->PutU16(collpoly.Material);
							*CollOutput << (REAL)collpoly.Plane.v[0];
							*CollOutput << (REAL)collpoly.Plane.v[1];
							*CollOutput << (REAL)collpoly.Plane.v[2];
							*CollOutput << (REAL)collpoly.Plane.v[3];
							for(i = 0; i < 4; i++)
							{
								*CollOutput << (REAL)collpoly.Vertex[i].v[0];
								*CollOutput << (REAL)collpoly.Vertex[i].v[1];
								*CollOutput << (REAL)collpoly.Vertex[i].v[2];
							}	
							*CollOutput << (REAL)collpoly.BBox.XMin;
							*CollOutput << (REAL)collpoly.BBox.XMax;
							*CollOutput << (REAL)collpoly.BBox.YMin;
							*CollOutput << (REAL)collpoly.BBox.YMax;
							*CollOutput << (REAL)collpoly.BBox.ZMin;
							*CollOutput << (REAL)collpoly.BBox.ZMax;
						}
					}
					delete CollOutput;
					CollOutput = NULL;
				}
			}
			delete Output;
			Output = NULL;
		}
	}

	for(int i = 0; i < LocalTheme->UnitCount(); i++)
	{
		*Skirts << SkirtData[i];
	}

	delete[] SkirtData;
	delete Skirts;

	U16 moduleindex = LocalTheme->ModuleCount();
	while(moduleindex--)
	{
		filename.Format("%s\\%03d.mod", (LPCTSTR)modulefolder, moduleindex);

		Output = new EndianOutputStream(filename, whichend, whichend == EndianStream::ENDIAN_FOREIGN);

		if(Output != NULL)
		{
			if(Output->is_open())
			{
				const RevoltModule*	module = LocalTheme->Module(moduleindex);
				OutputModule(module, Output);
			}
			delete Output;
			Output = NULL;
		}
	}
	LocalTheme = NULL;
}


PSXModelOutput::PSXModelOutput()
{
	LocalTheme = NULL;
	Output = NULL;
	CollOutput = NULL;
}




