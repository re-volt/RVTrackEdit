#ifndef _CONSTANTS_H
#define _CONSTANTS_H

#include <bobtypes.h>
#include <TypeDefs.h>

const REAL GameScale = 250.0f;
const U16 MAX_MODULE_ROUTES = 2;
const U32 GOURAUD_SHIFTED = 0x8000;

#endif
