#ifndef __STRUCTS_H
#define __STRUCTS_H

#include <limits.h>
#include <bobtypes.h>
#include <typedefs.h>

#define FOR_PC  1		// Bit values representing intended target machines
#define FOR_PSX 2       // These are for use with the VALID_TARGETS data type
#define FOR_N64 4       // An instance of this type can be found at the start of a track unit file

typedef U16 VALID_TARGETS;	// Bitfield indicating which target machines a file is valid for

const S16 MAX_ELEVATION = 16;	// number of 0.5 metre steps in 8 metres

#ifndef INDEX
typedef short INDEX;
const INDEX MAX_INDEX = SHRT_MAX;
#endif

const U16 MIN_VERT_COUNT = 3;
const S16 ZERO_INDEX = 0;

//-------------------------------------------------------------------------------------------------
// Unit orientation data type
//
//-------------------------------------------------------------------------------------------------
typedef enum DIRECTION{NORTH, EAST, SOUTH, WEST};

//-------------------------------------------------------------------------------------------------
// Type definition for a type used to express a portion - used in UV coords for example
//-------------------------------------------------------------------------------------------------
#ifdef _PSX
    typedef U8 REALPORTION;		//PSX portions expressed in the range of 0 to 255
#else
    typedef REAL REALPORTION;	//Other platforms use range 0.0 to 1.0 (or higher for tiling)
#endif

//-------------------------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------------------------
typedef struct
{
    REALPORTION U;
    REALPORTION V;
}RevoltUVCoord;

typedef struct 
{
	REAL X;
	REAL Y;
	REAL Z;
}RevoltVertex;

typedef struct
{
    INDEX		UnitID;	 // Index into the TrackUnit array
    S16			XPos;
    S16			YPos;
    S16			Elevation;
    DIRECTION	Direction; 
	D3DMATRIX	Matrix;
}RevoltTrackUnitInstance;

typedef struct
{
    INDEX		ModuleID;	 // Index into the Module array
    S16			Elevation;
	S16			XOffset;
	S16			YOffset;
    DIRECTION	Direction;	 // which direction the module is facing
}RevoltTrackModuleInstance;

typedef struct
{
	RevoltVertex	Position;
}TRACKLINK;

typedef struct
{
	RevoltVertex	Centre;
	REAL			XSize;
	REAL			YSize;
	REAL			ZSize;
	TRACKLINK		Links[2];
	bool			IsPipe;		//used only by the editor
}TRACKZONE;

typedef struct
{
	RevoltVertex	Ends[2];
	REAL			RacingLine;	//value in the range 0.0 (green) thru 1.0 (red) - used to express distance between Ends that the racing line is at
	char			Priority;
}AINODEINFO;

enum {AI_GREEN_NODE, AI_RED_NODE};	//enumerated values for the indices into AI_NODE::Ends[]

typedef struct
{
	RevoltVertex	Position;
	REAL			Distance;
	INDEX			Prev[4];
	INDEX			Next[4];
}POSNODE;

typedef struct 
{
	RevoltVertex	Position;
	REAL			Reach;
	RevoltVertex	Up;
	RevoltVertex	Forward;
	REAL			Cone;
	S16				Red;
	S16				Green;
	S16				Blue;
	U16				Type;
}LIGHTINFO;
#endif
